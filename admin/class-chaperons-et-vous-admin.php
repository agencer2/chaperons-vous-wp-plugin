<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/admin
 * @author     Agence R2 <contact@r2.fr>
 */
class Chaperons_Et_Vous_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chaperons_Et_Vous_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chaperons_Et_Vous_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chaperons-et-vous-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chaperons_Et_Vous_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chaperons_Et_Vous_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chaperons-et-vous-admin.js', array( 'jquery' ), $this->version, false );
		
	}
	
	public function yoursite_extra_user_profile_fields( $user ) {
	  ?>
	  <h3><?php _e("Informations LPCR", "blank"); ?></h3>
	  <table class="form-table">
	    <tr>
	      <th><label for="position"><?php _e("Position"); ?></label></th>
	      <td>
	        <input type="text" name="position" id="position" class="regular-text" 
	            value="<?php echo esc_attr( get_the_author_meta( 'position', $user->ID ) ); ?>" /><br />
	        <span class="description"><?php _e("Merci d'entrer la position de la personne."); ?></span>
	    </td>
	    </tr>
	  </table>
	<?php
	}
	
	public function yoursite_save_extra_user_profile_fields( $user_id ) {
	  $saved = false;
	  if ( current_user_can( 'edit_user', $user_id ) ) {
	    update_user_meta( $user_id, 'position', $_POST['position'] );
	    $saved = true;
	  }
	  return $saved;
	}

}
