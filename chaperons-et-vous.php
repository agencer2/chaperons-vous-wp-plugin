<?php

/**
 * @link              http://www.r2.fr
 * @since             1.0.0
 * @package           Chaperons_Et_Vous
 *
 * @wordpress-plugin
 * Plugin Name:       Chaperons & Vous
 * Description:       Fonctionalités Chaperons & Vous
 * Version:           1.0.0
 * Author:            Agence R2
 * Author URI:        http://www.r2.fr
 * Text Domain:       chaperons-et-vous
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-chaperons-et-vous-activator.php
 */
function activate_chaperons_et_vous() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chaperons-et-vous-activator.php';
	Chaperons_Et_Vous_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-chaperons-et-vous-deactivator.php
 */
function deactivate_chaperons_et_vous() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-chaperons-et-vous-deactivator.php';
	Chaperons_Et_Vous_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_chaperons_et_vous' );
register_deactivation_hook( __FILE__, 'deactivate_chaperons_et_vous' );

// Add SESSION support
// add_action('init', 'start_session', 1);
add_action('wp_logout', 'end_session');
add_action('wp_login', 'end_session');

/**
 * Start the session
 */
function start_session() {
    if(!session_id()) {
        session_start();
    }
}

/**
 * End the session
 */
function end_session() {
    session_destroy();
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-chaperons-et-vous.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_chaperons_et_vous() {

	$plugin = new Chaperons_Et_Vous();
	$plugin->run();

}
run_chaperons_et_vous();
