<?php
if(!session_id()) {
    session_start();
}

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public
 * @author     Agence R2 <contact@r2.fr>
 */
class Chaperons_Et_Vous_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * The current user
     */
    private $user;

    /**
     * The children of the current user (if the current user is a parent)
     * @var array $user_children array of "child" objects
     */
    private $user_children = array();

    /**
     * The active creche of the current user
     */
    public $user_active_creche;

    /**
     * The list of creches the user can access
     */
    private $user_creches;
    
    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        setlocale(LC_TIME, "fr_FR.utf8");
        
        // Include menu
        require_once plugin_dir_path(dirname(__FILE__)).'public/partials/chaperons-et-vous-menu.php';
        
        // Keep browser from caching the pages when the user is not logged in
        add_action('send_headers', array($this, 'no_cache_headers'));
        add_action('after_setup_theme', array($this, 'remove_admin_bar'));
        $this->user = $this->get_user();
        $this->find_user_children();
        $this->find_and_set_active_creche();
        $this->get_user_creches();
        add_filter('bloginfo', array($this, 'set_tagline'), 100, 2);
        
        // Avoid header already sent when use wp_redirect() in plugin
        add_action('init', array($this, 'app_output_buffer'));

        // Route non-logged users to login page
        add_action('template_redirect', array($this, 'route_non_logged_users_to_login_page'));
        // Route logged users out of login page
        add_action('template_redirect', array($this, 'route_logged_users_out_of_login_page'));
        // Route non-admin users outside of admin backoffice
        add_action('admin_init', array($this, 'route_non_admin_users_out_of_admin'));
        add_action('template_redirect', array($this, 'route_users_to_cgu_page'));
        add_action('template_redirect', array($this, 'route_admin_users_to_news'));
        

        add_action('wp_ajax_override_active_creche', array($this, 'override_active_creche_ajax'));
        // Action login
        if(!is_user_logged_in()) {
          add_action('init', array($this, 'ajax_login_init'));
        }
    }
    
    public function ajax_login_init()
    {
      wp_register_script('ajax-login-script', get_template_directory_uri() . '/js/theme-ajax.js', array('jquery') );
      wp_enqueue_script('ajax-login-script');
      wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' ),
      'redirecturl' => home_url(),
      'loadingmessage' => __('Veuillez patienter quelques instants...')
      ));
      add_action( 'wp_ajax_nopriv_ajaxlogin', array($this, 'ajax_login') );
    }
    
    public function ajax_login(){
      check_ajax_referer( 'ajax-login-nonce', 'security' );
      $info = array();
      $info['user_login'] = $_POST['username'];
      $info['user_password'] = $_POST['password'];
      $info['remember'] = true;
      $user_signon = wp_signon( $info, false );
      if ( is_wp_error($user_signon) ){
        echo json_encode(array('loggedin'=>false, 'message'=>__('Mauvais mot de passe ou mauvaise adresse, veuillez réessayer')));
      } else {
        // Force connect user by setting datas
        wp_set_current_user($user_signon->ID);
        
        // The user is logged in, we retreive is last connection date
        $lastConnectionDate = get_user_meta($user_signon->ID, "last_connection_date")[0];

        if(!empty($lastConnectionDate)) {
          $this->setNotifications($lastConnectionDate, $user_signon);
        }
        update_user_meta($user_signon->ID, 'last_connection_date', date('Y-m-d H:i:s'));
        echo json_encode(array('loggedin'=>true, 'message'=>__('Connexion réussie, vous allez être redirigé dans quelques instants...')));
      }
      die();
    }
    
    private function setNotifications($lastConnectionDate, $user_signon)
    {
      require_once 'models/class-chaperons-et-vous-album.php';
      require_once 'models/class-chaperons-et-vous-evenement.php';
      require_once 'models/class-chaperons-et-vous-document.php';
      require_once 'models/class-chaperons-et-vous-entity.php';
      require_once 'models/class-chaperons-et-vous-message.php';
            
      // Initialize SESSIONS
      $this->user = $this->get_user();
      $this->find_user_children();
      $this->find_and_set_active_creche();
      $this->get_user_creches();
            
      $userRights = new UserRights();
      
      // Articles
      $args = array( 'category_name'=> 'articles', );
      $myposts = get_posts( $args );
      
      $query_string = array(
        'post_type' => 'post',
        'date_query' => array(
          'after' => $lastConnectionDate
        ),
        'cat' => get_cat_ID("articles"),
        'post_status' => 'publish'
      );
      $resultArticles = new WP_Query($query_string);
      
      // Events
      $events = Evenement::fetchUserEvents($user_signon->ID, $this->user_active_creche, $lastConnectionDate);
      $nbEvents = count($events["events"]);
      
      $wp_session = WP_Session::get_instance();
      $wp_session["albums"] = count(Album::fetchUserAlbums($user_signon->ID, $userRights->getRights(UserRights::TYPE_ALBUM), $this->user_active_creche, $this->user_children, $lastConnectionDate));      
      $wp_session["articles"] = $resultArticles->post_count;
      $wp_session["evenements"] = $nbEvents;
      $wp_session["documents"] = count(Document::fetchUserDocuments($user_signon->ID, $this->user_active_creche, $this->user_children, $lastConnectionDate));
      $wp_session["messages"] = count(Message::fetchUserMessages($user_signon->ID, $lastConnectionDate));

      return;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Chaperons_Et_Vous_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Chaperons_Et_Vous_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/chaperons-et-vous-public.css', array(), $this->version, 'all' );
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Chaperons_Et_Vous_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Chaperons_Et_Vous_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/chaperons-et-vous-public.js', array( 'jquery' ), $this->version, false );
    }

    public function app_output_buffer() {
      ob_start();
    }
    
    /**
     * Add headers to keep browser from caching the pages when the user is not logged in
     * Resolves a problem where users see the login form after logging in and need to refresh to see content
     */
    public function no_cache_headers() {
        if (!is_user_logged_in())
        {
            nocache_headers();
        }
    }

    public function get_user() {
        require (ABSPATH . WPINC . '/pluggable.php');
        if (is_user_logged_in()) {
            global $current_user;
            get_currentuserinfo();
            $_SESSION['current_user'] = $current_user;
            return $current_user;
        }
    }
    
    public function get_creche()
    {
      return $this->user_active_creche;
    }
    
    public function get_children()
    {
      return $this->user_children;
    }

    /**
     * Hide the admin bar
     */
    public function remove_admin_bar() {
        show_admin_bar(false);
    }

    /**
     * Find the children of the user
     */
    private function find_user_children() {
        if (is_user_logged_in() && $this->user->roles[0] == UserRights::ROLE_PARENT)
        {
            require_once plugin_dir_path(dirname(__FILE__)).'public/models/class-chaperons-et-vous-child.php';
            global $wpdb;
            $children_id = $wpdb->get_results("SELECT `id_child` FROM `".$wpdb->prefix."app_asso_parents_children` WHERE `id_parent` = ".get_current_user_id().";");
            foreach ($children_id as $child_object) {
                $child = new Child($child_object->id_child);
                array_push($this->user_children, $child);
            }
            $_SESSION['children'] = $this->user_children;
        }
    }

    /**
     * Find the active creche of the current user
     */
    private function find_and_set_active_creche() {
        global $wpdb;
        require_once plugin_dir_path(dirname(__FILE__)).'public/models/class-chaperons-et-vous-creche.php';
        if (is_user_logged_in())
        {
            
            switch ($this->user->roles[0])
            {
                case UserRights::ROLE_PARENT:
                    if(!empty($this->user_children)) {
                      $id_creche = $this->user_children[0]->get_id_creche();
                    }
                    $active_creche = new Creche($id_creche);
                    $_SESSION['active_creche'] = $active_creche;
                    return $this->user_active_creche = $active_creche;
                    break;
                case UserRights::ROLE_LIMITEDCONTRIB:
                case UserRights::ROLE_CONTRIB:
                case UserRights::ROLE_EDITOR:
                    $id_creche = NULL;
                    if (isset($_SESSION['override_creche_id']) && $_SESSION['override_creche_id'] != 0) {
                        $id_creche = $_SESSION['override_creche_id'];
                    }
                    else
                    {
                        $id_creche = $wpdb->get_var('SELECT `id_creche` FROM `'.$wpdb->prefix.'app_asso_employees_creches` WHERE `id_user` = '.get_current_user_id().';');    
                    }                    
                    $active_creche = new Creche($id_creche);
                    $_SESSION['active_creche'] = $active_creche;
                    return $this->user_active_creche = $active_creche;
                    break;
                default:
                    return $this->user_active_creche = null;
                    break;
            }
        }
    }

    public function override_active_creche_ajax()
    {
        $id = $_POST['id_creche'];
        $this->override_active_creche($id);
    }


    /**
    * Override active creche for user
    **/

    public function override_active_creche($id_new_creche)
    {
        global $wpdb;
        if (is_user_logged_in() && $id_new_creche)
        {
            require_once plugin_dir_path(dirname(__FILE__)).'public/models/class-chaperons-et-vous-creche.php';
            
            $active_creche = new Creche($id_new_creche);
            if ($active_creche) {
                $_SESSION['override_creche_id'] = $id_new_creche;
                return $this->user_active_creche = $active_creche;
            }
            
        }
        return null;
    }


    /**
     * Returns the list of creches the user can access
     */
    public function get_user_creches() {
        global $wpdb;
        $list_of_creches = array();

        if (is_user_logged_in()) {
           if ($this->user->roles[0] != UserRights::ROLE_PARENT) {
            $creches = $wpdb->get_results("SELECT `id_creche`, `name` FROM `".$wpdb->prefix."app_asso_employees_creches` INNER JOIN `".$wpdb->prefix."app_creches` ON `id_creche` = `id` WHERE `id_user` = ".$this->user->ID.";");

            foreach ($creches as $creche) {
              array_push($list_of_creches, array("id" => $creche->id_creche, "name" => $creche->name));
            }
          }
        }
        $_SESSION['creches'] = $list_of_creches;
        $this->user_creches = $list_of_creches;
        return $list_of_creches;
    }

    /**
     * Set the tagline according to the active creche name
     */
    public function set_tagline($info, $show) {
        if (is_user_logged_in()) {
            if ($show == 'description')
            {
                if ($this->user_active_creche != null)
                {                
                    $info = $this->user_active_creche->get_name();
                }
                else
                {
                    $info = 'Administration';
                }
            }
            return $info;
        }
    }

    /**
     * Route users who don't have accepted CGU to CGU page
     */
    public function route_users_to_cgu_page() {
      global $wpdb;
      $varName = $wpdb->prefix."cgu_accept";
      if (is_user_logged_in() && $this->user->data->$varName == 0  && !is_page('cgu')) {
        exit( wp_redirect( home_url('/cgu/') ) );
      }
    }
    
    /**
     * Route non-logged users to login page
     */
    public function route_non_logged_users_to_login_page() {
        if (!is_user_logged_in()  && !is_page('page-accueil')) {
            exit( wp_redirect( home_url('/page-accueil') ) );
        }
    }

    /**
     * Route logged users out of login page
     */
    public function route_logged_users_out_of_login_page() {
        if (is_user_logged_in() && is_page('page-accueil'))
        {
            exit( wp_redirect( home_url() ) );
        }
    }

    /**
     * Route non-admin users outside of admin backoffice
     */
    public function route_non_admin_users_out_of_admin() {
        if (!current_user_can('administrator') && !defined('DOING_AJAX'))
        {
            exit( wp_redirect( home_url() ) );
        }
    }
    
    public function route_admin_users_to_news() {
      if(is_user_logged_in() && $this->user->roles[0] == UserRights::ROLE_ADMIN && empty($this->user_active_creche) && !is_page('articles') && !defined('DOING_AJAX')) {
        exit( wp_redirect( home_url("/articles/") ) );
      }
    }

}
