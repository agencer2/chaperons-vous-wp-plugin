<?php

/**
* 
*/
class Status
{

    private $author_id;
    private $creation_date;
    private $body;
    private $id_creche;
    
    public function __construct($user_id, $date, $body, $id_creche)
    {
        $this->set_author_id($user_id);
        $this->set_creation_date($date);
        $this->set_body($body);
        $this->id_creche = $id_creche;
    }

    // Setters
    private function set_author_id($user_id){ $this->author_id = $user_id; }
    private function set_creation_date($date){ $this->creation_date = $date; }
    private function set_body($body){ $this->body = $body; }
    private function set_id_creche($id_creche){ $this->id_creche = $id_creche;}

    // Getters
    public function get_author_id(){ return $this->author_id; }
    public function get_creation_date(){ return $this->creation_date; }
    public function get_body(){ return $this->body; }
    public function get_id_creche(){ return $this->id_creche; }

    public function save()
    {
        global $wpdb;
        $wpdb->query("INSERT INTO `".$wpdb->prefix."app_feed_status` (`id`, `author_id`, `creation_date`, `body`, `id_creche`) VALUES (NULL, '".$this->author_id."', '".$this->creation_date."', '".$this->body."', '".$this->id_creche."');");
    }

}
