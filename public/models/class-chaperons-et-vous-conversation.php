<?php

/**
* 
*/
class Conversation
{

    private $subject;
    private $user1_id;
    private $user2_id;
    
    public function __construct($subject, $user1_id, $user2_id)
    {
        $this->set_subject($subject);
        $this->set_user1_id($user1_id);
        $this->set_user2_id($user2_id);
    }

    // Setters
    private function set_subject($subject){ $this->subject = $subject; }
    private function set_user1_id($id){ $this->user1_id = $id; }
    private function set_user2_id($id){ $this->user2_id = $id; }

    // Getters
    public function get_subject(){ return $this->subject; }
    public function get_user1_id(){ return $this->user1_id; }
    public function get_user2_id(){ return $this->user2_id; }

    public function save()
    {
        global $wpdb;
        $wpdb->insert(
            $wpdb->prefix."app_conversations",
            array(
                'subject' => $this->get_subject(),
                'user1' => $this->get_user1_id(),
                'user2' => $this->get_user2_id()
                ),
            array('%s', '%d', '%d')
        );
        return $wpdb->insert_id;
    }

}

