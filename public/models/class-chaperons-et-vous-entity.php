<?php

class Entity
{
    
    protected $creche_id;
    protected $entity_type;
    protected $entity_id;
    protected $user_id;

    const VISIBILITY_CRECHE = 1;
    const VISIBILITY_SECTION = 2;
    const VISIBILITY_CHILD = 3;
    
    public function __construct() {}

    // getters
    public function get_entity_type() { return $this->entity_type; }
    public function get_entity_id() { return $this->entity_id; }
    public function get_user_id() { return $this->user_id; }
    public function get_creche_id() { return $this->creche_id; }
    // setters
    public function set_entity_type($data) { $this->entity_type = $data; }
    public function set_entity_id($data) { $this->entity_id = $data; }
    public function set_user_id($data) { $this->user_id = $data; }
    public function set_creche_id($data) { $this->creche_id = $data; }

    // permission functions

    public function hasVisibilityRights() {
      // Does the user has the right to view this object?
      $user = $_SESSION["current_user"];
      if($user->roles[0] == "administrator") {
        $isAllowed = true;
        return $isAllowed;
      }
      $active_creche = $_SESSION["active_creche"];
      if (!$active_creche)
        return false;
      $isAllowed = false;
      if ($this->get_user_id() == $user->ID) {
        $isAllowed = true;
      } else {
        // Checking with creche, section and children
        $entity_type = $this->get_entity_type();
        $entity_id = $this->get_entity_id();
        switch ($entity_type) {
            case EntityRight::VISIBILITY_CRECHE:
              if ($entity_id == $active_creche->get_id()) {
                $isAllowed = true;
              }
            break;
               
            case EntityRight::VISIBILITY_SECTION:
              if (!empty($active_creche)) {
                $sections = $active_creche->get_sections();
                foreach ($sections as $section) {
                  if ($entity_id == $section->get_id()) {
                    $isAllowed = true;
                    break;
                  }
                }
              }
            break;
               
            case EntityRight::VISIBILITY_CHILD:
              $children = $user->get_children();
              if (!empty($children)) {
                foreach ($children as $child) {
                  if ($entity_id == $child->get_id()) {
                    $isAllowed = true;
                    break;
                  }
                }
              }
            break;
        }
      }
      return $isAllowed;
    }

    public function hasEditDeleteRights() {
        //Does the user has the right to edit or delete this object?
        $user = $_SESSION["current_user"];
        $object_id_creche = $this->get_creche_id();
        $role = $user->roles[0];
        $permission = false;
        switch ($role) {
            case UserRights::ROLE_PARENT:
                break;
            case UserRights::ROLE_LIMITEDCONTRIB:
                if($user->ID == intval($this->get_user_id()))
                    $permission = true;
                    break;
            case UserRights::ROLE_CONTRIB:
            case UserRights::ROLE_EDITOR:
            case UserRights::ROLE_MULTICONTRIB:
            case UserRights::ROLE_MULTIEDITOR:
                $permission = $this->isMyCreche($id_creche);
                break;
            case UserRights::ROLE_ADMIN:
                $permission = true;
                break;
        }
        return $permission;
    }
 
    // check if the creche is in the list of user's creches
    public function isMyCreche($id_creche) {
        $user_creche_list = $_SESSION["creches"];
        $toReturn = false;
        if (in_array($id_creche, $user_creche_list));
           return true;
        return $toReturn;
    }

}
