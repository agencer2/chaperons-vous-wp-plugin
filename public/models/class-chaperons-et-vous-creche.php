<?php

/**
* 
*/
class Creche
{

    private $id;
    private $name;
    private $country;
    private $region;
    private $dpt_number;
    private $post_code;
    private $city;
    private $address;
    private $phone;
    private $email;
    private $company;
    private $siret;
    private $opening_date;
    private $opening_time;
    private $closing_time;
    private $nature;
    private $sections = array();
    private $admins = array();
    
    public function __construct($id)
    {
        $this->id = $id;
        $this->get_data_from_db($id);
    }

    // Getters
    public function get_id() { return $this->id; }
    public function get_name() { return $this->name; }
    public function get_country() { return $this->country; }
    public function get_region() { return $this->region; }
    public function get_dpt_number() { return $this->dpt_number; }
    public function get_post_code() { return $this->post_code; }
    public function get_city() { return $this->city; }
    public function get_address() { return $this->address; }
    public function get_phone() { return $this->phone; }
    public function get_email() { return $this->email; }
    public function get_company() { return $this->company; }
    public function get_siret() { return $this->siret; }
    public function get_opening_date() { return $this->opening_date; }
    public function get_opening_time() { return $this->opening_time; }
    public function get_closing_time() { return $this->closing_time; }
    public function get_nature() { return $this->nature; }
    public function get_sections() { return $this->sections; }
    public function get_admins() { return $this->admins; }

    private function get_data_from_db($id) {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_creches` WHERE `id` = ".$id.";");
        $this->id = $results[0]->id;
        $this->name = $results[0]->name;
        $this->country = $results[0]->country;
        $this->region = $results[0]->region;
        $this->dpt_number = $results[0]->department_number;
        $this->post_code = $results[0]->post_code;
        $this->city = $results[0]->city;
        $this->address = $results[0]->address;
        $this->phone = $results[0]->phone;
        $this->email = $results[0]->email;
        $this->company = $results[0]->company;
        $this->siret = $results[0]->siret;
        $this->opening_date = $results[0]->opening_date;
        $this->opening_time = $results[0]->opening_time;
        $this->closing_time = $results[0]->closing_time;
        $this->nature = $results[0]->nature;
        $results = $wpdb->get_results("SELECT `id_section` FROM `".$wpdb->prefix."app_asso_creches_section` WHERE `id_creche` = ".$this->id.";");
        foreach ($results as $item) {
            $section = $wpdb->get_row('SELECT * FROM `'.$wpdb->prefix.'app_sections` WHERE `id` = '.$item->id_section.';');
            $this->sections[$item->id_section] = $section->name;
        }
        $results = $wpdb->get_results("SELECT `id_user` FROM `".$wpdb->prefix."app_asso_employees_creches` WHERE `id_creche` = ".$this->id.";");
        foreach ($results as $item) {
          // If the user is considered as a admin, we list him int he admin list
          $user = get_userdata($item->id_user);
          $role = $user->roles[0];
          if ($role == UserRights::ROLE_ADMIN || $role == UserRights::ROLE_MULTIEDITOR || $role == UserRights::ROLE_MULTICONTRIB || $role == UserRights::ROLE_EDITOR) {
            $this->admins[] = $item->id_user;
          }
        }
    }

}
