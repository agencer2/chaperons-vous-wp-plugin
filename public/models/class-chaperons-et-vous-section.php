<?php

/**
* 
*/
class Section
{

    private $id;
    private $name;
    
    public function __construct($id)
    {
        $this->id = $id;
        $this->get_data_from_db($id);
    }

    // Getters
    public function get_id() { return $this->id; }
    public function get_name() { return $this->name; }

    private function get_data_from_db($id) {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_sections` WHERE `id` = ".$id.";");
        $this->id = $results[0]->id;
        $this->name = $results[0]->name;
    }
    
    public static function loadByName($name) {
      global $wpdb;
      $section = $wpdb->get_row("SELECT * FROM `".$wpdb->prefix."app_sections` WHERE `name` = '".$name."';");
      $section = new Section($section->id);
      return $section;
    }

}
