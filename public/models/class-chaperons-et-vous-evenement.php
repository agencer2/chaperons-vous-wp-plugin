<?php
class Evenement
{

    private $id;
    private $title;
    private $image_url;
    private $body;
    private $entity_type;
    private $entity_id;
    private $datetime_begin;
    private $datetime_end;
    private $creche_id;
    private $user_id;
    private $type_event;
    private $active;
    private $date_add;
    
    const PHOTO_MAX_SIZE = 4000000;
    const PHOTO_FOLDER = "/wp-content/uploads/events/";
    
    const TYPE_ACTIVITY_PARENTS = "Activités parents";
    const TYPE_BIRTHDAY = "Anniversaire";
    const TYPE_INTERVENTION_EXT = "Interventions extérieures";
    const TYPE_INTERVENTION_DOC = "Interventions médecins";
    const TYPE_INTERVENTION_PSY = "Intervention psychologique";
    const TYPE_SHOW = "Spectacle";
    
    public static $types = array(
    	self::TYPE_ACTIVITY_PARENTS,
      self::TYPE_BIRTHDAY,
      self::TYPE_INTERVENTION_EXT,
      self::TYPE_INTERVENTION_DOC,
      self::TYPE_INTERVENTION_PSY,
      self::TYPE_SHOW
    );
    
    public function __construct($id=null)
    {
      if (!empty($id) && is_numeric($id))
        $this->get_data_from_db($id);
    }

    // Getters
    public function get_id() { return $this->id; }
    public function get_title() { return $this->title; }
    public function get_image_url() { return $this->image_url; }
    public function get_body() { return $this->body; }
    public function get_entity_type() { return $this->entity_type; }
    public function get_entity_id() { return $this->entity_id; }
    public function get_datetime_begin() { return $this->datetime_begin; }
    public function get_datetime_end() { return $this->datetime_end; }
    public function get_user_id() { return $this->user_id; }
    public function get_creche_id() { return $this->creche_id; }
    public function get_type_event() { return $this->type_event; }
    public function get_active() { return $this->active; }
    public function get_date_add() { return $this->date_add; }

    // Setters
    public function set_id($data) { $this->id = $data; }
    public function set_title($data) { $this->title = $data; }
    public function set_image_url($data) { $this->image_url = $data; }
    public function set_body($data) { $this->body = $data; }
    public function set_entity_type($data) { $this->entity_type = $data; }
    public function set_entity_id($data) { $this->entity_id = $data; }
    public function set_datetime_begin($data) { $this->datetime_begin = $data; }
    public function set_datetime_end($data) { $this->datetime_end = $data; }
    public function set_user_id($data) { $this->user_id = $data; }
    public function set_creche_id($data) { $this->creche_id = $data; }
    public function set_type_event($data) { $this->type_event = $data; }
    public function set_active($data) { $this->active = $data; }
    public function set_date_add($data) { $this->date_add = $data; }

    private function get_data_from_db($id) {
        global $wpdb;
        $result = $wpdb->get_row("SELECT * FROM `".$wpdb->prefix."app_evenements` WHERE `id` = ".$id.";");

        if(!empty($result)) {
          $this->id = $result->id;
          $this->title = $result->title;
          $this->image_url = $result->image_url;
          $this->body = $result->body;
          $this->datetime_begin = $result->datetime_begin;
          $this->datetime_end = $result->datetime_end;
          $this->type_event = $result->type_event;
          $this->active = $result->active;
          $this->date_add = $result->date_add;
          
          // parent attributes
          $this->creche_id = $result->creche_id;
          $this->entity_type = $result->entity_type;
          $this->entity_id = $result->entity_id;
          $this->user_id = $result->user_id;
        }
    }


    // insert and edit
    public function save()
    {
        global $wpdb;
        if($this->get_id()) {
            return $wpdb->update( 
                $wpdb->prefix."app_evenements", 
                array( 
                    'title' => $this->get_title(),  
                    'image_url' => $this->get_image_url(),
                    'body' => $this->get_body(),    
                    'entity_type' => $this->get_entity_type(),    
                    'entity_id' => $this->get_entity_id(),
                    'datetime_begin' => $this->get_datetime_begin(),
                    'datetime_end' => $this->get_datetime_end(),
                    'user_id' => $this->get_user_id(),
                    'creche_id' => $this->get_creche_id(),
                    'type_event' => $this->get_type_event(),
                    'active' => $this->get_active(),
                    'date_add' => $this->get_date_add()
                ), 
                array( 'id' => $this->get_id() )
            );
        } else {
            $insertOK = $wpdb->insert( 
                $wpdb->prefix."app_evenements", 
                array( 
                    'title' => $this->get_title(),  
                    'image_url' => $this->get_image_url(),
                    'body' => $this->get_body(),    
                    'entity_type' => $this->get_entity_type(),    
                    'entity_id' => $this->get_entity_id(),
                    'datetime_begin' => $this->get_datetime_begin(),
                    'datetime_end' => $this->get_datetime_end(),
                    'user_id' => $this->get_user_id(),
                    'creche_id' => $this->get_creche_id(),
                    'type_event' => $this->get_type_event(),
                    'active' => $this->get_active(),
                    'date_add' => $this->get_date_add()
                )
            );
            
            if ($insertOK){
              $this->id = $wpdb->insert_id;
              return true;
            } else {
              return false;
            }
        }
    }
    
    // delete
    public function delete()
    {
      global $wpdb;
      if(empty($this->id)) {
        return false;
      }
      
      $wpdb->delete($wpdb->prefix."app_evenements", array("id"=>$this->id));
      
      return true;
    }

    // display functions

    public function getCreator() {
        $id_user = $this->get_user_id();
        $user = new WP_User($id_user);
        if(!$user)
            return false;
        return $user;
    }

    public function getCreche() {
        $id_creche = $this->get_creche_id();
        $creche = new Creche($id_creche);
        if(!$creche)
            return false;
        return $creche;
    }

    public function isUserRegistered($user) {
        if(!$user)
            return false;
        global $wpdb;
        $query = "SELECT `id` FROM `".$wpdb->prefix."app_evenements_participation` WHERE `id_user` = ".$user->ID." AND `id_event` = ".$this->get_id();
        $is_user_registered = $wpdb->get_var($query);
        $is_user_registered = $is_user_registered ? true : false;
        return $is_user_registered;
    }

    public function getFormatMonth() {
        $dt = new DateTime($this->datetime_begin);
        return strftime("%B %Y", strtotime($dt->format("d F Y")));
    }

    public function getFormattedDate($strftime_format) {
        $dt = new DateTime($this->get_datetime_begin());
        $date = $dt->format("d F Y");
        $timestamp = strtotime($date);
        return strftime($strftime_format, $timestamp);

    }
    
    public static function fetchUserEvents($userId, $active_creche, $sinceDate = null)
    {
      global $wpdb;
      
      if(empty($active_creche)) {
        return array("events"=>null, "anniversary"=>null);
      }
      // Fetch events for the next 2 months
      $thisMonth = date("n");
      $inThreeMonths = date("n", strtotime("+2 months"));
      
      if($inThreeMonths < $thisMonth) {
        $operator = "OR";
      } else {
        $operator = "AND";
      }
      
      $or = "";
      if (!empty($active_creche)) {
        $or = "creche_id = ".$active_creche->get_id();
      }
      
      $since = "";
      if(!empty($sinceDate)) {
        $since = ' AND date_add >= "'.$sinceDate.'"';
      }
      
      $query = "SELECT id FROM ".$wpdb->prefix."app_evenements WHERE(".$or.") AND (DATE(datetime_begin) >= CURDATE() $operator MONTH(`datetime_begin`) <= $inThreeMonths)".$since." ORDER BY YEAR(`datetime_begin`) ASC, MONTH(`datetime_begin`) ASC;";
      $evenements = $wpdb->get_results($query);
      
      $query = "SELECT * FROM ".$wpdb->prefix."app_children WHERE id_creche = ".$active_creche->get_id()." AND birth_date_publication = 1 AND (DATE(birth_date) >= CURDATE() ".$operator." MONTH(`birth_date`) <= ".$inThreeMonths.") ORDER BY YEAR(`birth_date`) ASC, MONTH(`birth_date`) ASC;";
      $anniversary = $wpdb->get_results($query);
      
      return array("events"=>$evenements, "anniversary"=>$anniversary);
    }

}
