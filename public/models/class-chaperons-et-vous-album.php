<?php

/**
* 
*/
class Album
{

    private $id;
    private $name;
    private $publication;
    private $user_id;
    private $creche_id;
    private $entity_type;
    private $entity_id;
    private $date_add;
    
    const ROOT_URL = "/albums/";
    const URL_DETAILS = "/album-details/";
    
    public function __construct($id=null)
    {
      if (!empty($id) && is_numeric($id)) {
        $this->get_data_from_db($id);
      }
    }

    // Getters
    public function get_id() { return $this->id; }
    public function get_name() { return $this->name; }
    public function get_publication() { return $this->publication; }
    public function get_creche_id() { return $this->creche_id; }
    public function get_user_id() { return $this->user_id; }
    public function get_entity_type() { return $this->entity_type; }
    public function get_entity_id() { return $this->entity_id; }
    public function get_date_add() { return $this->date_add; }
    
    // Generate URL
    public function generate_url() {
      return self::URL_DETAILS."?id=".$this->get_id();
    }
    
    // Setters
    public function set_id($id) { $this->id = $id; }
    public function set_name($name) { $this->name = $name; }
    public function set_publication($publication) { $this->publication = $publication; }
    public function set_user_id($user_id) { $this->user_id = $user_id; }
    public function set_creche_id($creche_id) { $this->creche_id = $creche_id; }
    public function set_entity_type($entity_type) { $this->entity_type = $entity_type; }
    public function set_entity_id($entity_id) { $this->entity_id = $entity_id; }
    public function set_date_add($date_add) { $this->date_add = $date_add; }
    
    private function get_data_from_db($id) {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_albums` WHERE `id` = ".$id.";");
        if (!empty($results)) {
          $this->id = $results[0]->id;
          $this->name = $results[0]->name;
          $this->publication = $results[0]->publication;
          $this->user_id = $results[0]->user_id;
          $this->creche_id = $results[0]->creche_id;
          $this->entity_type = $results[0]->entity_type;
          $this->entity_id = $results[0]->entity_id;
          $this->date_add = $results[0]->date_add;
        }
    }
    
    public function get_photos()
    {
      global $wpdb;
      $results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_photos` WHERE `album_id` = ".$this->id.";");
      return $results;
    }
    
    public function create()
    {
      global $wpdb;
      $data = array(
      	"name"=>$this->name,
        "publication"=>$this->publication,
        "user_id"=>$this->user_id,
        "creche_id"=>$this->creche_id,
        "entity_type"=>$this->entity_type,
        "entity_id"=>$this->entity_id,
        "date_add"=>$this->date_add,
      );
      if($wpdb->insert($wpdb->prefix."app_albums", $data)) {
        return $wpdb->insert_id;
      }
      return 0;
    }
    
    public function update()
    {
      global $wpdb;
      $data = array(
      	"name"=>$this->name,
        "publication"=>$this->publication,
        "user_id"=>$this->user_id,
        "creche_id"=>$this->creche_id,
        "entity_type"=>$this->entity_type,
        "entity_id"=>$this->entity_id,
        "date_add"=>$this->date_add,
      );
      if($wpdb->update($wpdb->prefix."app_albums", $data, array("id"=>$this->id))) {
        return true;
      }
      return false;
    }
    
    public function delete()
    {
      global $wpdb;
      if(empty($this->id)) {
        return false;
      }
      $wpdb->delete($wpdb->prefix."app_albums", array("id"=>$this->id));
      
      // deleting photos in cascade
      $wpdb->delete($wpdb->prefix."app_photos", array("album_id"=>$this->id));
      
      return true;
    }
    
    public static function fetchUserAlbums($userId, $right, $active_creche, $children, $sinceDate = null)
    {
      global $wpdb;
      
      // Get active creche's sections
      $sections = array();
      if (!empty($active_creche)) {
        if($right > UserRights::READ_RIGHTS) {
          $sections = $active_creche->get_sections();
        } else {
          // Get children's sections
          foreach($children as $child) {
            $sections[$child->get_id_section()] = $child->get_id_section();
          }
        }
      }
      
      // Imploding children and sections
      $arraySection = array();
      foreach ($sections as $section_id => $section_name) {
        array_push($arraySection, $section_id);
      }
      $arrayChildren = array();
      foreach ($children as $child) {
        array_push($arrayChildren, $child->get_id());
      }
      
      // Prepare the query
      $query = "SELECT ".$wpdb->prefix."app_albums.*, ".$wpdb->prefix."app_photos.path, COUNT(".$wpdb->prefix."app_photos.path) AS nb_photos FROM ".$wpdb->prefix."app_albums INNER JOIN ".$wpdb->prefix."app_photos ON ".$wpdb->prefix."app_photos.album_id = ".$wpdb->prefix."app_albums.id WHERE ";
      
      $query .= "(user_id = ".$userId;
      
      if($right >= UserRights::WRITE_RIGHTS && !empty($active_creche)) {
        $id_creche = $active_creche->get_id();
        $query .= " AND (creche_id = ".$id_creche.") ";
      } else {
        // if an active creche is set
        if (!empty($active_creche)) {
          $query .= " OR (entity_type = ".Entity::VISIBILITY_CRECHE." AND entity_id = ".$active_creche->get_id().") ";
        }
        // if the active creche containts sections
        if (!empty($arraySection)) {
          $query .= " OR (entity_type = ".Entity::VISIBILITY_SECTION." AND entity_id IN (".implode(",", $arraySection).")) ";
        }
        // if the user has children
        if (!empty($arrayChildren)) {
          $query .= " OR (entity_type = ".Entity::VISIBILITY_CHILD." AND entity_id IN (".implode(",", $arrayChildren).")) ";
        }
      }
      
      $query .= ")";
      
      if(!empty($sinceDate)) {
        $query .= ' AND '.$wpdb->prefix.'app_albums.date_add >= "'.$sinceDate.'"';
      }
      
      $query .= " GROUP BY ".$wpdb->prefix."app_albums.id HAVING MIN(".$wpdb->prefix."app_photos.id) ORDER BY ".$wpdb->prefix."app_albums.date_add DESC";
      $results = $wpdb->get_results($query);
      return $results;
    }
}
