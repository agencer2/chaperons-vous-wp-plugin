<?php

/**
* 
*/
class Child
{

    private $id;
    private $first_name;
    private $last_name;
    private $birth_date;
    private $birth_date_publication;
    private $gender;
    private $id_creche;
    private $id_section;
    private $contract_end_date;
    
    public function __construct($id)
    {
        $this->id = $id;
        $this->get_data_from_db($id);
    }

    // Getters
    public function get_id() { return $this->id; }
    public function get_first_name() { return $this->first_name; }
    public function get_last_name() { return $this->last_name; }
    public function get_birth_date() { return $this->birth_date; }
    public function get_birth_date_publication() { return $this->birth_date_publication; }
    public function get_gender() { return $this->gender; }
    public function get_id_creche() { return $this->id_creche; }
    public function get_id_section() { return $this->id_section; }
    public function get_contract_end_date() { return $this->contract_end_date; }
    
    // Setters
    public function set_first_name($data) { $this->first_name = $data; }
    public function set_last_name($data) { $this->last_name = $data; }
    public function set_birth_date($data) { $this->birth_date = $data; }
    public function set_birth_date_publication($data) { $this->birth_date_publication = $data; }
    public function set_gender($data) { $this->gender = $data; }
    public function set_id_creche($data) { $this->id_creche = $data; }
    public function set_id_section($data) { $this->id_section = $data; }
    public function set_contract_end_date($data) { $this->contract_end_date = $data; }

    private function get_data_from_db($id) {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_children` WHERE `id` = ".$id.";");
        $this->id = $results[0]->id;
        $this->first_name = $results[0]->first_name;
        $this->last_name = $results[0]->last_name;
        $this->birth_date = $results[0]->birth_date;
        $this->birth_date_publication = $results[0]->birth_date_publication;
        $this->gender = $results[0]->gender;
        $this->id_creche = $results[0]->id_creche;
        $this->id_section = $results[0]->id_section;
        $this->contract_end_date = $results[0]->contract_end_date;
    }
    
    public function save()
    {
        global $wpdb;
        if($this->get_id()) {
            return $wpdb->update( 
                $wpdb->prefix."app_children", 
                array( 
                    'first_name' => $this->get_first_name(),  
                    'last_name' => $this->get_last_name(),
                    'birth_date' => $this->get_birth_date(),
                    'birth_date_publication' => $this->get_birth_date_publication(),    
                    'gender' => $this->get_gender(),
                    'id_creche' => $this->get_id_creche(),
                    'id_section' => $this->get_id_section(),
                    'contract_end_date' => $this->get_contract_end_date()
                ), 
                array( 'id' => $this->get_id() )
            );
        } else {
            $insertOK = $wpdb->insert( 
                $wpdb->prefix."app_evenements", 
                array( 
                    'first_name' => $this->get_first_name(),  
                    'last_name' => $this->get_last_name(),
                    'birth_date' => $this->get_birth_date(),
                    'birth_date_publication' => $this->get_birth_date_publication(),    
                    'gender' => $this->get_gender(),
                    'id_creche' => $this->get_id_creche(),
                    'id_section' => $this->get_id_section(),
                    'contract_end_date' => $this->get_contract_end_date()
                )
            );
            
            if ($insertOK){
              $this->id = $wpdb->insert_id;
              return true;
            } else {
              return false;
            }
        }
    }

}