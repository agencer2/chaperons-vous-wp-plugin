<?php

/**
* 
*/
class Message
{

    private $conversation_id;
    private $sender_id;
    private $recipient_id;
    private $sent_date;
    private $body;
     
    public function __construct($conversation_id, $sender_id, $recipient_id, $sent_date, $body)
    {
        $this->set_conversation_id($conversation_id);
        $this->set_sender_id($sender_id);
        $this->set_recipient_id($recipient_id);
        $this->set_sent_date($sent_date);
        $this->set_body($body);
    }

    // Setters
    private function set_conversation_id($conversation_id) { $this->conversation_id = $conversation_id; }
    private function set_sender_id($sender_id) { $this->sender_id = $sender_id; }
    private function set_recipient_id($recipient_id) { $this->recipient_id = $recipient_id; }
    private function set_sent_date($sent_date) { $this->sent_date = $sent_date; }
    private function set_body($body) { $this->body = $body; }

    // Getters
    public function get_conversation_id() { return $this->conversation_id; }
    public function get_sender_id() { return $this->sender_id; }
    public function get_recipient_id() { return $this->recipient_id; }
    public function get_sent_date() { return $this->sent_date; }
    public function get_body() { return $this->body; }

    public function save()
    {
        global $wpdb;
        $wpdb->insert(
            $wpdb->prefix."app_messages",
            array(
                'conversation_id' => $this->conversation_id,
                'sender_id' => $this->sender_id,
                'recipient_id' => $this->recipient_id,
                'sent_date' => $this->sent_date,
                'body' => $this->body
            ),
            array('%d', '%d', '%d', '%s', '%s')
        );
        return $wpdb->insert_id;
    }

    public static function fetchUserMessages($userId, $sinceDate = null)
    {
      global $wpdb;
      
      $since = "";
      if(!empty($sinceDate)) {
        $since = ' AND sent_date >= "'.$sinceDate.'"';
      }
      
      $messages = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_messages` WHERE (`recipient_id` = ".$userId." OR `sender_id` = ".$userId.")$since;");
      return $messages;
    }
}