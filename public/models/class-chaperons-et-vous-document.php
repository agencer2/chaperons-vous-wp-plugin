<?php

/**
* 
*/
class Document
{

    private $id;
    private $name;
    private $folder_id;
    private $path;
    private $description;
    private $type;
    private $size;
    private $creche_id;
    private $user_id;
    private $entity_type;
    private $entity_id;
    private $date_add;
    
    const DOCUMENT_FOLDER = "/wp-content/uploads/documents/";
    const MAX_SIZE = "5000000";
    
    const TYPE_PDF = 1;
    const TYPE_DOC = 2;
    const TYPE_XLS = 3;
    const TYPE_PPT = 4;
    
    public static $mime_types = array(
    	"application/pdf"=>self::TYPE_PDF,
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document"=>self::TYPE_DOC,
      "application/msword"=>self::TYPE_DOC,
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"=>self::TYPE_XLS,
      "application/vnd.ms-excel"=>self::TYPE_XLS,
      "application/vnd.openxmlformats-officedocument.presentationml.presentation"=>self::TYPE_PPT,
      "application/vnd.ms-powerpoint"=>self::TYPE_PPT
    );
    
    public function __construct($id=null)
    {
      if (!empty($id) && is_numeric($id)) {
        $this->get_data_from_db($id);
      }
    }

    // Getters
    public function get_id() { return $this->id; }
    public function get_name() { return $this->name; }
    public function get_folder_id() { return $this->folder_id; }
    public function get_path() { return $this->path; }
    public function get_description() { return $this->description; }
    public function get_type() { return $this->type; }
    public function get_size() { return $this->size; }
    public function get_creche_id() { return $this->creche_id; }
    public function get_user_id() { return $this->user_id; }
    public function get_human_size() {
        $decimals = 2;
        $bytes = $this->size;

        $quant = array(
            'TB' => 1099511627776,  // pow( 1024, 4)
            'GB' => 1073741824,     // pow( 1024, 3)
            'MB' => 1048576,        // pow( 1024, 2)
            'KB' => 1024,           // pow( 1024, 1)
            'B ' => 1,              // pow( 1024, 0)
        );
        foreach ($quant as $unit => $mag )
        {
            if (doubleval($bytes) >= $mag)
            {
                return sprintf('%01.'.$decimals.'f', ($bytes / $mag)).' '.$unit;
            }
        }
        return false;
    }
    public function get_date_add() { return $this->date_add; }
    public function get_format() { 
        return strtoupper(pathinfo($this->path)['extension']);
    }
    
    // Setters
    public function set_id($id) { $this->id = $id; }
    public function set_name($name) { $this->name = $name; }
    public function set_folder_id($folder_id) { $this->folder_id= $folder_id; }
    public function set_path($path) { $this->path = $path; }
    public function set_description($description) { $this->description = $description; }
    public function set_type($type) { $this->type = $type; }
    public function set_size($size) { $this->size = $size; }
    public function set_creche_id($creche_id) { $this->creche_id = $creche_id; }
    public function set_user_id($user_id) { $this->user_id = $user_id; }
    public function set_entity_type($entity_type) { $this->entity_type = $entity_type; }
    public function set_entity_id($entity_id) { $this->entity_id = $entity_id; }
    public function set_date_add($date_add) { $this->date_add = $date_add; }
    
    public function create()
    {
      global $wpdb;
      $data = array(
      	"folder_id"=>$this->folder_id,
        "name"=>$this->name,
        "description"=>$this->description,
        "path"=>$this->path,
        "type"=>$this->type,
        "size"=>$this->size,
        "creche_id"=>$this->creche_id,
        "user_id"=>$this->user_id,
        "entity_type"=>$this->entity_type,
        "entity_id"=>$this->entity_id,
        "date_add"=>$this->date_add,
      );
      if($wpdb->insert($wpdb->prefix."app_documents", $data)) {
        return $wpdb->insert_id;
      }
      return 0;
    }
    
    public function delete()
    {
      global $wpdb;
      if(empty($this->id)) {
        return false;
      }
      
      $wpdb->delete($wpdb->prefix."app_documents", array("id"=>$this->id));
      
      return true;
    }
    
    private function get_data_from_db($id) {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_documents` WHERE `id` = ".$id.";");
        $this->id = $results[0]->id;
        $this->name = $results[0]->name;
        $this->folder_id = $results[0]->folder_id;
        $this->path = $results[0]->path;
        $this->description = $results[0]->description;
        $this->type = $results[0]->type;
        $this->size = $results[0]->size;
        $this->user_id = $results[0]->user_id;
        $this->creche_id = $results[0]->creche_id;
        $this->entity_type = $results[0]->entity_type;
        $this->entity_id = $results[0]->entity_id;
        $this->date_add = $results[0]->date_add;
    }

    public static function fetchUserDocuments($userId, $active_creche, $children, $sinceDate = null)
    {
      global $wpdb;
      
      if (!empty($active_creche)) {
        $sections = $active_creche->get_sections();
      } else {
        $sections = array();
      }
      
      // Imploding children and sections
      $arraySection = array();
      foreach ($sections as $sectionId => $sectionName) {
        $arraySection[] = $sectionId;
      }
      
      $arrayChildren = array();
      foreach ($children as $child) {
        $arrayChildren[] = $child->get_id();
      }
      
      // Listings folders
      $query = "
        SELECT ".$wpdb->prefix."app_folders.id AS folder_id, ".$wpdb->prefix."app_documents.id as document_id
        FROM ".$wpdb->prefix."app_folders
        INNER JOIN ".$wpdb->prefix."app_documents ON ".$wpdb->prefix."app_documents.folder_id = ".$wpdb->prefix."app_folders.id
        WHERE (user_id = ".$userId;
      
      if (!empty($active_creche)) {
        $query .= " OR (entity_type = ".Entity::VISIBILITY_CRECHE." AND entity_id = ".$active_creche->get_id().") ";
      }
      
      if (!empty($arraySection)) {
        $query .= " OR (entity_type = ".Entity::VISIBILITY_SECTION." AND entity_id IN (".implode(",", $arraySection).")) ";
      }
      
      if (!empty($arrayChildren)) {
        $query .= " OR (entity_type = ".Entity::VISIBILITY_CHILD." AND entity_id IN (".implode(",", $arrayChildren).")) ";
      }
      
      $query .= ")"; // Closing parenthesis
      
      if(!empty($sinceDate)) {
        $query .= ' AND '.$wpdb->prefix.'app_documents.date_add >= "'.$sinceDate.'"';
      }
      
      $query .= " ORDER BY ".$wpdb->prefix."app_documents.date_add DESC";
      $raw_documents = $wpdb->get_results($query);
      
      return $raw_documents;
    }
}
