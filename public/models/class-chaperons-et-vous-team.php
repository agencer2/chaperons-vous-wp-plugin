<?php

/**
* 
*/
class Team
{

    private $id;
    private $first_name;
    private $last_name;
    private $position;
    private $description;
    private $photo;
    private $id_creche;
    private $id_section;
    
    const PHOTO_MAX_SIZE = 4000000;
    const PHOTO_FOLDER = "/wp-content/uploads/team/";
    
    public function __construct($id=null)
    {
      if (!empty($id) && is_numeric($id)) {
        $this->get_data_from_db($id);
      }
    }

    // Getters
    public function get_id() { return $this->id; }
    public function get_first_name() { return $this->first_name; }
    public function get_last_name() { return $this->last_name; }
    public function get_position() { return $this->position; }
    public function get_description() { return $this->description; }
    public function get_photo() { return $this->photo; }
    public function get_id_creche() { return $this->id_creche; }
    public function get_id_section() { return $this->id_section; }
    
    // Setters
    public function set_id($data) { $this->id = $data; }
    public function set_first_name($data) { $this->first_name = $data; }
    public function set_last_name($data) { $this->last_name = $data; }
    public function set_position($data) { $this->position = $data; }
    public function set_description($data) { $this->description = $data; }
    public function set_photo($data) { $this->photo = $data; }
    public function set_id_creche($data) { $this->id_creche = $data; }
    public function set_id_section($data) { $this->id_section = $data; }
    
    private function get_data_from_db($id) {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_team` WHERE `id` = ".$id.";");
        if (!empty($results)) {
          $this->id = $results[0]->id;
          $this->first_name = $results[0]->first_name;
          $this->last_name = $results[0]->last_name;
          $this->position = $results[0]->position;
          $this->description = $results[0]->description;
          $this->photo = $results[0]->photo;
          $this->id_creche = $results[0]->id_creche;
          $this->id_section = $results[0]->id_section;
        }
    }
    
    public function create()
    {
      global $wpdb;
      $data = array(
      	"first_name"=>$this->first_name,
        "last_name"=>$this->last_name,
        "position"=>$this->position,
        "description"=>$this->description,
        "photo"=>$this->photo,
        "id_creche"=>$this->id_creche,
        "id_section"=>$this->id_section,
      );
      if($wpdb->insert($wpdb->prefix."app_team", $data)) {
        return $wpdb->insert_id;
      }
      return 0;
    }
    
    public function update()
    {
      global $wpdb;
      $data = array(
      	"first_name"=>$this->first_name,
        "last_name"=>$this->last_name,
        "position"=>$this->position,
        "description"=>$this->description,
        "photo"=>$this->photo,
        "id_creche"=>$this->id_creche,
        "id_section"=>$this->id_section,
      );
      if($wpdb->update($wpdb->prefix."app_team", $data, array("id"=>$this->id))) {
        return true;
      }
      return false;
    }
    
    public function delete()
    {
      global $wpdb;
      if(empty($this->id)) {
        return false;
      }
      $wpdb->delete($wpdb->prefix."app_team", array("id"=>$this->id));
      return true;
    }
}
