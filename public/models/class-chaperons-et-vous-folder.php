<?php

/**
* 
*/
class Folder
{

    private $id;
    private $name;
    private $publication;
    private $date_add;
    public $documents = array();
    
    public function __construct($id=null)
    {
      if (!empty($id) && is_numeric($id)) {
        $this->get_data_from_db($id);
      }
    }

    // Getters
    public function get_id() { return $this->id; }
    public function get_name() { return $this->name; }
    public function get_publication() { return $this->publication; }
    public function get_date_add() { return $this->date_add; }
    
    // Append document
    public function appendDocument($document)
    {
      $this->documents[] = $document;
    }
    
    private function get_data_from_db($id) {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_folders` WHERE `id` = ".$id.";");
        $this->id = $results[0]->id;
        $this->name = $results[0]->name;
        $this->publication = $results[0]->publication;
        $this->date_add = $results[0]->date_add;
    }

}
