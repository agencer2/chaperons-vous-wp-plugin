<?php
/**
* 
*/
class Photo
{

    private $id;
    private $name;
    private $album_id;
    private $path;
    private $video;
    private $date_add;
    
    const TMP_FOLDER = '/wp-content/uploads/tmp/';
    const GALLERY_FOLDER = '/wp-content/uploads/gallery/';
    const MAX_SIZE = '4000000';
    const THUMB_PREFIX = 'thumb-';
    
    public function __construct($id=null)
    {
      if (!empty($id) && is_numeric($id)) {
        $this->get_data_from_db($id);
      }
    }

    // Getters
    public function get_id() { return $this->id; }
    public function get_name() { return $this->name; }
    public function get_album_id() { return $this->album_id; }
    public function get_path() { return $this->path; }
    public function get_video() { return $this->video; }
    public function get_date_add() { return $this->date_add; }
    
    // Setters
    public function set_id($id) { $this->id = $id; }
    public function set_name($name) {$this->name = $name; }
    public function set_album_id($album_id) { $this->album_id = $album_id; }
    public function set_path($path) { $this->path = $path; }
    public function set_video($video) { $this->video = $video; }
    public function set_date_add($date_add) { $this->date_add = $date_add; }
    
    private function get_data_from_db($id) {
        global $wpdb;
        $results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_photos` WHERE `id` = ".$id.";");
        $this->id = $results[0]->id;
        $this->name = $results[0]->name;
        $this->album_id = $results[0]->album_id;
        $this->path = $results[0]->path;
        $this->video = $results[0]->video;
        $this->date_add = $results[0]->date_add;
    }
    
    public function create()
    {
      global $wpdb;
      $data = array(
      	"name"=>$this->name,
        "album_id"=>$this->album_id,
        "path"=>$this->path,
        "video"=>$this->video,
        "date_add"=>$this->date_add,
      );
      if($wpdb->insert($wpdb->prefix."app_photos", $data)) {
        $this->set_id($wpdb->insert_id);
        return $wpdb->insert_id;
      }
      return 0;
    }
    
    public function update()
    {
      global $wpdb;
      $data = array(
        "name"=>$this->name,
        "album_id"=>$this->album_id,
        "path"=>$this->path,
        "video"=>$this->video,
        "date_add"=>$this->date_add,
      );
      if($wpdb->update($wpdb->prefix."app_photos", $data, array("id"=>$this->id))) {
        return true;
      }
      return false;
    }
    
    public function delete()
    {
      global $wpdb;
      if(empty($this->id)) {
        return false;
      }

      $wpdb->delete($wpdb->prefix."app_photos", array("id"=>$this->id));
      
      return true;
    }
}
