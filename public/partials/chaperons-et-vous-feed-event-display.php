<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/class-chaperons-et-vous-photo.php';

/**
 * Provide a public-facing view for a new album in the feed
 * 
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */
?>

<?php 

function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

  // trim
  $text = trim($text, '-');

  // transliterate
  setlocale(LC_CTYPE, 'en_GB.utf8');
  $text = iconv(mb_internal_encoding(), 'us-ascii//TRANSLIT', $text);
  setlocale(LC_CTYPE, 'C');
  
  // lowercase
  $text = strtolower($text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  if (empty($text))
  {
    return 'n-a';
  }

  return $text;
}


function lpcr_htmlize_feed_event($event) {
    $author = get_userdata($event->get_user_id());
    // var_dump($event);
?>

	   <div class="cadre-post col-xs-12 col-sm-12 col-md-12">
        <div class="feed-event">
            <div class="row">
                <div class="pull-left">
                    <div class="feed-profile">
                        <div class="feed-profile__photo" style="background-image: url(/wp-content/themes/chaperons-vous-theme/img/profil-defaut.png);"></div>
                        <div class="feed-profile__name-container">
                            <span class="feed-profile__name"><?= ucwords($author->display_name) ?></span> 
                            <span class="feed-profile__creche"><?= ucwords('Crèche R2') ?></span> 
                        </div>
                    </div>
                </div>
                <div class="pull-right">
                    Il y a <?php echo human_time_diff(strtotime($event->get_date_add()), current_time('timestamp')); ?>
                </div>
            </div>
            <hr>
            <div class="row">
				<div class="feed-event__photo hidden-xs hidden-sm">
					<?php if(!empty($event->get_image_url())): ?>
		                    <img class="img-responsive" src="<?= $event->get_image_url(); ?>" alt="<?= $event->get_title() ?>" />
		                <?php else: ?>
		                    <img class="img-responsive" src="/wp-content/themes/chaperons-vous-theme/img/pictos-evenements/<?= slugify($event->get_type_event()) ?>.jpg" alt="">
		            <?php endif; ?>
				</div>
				<div class="feed-event__text">
					<h3><?= $event->get_title() ?> <span class="label"><?= $event->get_type_event() ?></span></h3>
					<h5><i class="fa fa-calendar"></i>Le 25 Octobre 2015</h5>
					<p><?= $event->get_body() ?></p>
				</div>
            </div>
        </div>
    </div>

<?php } ?>
