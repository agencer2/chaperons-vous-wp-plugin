<?php

/**
 * Provide a public-facing view for the messenger package
 * 
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */
?>

<!-- formulaire d'envoi -->

<?php
function lpcr_messenger_form($sender, $recipients_list) {
if($sender->roles[0] == UserRights::ROLE_PARENT) {
  $title = "Contacter la crèche";
} elseif($sender->roles[0] == UserRights::ROLE_EDITOR) {
  $title = "Contacter les parents";
} else {
  $title = "Contact";
}
?>

<form action="<?php echo site_url(); ?>/wp-admin/admin-ajax.php" method="post" name="messagesform" id="messagesform">
    <div class="cadre-post col-xs-12 col-sm-12 col-md-12">
        <div class="messages-form">
            
            <div class='post-title-m'>
                <h2>
                    <a href="#"><?= $title ?></a>
                </h2>
            </div>
            
            <div class="messages-contact-email">
                <div class="contact-email">
                    <label for="select_email">À :</label>
                    <select name="email" class="selectize message" id="select_email" placeholder="Destinataire"></select>
                </div>
            </div>

            <div class="messages-contact-objet">
                <input type="text" class="contact-objet-input" name ="object" id="object" placeholder="Objet : " />
            </div>

            <div class="messages-contact-contenu">
                <textarea class="contact-contenu-textarea" rows="9" name="message" placeholder="Message : " id="message" ></textarea>
            </div>

            <input type="hidden" name="file" value="" id="file" />
            <?php if(false): ?>
            <div class="visible-xs joindre-xs">
                <div class="messages-joindre" onClick="?" >
                    <div class="col-xs-4 messages-joindre-i">
                        <img src="<?php echo theme_root()."/img/joindre-i.png"; ?>"  alt="joindre" />
                    </div>
                    <div class="col-xs-4 messages-joindre-t">Joindre un fichier</div>
                </div>
            </div>
            <?php endif; ?>

            <input type="hidden" name="submit" value="submit" />
            <div class="messages-envoyer" id="submit">
                <div class="col-xs-4 messages-envoyer-i"><img src="<?php echo theme_root()."/img/envoyer-i.svg"; ?>"  alt="envoyer" data-no-retina /></div>
                <div class="col-xs-4 messages-envoyer-t">Envoyer</div>
            </div>

            <?php if(false): ?>
            
            <div class="hidden-xs">
                <div class="messages-joindre" onClick="?" >
                    <div class="col-xs-4 messages-joindre-i">
                        <img src="<?php echo theme_root()."/img/joindre-i.png"; ?>" alt="joindre" />
                    </div>
                    <div class="col-xs-4 messages-joindre-t">Joindre un fichier</div>
                </div>
            </div>

            <?php endif; ?>
        </div>
    </div>
</form>

<script type="text/javascript">
    jQuery(document).ready(function($) {

        // Selectize.js options
        var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' + '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';
        $('.messages-form .selectize').selectize({
            persist: false,
            maxItems: 50,
            create: false,
            closeAfterSelect: true,
            valueField: 'email',
            labelField: 'name',
            searchField: ['name', 'email'],
            options: [
                    <?php
                        foreach ($recipients_list as $recipient) {
                            echo '{email: "'.$recipient[2].'", name: "'.$recipient[1].'"},';
                        }
                    ?>
                    {email: 'chaperonsetvous@lpcr.fr', name: 'Support'}
                ],
            render: {
                item: function(item, escape) {
                    return '<div>' +
                        (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
                        (item.email ? '<span class="email">' + escape(item.email) + '</span>' : '') +
                    '</div>';
                },
                option: function(item, escape) {
                    var label = item.name || item.email;
                    var caption = item.name ? item.email : null;
                    return '<div>' +
                        '<span class="label">' + escape(label) + '</span>' +
                        (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
                    '</div>';
                }
            }
        });
    
        // Send form using AJAX
        $('#submit').click(function(e){
          e.preventDefault();
          var email_list = $('#select_email').val();
          var object_content = $('#object').val();
          var message_content = $('#message').val();
          if (email_list !== '' && object_content !== '' && message_content !== '') {
            var data = {
                action : 'lpcr_messenger_create_message',
                recipients : email_list,
                object : object_content,
                message : message_content
            };
            jQuery.post($('#messagesform').attr('action'), data, function(response){
                location.reload();
            });
          } else {
            alert("Veuillez remplir l'intégralité des champs pour envoyer un message");
          }
        });
    });
</script>

<?php } ?>

<!-- /formulaire d'envoi -->


<!-- nouveau message -->

<?php 

function lpcr_htmlize_message($contact, $tdateime, $subject, $content) {
global $wpdb;
global $current_user;
$contact_first_name = $wpdb->get_row("SELECT `meta_value` FROM `".$wpdb->prefix."usermeta` WHERE `user_id` = ".$contact->ID." AND `meta_key` = 'first_name';");
$contact_last_name = $wpdb->get_row("SELECT `meta_value` FROM `".$wpdb->prefix."usermeta` WHERE `user_id` = ".$contact->ID." AND `meta_key` = 'last_name';");
$contact_name = ucfirst(strtolower($contact_first_name->meta_value)).' '.strtoupper($contact_last_name->meta_value);

$avatar_path = theme_root().'/img/profil-defaut.png';
$image = get_user_option( 'photo', $contact->ID );
if(!empty($image)) {
  $avatar_path = $image;
}


?>

<div class="cadre-post col-xs-12 col-sm-12 col-md-12">
    <div class="messages-post">

        <div class="profil-p">
            <svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
                <defs>
                    <mask id="svgmask2" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                    <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m2" />
                    <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m2-2x" />
                    </mask>
                </defs>
                
                <image xlink:href="<?php echo $avatar_path; ?>" class="attachment-profil-size" mask="url(#svgmask2)" id="p2" y="-10%" x="-10%" width="125%" height="125%" />
                <image xlink:href="<?php echo $avatar_path; ?>" class="attachment-profil-size" mask="url(#svgmask5)" id="p2-2x" y="-10%" x="-10%" width="125%" height="125%" />
            </svg>
        </div>

        <div class="messages-nom">
            <?php
            // Display the contact name only for messages from other users
            if ($current_user->data->ID != $contact->ID) {
                echo $contact_name;
            }
            else {
                echo 'Moi';
            }
        ?>
        </div>
        <br />
        <div class="messages-email">
            <a href=""><?php // echo $contact->user_email ?></a>
        </div>
        <div class="messages-objet">Objet : <?php echo $subject ?></div>
        <div class="icone-messages">
            <img src="<?php echo theme_root();?>/img/iconeprofil-r.png" alt="icone profil" />
        </div>
        <div class="date-messages hidden-xs hidden-sm">
            <abbr class="timeago" title="<?= $tdateime ?>"></abbr>
        </div>



        <div class="contenu-messages">
            <?php echo $content ?>
        </div>
        <?php
        // Display the "Answer" button only on messages from other users
        if ($current_user->data->ID != $contact->ID) {
        ?>
        <a href="">
            <div class="messages-repondre">
                <div class="col-xs-4 messages-envoyer-i">
                    <img src="<?php echo theme_root()."/img/envoyer-i.svg"; ?>" alt="répondre av" data-no-retina />
                    <img src="<?php echo theme_root()."/img/repondre-i.svg"; ?>" alt="répondre" class="repondre-i" data-no-retina />
                </div>
                <div class="col-xs-4  messages-repondre-t">Répondre</div>
                <div class="col-xs-4"></div>
            </div>
        </a>
        <?php } ?>
        
    </div>
</div>

<?php } ?>
<!-- /nouveau message -->
