<?php

/**
 * Provide a public-facing view for the status
 * 
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */
?>

<?php
function lpcr_status_form() {
?>

    <form id="status-form" action="<?php echo $_SERVER['REQUEST_URI'] ?>wp-admin/admin-ajax.php" method="post" name="statusform">
    <div class='cadre-post col-xs-12 col-sm-12 col-md-12 statut-container'>
        <div class="row">
            <input type="text" name="status" class="form-control" id="status-txt" placeholder="Quoi de neuf ?" value="" autofocus>
        </div>
        <div class="row statut-images-container">
        </div>
        <div class="row">
           
            <div class="pull-right">
                <?php if(false): ?>
                <label for="statut-permissions" class="statut-label">Qui verra ce statut ?</label>
                <select class="selectpicker" name="statut-permissions" id="statut-permissions" multiple multiple data-selected-text-format="count>2">
                    <option>Tout le monde</option>
                    <option>Parents uniquement</option>
                    <option>Autre exemple de droit</option>
                </select>
                 <?php endif; ?>
                <a href="#" class="button-md red" id="submit">Poster</a>
            </div>  

        </div>
    </div>
    
    <input type="hidden" name="action" value="lpcr_feed_create_status">
    <input type="hidden" name="redirect" value="/">
    </form>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $("#submit").click(function(e){
                e.preventDefault();
                var status_txt = $('#status-txt').val();
                if (status_txt != '') {
                    var data = {
                        action : 'lpcr_feed_create_status',
                        status : status_txt
                    };
                    jQuery.post($('#status-form').attr('action'), data, function(result){
                        location.reload();
                    });
                }
            });
        });
    </script>

<?php } ?>

<?php 
function lpcr_htmlize_status($status) {
    $author = get_userdata($status->author_id);
    $avatar_path = theme_root().'/img/profil-defaut.png';
    $image = get_user_option('photo', $status->author_id );
    if(!empty($image)) {
      $avatar_path = $image;
    }
?>

    <div class='cadre-post col-xs-12 col-sm-12 col-md-12'>
        <div class="profil-p">
          <svg width="70" height="50" baseProfile="full" preserveAspectRatio="none">
              <defs>
                  <mask id="svgmask3" maskUnits="userSpaceOnUse" maskContentUnits="userSpaceOnUse">
                      <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask.png" id="m3" />
                      <image width="100%" height="100%" xlink:href="<?php echo theme_root();?>/img/mask@2x.png" id="m3-2x" />
                  </mask>
              </defs>
              <image xlink:href="<?php echo $avatar_path ?>" class="attachment-profil-size" mask="url(#svgmask3)" id="p3" y="-10%" x="-10%" width="125%" height="125%" />
              <image xlink:href="<?php echo $avatar_path ?>" class="attachment-profil-size" mask="url(#svgmask3)" id="p3-2x" y="-10%" x="-10%" width="125%" height="125%" />
          </svg>
        </div>
        <div class='nomprofil2'>
            <?php echo $author->display_name; ?>
        </div>
        <?php if (!empty($_SESSION['active_creche'])): ?>
        <div class='statut-profil'>
            <?php echo $_SESSION['active_creche']->get_name(); ?>
        </div>
        <?php endif; ?>
        
        <div class='date-post hidden-xs'>
            Il y a <?php echo human_time_diff(strtotime($status->creation_date), current_time('timestamp')); ?>
            <div class="cat-icon">
                <img src="<?php echo theme_root(); ?>/img/articles.svg">
            </div>
        </div>
        <br />
        <div class='excerpt'>
            <?php echo $status->body ?>
        </div>
        <div class='clearfix'></div>
        <br />
        <div class='date-post visible-xs'>
            <br /><br />Il y a <?php echo human_time_diff(strtotime($status->creation_date), current_time('timestamp')); ?>
        </div>
    </div>

<?php } ?>
