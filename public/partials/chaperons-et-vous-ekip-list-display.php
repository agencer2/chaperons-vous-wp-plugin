<?php
/**
* Provide a public-facing view for the blog articles
*
* @link       http://www.r2.fr
* @since      1.0.0
*
* @package    Chaperons_Et_Vous
* @subpackage Chaperons_Et_Vous/public/partials
*/
?>
<?php
function lpcr_htmlize_ekip($creche, $ekip)
{
global $wpdb;
$userRights = new UserRights();
$directors = $ekip['directors'];
$subdirectors = $ekip['subdirectors'];
$others = $ekip['others'];
$sections = $ekip['sections'];
// Displaying directors

?>
<div class="col-xs-12 cadre-post cadre-eq" id="main-column">
  <div class="profil-post">
    <div class="post-title pull-left"><h2>Votre crèche</h2></div>
    <div class="post-title2 pull-left">Les Petits Chaperons Rouges</div>
    <div class="post-title2r"><?php echo $creche->get_name() ; ?></div>
    <br />
    <div class="excerpt">
      <?php foreach ($directors as $director) : ?>
      <div class="equipe col-xs-12 col-sm-12 col-md-6">
        <div class="equipe-list-item">
          <div class="row">
            <div class="col-xs-3">
              <img src="<?php
              
              if(!empty($director['avatar_path'])){
              echo $director['avatar_path'];
              }
              else{
              echo '/wp-content/themes/chaperons-vous-theme/img/profil-defaut.png';
              }
              ?>" alt="" class="img-circle img-responsive">
            </div>
            <div class="col-xs-9">

              <div class="profil-name"><?php echo $director['name']; ?></div>
              <div class="statut-profil">
                <?php echo $director['position']; ?>
              </div>
              <div class="description-profil"><?php echo $director['description']; ?></div>
              <div class="equipe-controls">
                <?php if($userRights->hasCreateRights(UserRights::TYPE_TEAM)): ?>
                <a href="" data-id="<?= $director['id'] ?>" data-role="<?= $director['role'] ?>" class="js-edit-equipe"><i class="glyphicon glyphicon-pencil"></i>Éditer</a>
                <a href="" data-id="<?= $director['id'] ?>" data-role="<?= $director['role'] ?>" class="js-delete-equipe"><i class="glyphicon glyphicon-remove"></i>Supprimer</a>
                <?php endif; ?>
              </div>
 
            </div>
            
          </div>
          
        </div>
      </div>
      <?php endforeach; ?>
      <?php foreach ($subdirectors as $subdirector) : ?>
      <div class="equipe col-xs-12 col-sm-12 col-md-6">
        <div class="equipe-list-item">
          <div class="row">
            <div class="col-xs-3">
              <img src="<?php
              
              if(!empty($subdirector['avatar_path'])){
              echo $subdirector['avatar_path'];
              }
              else{
              echo '/wp-content/themes/chaperons-vous-theme/img/profil-defaut.png';
              }
              ?>" alt="" class="img-circle img-responsive">
            </div>
            <div class="col-xs-9">

              <div class="profil-name"><?php echo $subdirector['name']; ?></div>
              <div class="statut-profil">
                <?php echo $subdirector['position']; ?>
              </div>
              <div class="description-profil"><?php echo $subdirector['description']; ?></div>
              <div class="equipe-controls">
                <?php if($userRights->hasCreateRights(UserRights::TYPE_TEAM)): ?>
                <a href="" data-id="<?= $subdirector['id'] ?>" data-role="<?= $subdirector['role'] ?>" class="js-edit-equipe"><i class="glyphicon glyphicon-pencil"></i>Éditer</a>
                <a href="" data-id="<?= $subdirector['id'] ?>" data-role="<?= $subdirector['role'] ?>" class="js-delete-equipe"><i class="glyphicon glyphicon-remove"></i>Supprimer</a>
                <?php endif; ?>
              </div>
 
            </div>
            
          </div>
          
        </div>
      </div>
      <?php endforeach; ?>
      <?php foreach ($others as $other) : ?>
      <div class="equipe col-xs-12 col-sm-12 col-md-6">
        <div class="equipe-list-item">
          <div class="row">
            <div class="col-xs-3">
              <img src="<?php
              
              if(!empty($other['avatar_path'])){
              echo $other['avatar_path'];
              }
              else{
              echo '/wp-content/themes/chaperons-vous-theme/img/profil-defaut.png';
              }
              ?>" alt="" class="img-circle img-responsive">
            </div>
            <div class="col-xs-9">

              <div class="profil-name"><?php echo $other['name']; ?></div>
              <div class="statut-profil">
                <?php echo $other['position']; ?>
              </div>
              <div class="description-profil"><?php echo $other['description']; ?></div>
              <div class="equipe-controls">
                <?php if($userRights->hasCreateRights(UserRights::TYPE_TEAM)): ?>
                <a href="" data-id="<?= $other['id'] ?>" data-role="<?= $other['role'] ?>" class="js-edit-equipe"><i class="glyphicon glyphicon-pencil"></i>Éditer</a>
                <a href="" data-id="<?= $other['id'] ?>" data-role="<?= $other['role'] ?>" class="js-delete-equipe"><i class="glyphicon glyphicon-remove"></i>Supprimer</a>
                <?php endif; ?>
              </div>
 
            </div>
            
          </div>
          
        </div>
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
<?php
$listSections = $creche->get_sections();
foreach($ekip["sections"] as $section_id=>$members) :
if(!empty($listSections[$section_id])):
?>
<div class="col-xs-12 cadre-post cadre-eq" id="main-column">
  <div class="profil-post">
    <div class="row">
      <div class="post-title pull-left">
        <h2><?= $listSections[$section_id] ?></h2>
      </div>
    </div>
    <br />
    <div class="excerpt">
      <?php foreach ($members as $other) : ?>
      <div class="equipe col-xs-12 col-sm-12 col-md-6">
        <div class="equipe-list-item">
          <div class="row">
            <div class="col-xs-3">
              <img src="<?php
              
              if(!empty($other['avatar_path'])){
              echo $other['avatar_path'];
              }
              else{
              echo '/wp-content/themes/chaperons-vous-theme/img/profil-defaut.png';
              }
              ?>" alt="" class="img-circle img-responsive">
            </div>
            <div class="col-xs-9">

              <div class="profil-name"><?php echo $other['name']; ?></div>
              <div class="statut-profil">
                <?php echo $other['position']; ?>
              </div>
              <div class="description-profil"><?php echo $other['description']; ?></div>
              <div class="equipe-controls">
                <?php if($userRights->hasCreateRights(UserRights::TYPE_TEAM)): ?>
                <a href="" data-id="<?= $other['id'] ?>" data-role="<?= $other['role'] ?>" class="js-edit-equipe"><i class="glyphicon glyphicon-pencil"></i>Éditer</a>
                <a href="" data-id="<?= $other['id'] ?>" data-role="<?= $other['role'] ?>" class="js-delete-equipe"><i class="glyphicon glyphicon-remove"></i>Supprimer</a>
                <?php endif; ?>
              </div>
 
            </div>
            
          </div>
          
        </div>
      </div>
      <?php endforeach; ?>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<?php
endif;
endforeach;
?>
<form action="/wp-admin/admin-ajax.php" method="POST" enctype="multipart/form-data">
  <div class="c-modal-binder">
    <div class="c-modal-overlay modal--create-equipe">
      <div class="c-modal-container">
        <div class="col-xs-12">
          <div class="c-modal-header">
            <div class="row">
              <h1>Créer / Éditer un membre de l'équipe</h1>
            </div>
          </div>
          <div class="row">
            <div class="c-modal-form-content">
              
              <div class="col-xs-12 col-sm-8">
                <div class="row">
                  <fieldset>
                    <label for="">Prénom</label>
                    <input type="text" name="first_name" class="form-control" autofocus placeholder="Prénom">
                  </fieldset>
                  <fieldset>
                    <label for="">Nom</label>
                    <input type="text" name="last_name" class="form-control" autofocus placeholder="Nom">
                  </fieldset>
                  <fieldset>
                    <label for="">Section</label>
                    <select name="id_section" class="form-control">
                      <option value="0">Aucune section</option>
                      <?php foreach($_SESSION['active_creche']->get_sections() as $key => $section): ?>
                      <option value="<?= $key ?>"><?= $section ?></option>
                      <?php endforeach; ?>
                    </select>
                  </fieldset>
                  <fieldset>
                    <label for="">Poste</label>
                    <input type="text" name="position" class="form-control" autofocus placeholder="Poste">
                  </fieldset>
                  <fieldset>
                    <label for="">Descriptif (40 caractères max)</label>
                    <textarea class="form-control" name="description" id="" cols="30" rows="10"></textarea>
                  </fieldset>
                </div>
              </div>
              <div class="col-xs-12 col-sm-4">
                <a href="#" class="js-equipe-trigger-file modal-add-zone">
                  <span class="icon icon-add-photo"></span>
                  Ajouter une photo
                </a>
                <input type="file" name="photo" id="equipe-profile-upload" class="hidden-file-zone">
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <hr>
        <div class="c-modal-footer">
          <div class="pull-right">
            <a class="button-md grey modal--close-equipe" href="">Annuler</a>
            <button class="button-md red" onclick="submit();" href="">Créer le membre de l'équipe</button>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" value="lpcr_ekip_add" name="action">
  <input type="hidden" value="" name="user_id">
  <input type="hidden" value="/equipe/" name="redirect">
  <input type="hidden" value="" name="role">
</form>
<?php
}
?>
<?php
function lpcr_htmlize_creche($creche, $ekip)
{
global $wpdb;
?>
<br>
<div class="row">
  <div class="col-xs-12">
    <div class="pull-right">
      <a href="#" class="button-md red js-create-equipe">Créer un nouveau membre d'équipe</a>
    </div>
  </div>
</div>
<div class="col-xs-12 cadre-post cadre-eq" id="main-column">
  <div class="profil-post">
    <div class="post-title pull-left"><h2>Votre crèche</h2></div>
    <div class="post-title2 pull-left">Les Petits Chaperons Rouges</div>
    <div class="post-title2r"><?php echo $creche->get_name() ; ?></div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <div class="infos-creche-container">
        <div class="row">
          <div class="col-xs-3 hidden-xs hidden-sm">
            <a target="_blank" class="hidden-xs" href="https://www.google.fr/maps/search/<?= $creche->get_address().' '.$creche->get_post_code().' '.$creche->get_city() ?>">
              <img class="img-circle" style="width: 150px;" src="http://maps.googleapis.com/maps/api/staticmap?center=<?= $creche->get_address().' '.$creche->get_post_code().' '.$creche->get_city() ?>&zoom=15&size=300x300&key=AIzaSyDXPaQwLNGHbEBqO52YPhVoM8NbvX4jDbU" alt="">
            </a>
          </div>
          <div class="col-xs-8">
            
            <div class="infos-creche">
              <p>
                <b><?= $creche->get_name() ?></b><br>
                <?= $creche->get_address() ?><br>
                <?= $creche->get_post_code() ?> <?= $creche->get_city() ?> (<?= $creche->get_region() ?>) <br>
                <b>Tel. </b> <?= $creche->get_phone() ?> <b>Mail. </b> <a href="mailto:<?= $creche->get_email() ?>"><?= $creche->get_email() ?></a>
              </p>
              <p>
                <b>Ouvert de</b> <?= str_replace(':','h',substr($creche->get_opening_time(), 0, 5)) ?>
                <b>à</b> <?= str_replace(':','h',substr($creche->get_closing_time(), 0, 5)) ?><br>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
}
?>