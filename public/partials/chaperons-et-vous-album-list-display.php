<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/class-chaperons-et-vous-photo.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/class-chaperons-et-vous-entity.php';
/**
 * Provide a public-facing view for the blog articles
 * 
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */
?>

<?php
function lpcr_htmlize_albums_header()
{
    $userRights = new UserRights();
    if(is_user_logged_in() && $userRights->hasCreateRights(UserRights::TYPE_ALBUM) ) {
?>
        <div class="album-header">
            <div class="row">
                <div class="pull-right">
                    <a href="#" class="button-md red js-create-album">Créer un album</a>
                </div>
            </div>
        </div>
<?php
    }
}
?>

<?php
function lpcr_htmlize_albums_modal($rights) {
?>
<div class="c-modal-binder">
    <div class="c-modal-overlay modal--create-album">
        <div class="c-modal-container">

            <div class="col-xs-12">
                <div class="c-modal-header">
                    <div class="row">
                        <h1>Créer un nouvel album</h1>
                    </div>
                    <div class="row">
                        <fieldset>
                            <label for="">Titre de l'album</label>
                            <input type="text" class="form-control js-form-create-album-name" autofocus placeholder="Nom du nouvel album">
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <script id="template-img-tmp" type="x-tmpl-mustache">
                        <div class="col-md-4 col-sm-4 col-xs-6 album-item-container" data-photo-id="1">
                            <div class="album-item">
                                <a class="album-item-link" href="#">
                                    <span class="album-item-img">
                                        <img class="img-responsive js-img-mount" src="{{img}}" alt="">
                                    </span>
                                </a>
                                <div class="album-item-controls">
                                    <ul>
                                        <li><a href="#" class="js-album-delete-photo" data-photo-id="{{id}}"><i class="icon icon-trash"></i></a></li>
                                    </ul>
                                </div>
                                <input class="modal-input-legende" data-id="{{id}}" placeholder="Légende.." type="text"/>
                            </div>
                        </div>
                    </script>
                    <div class="js-tmp-mount">
                        
                    </div>
                    <div class="row">
                         <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="album-modal-add-zone">
                                 <form action="" id="form-upload-photo">
                                        <span class="icon icon-add-photo"></span>
                                            Ajouter des photos
                                            <input type="file" name="photo-upload" id="photo-upload">
                                    </form>
                                </div>
                        </div>                       
                    </div>
                    <div class="row">
                         <div class="col-md-12 col-sm-12 col-xs-12">
                           <label for="entity">Sélectionnez la visibilité</label>
                           <select name="entity" class="js-form-create-album-visibility" id="permissions-select">
                             <?php 
                             foreach($rights as $right): ?>
                                <optgroup label="<?= $right['name'] ?>">
                                   <?php foreach($right['data'] as $option): ?> 
                                    <option value="<?= $option['value'] ?>"><?= $option['name'] ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                             <?php endforeach; ?>

                           </select>
                        </div>                       
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr>
            <div class="c-modal-footer">
                <div class="pull-right">
                    <a class="button-md grey modal--close-album" href="">Annuler</a>
                    <a class="button-md red js-create-album-submit" href="">Créer l'album</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
function lpcr_htmlize_albums($albums)
{
  $userRights = new UserRights();
  $rights = $userRights->getRights(UserRights::TYPE_ALBUM);
?>
    <div class="cadre-post cadre-cat">
        <div class="profil-post col-xs-12" >
            <div class="row album-image-list">
                <?php foreach($albums as $album):                
                if(!$userRights->canModerate(UserRights::TYPE_ALBUM) && $album->publication != 1 && $album->user_id != $_SESSION["current_user"]->data->ID) {
                  continue;
                }
                
                ?>
                    <div class="col-md-3 col-sm-6 col-xs-6 album-item-container" data-album-id="<?php echo $album->id ?>">
                        <div class="album-item album-item--all">
                            <a class="album-item-link" href="<?php echo home_url()?>/album-details/?id=<?php echo $album->id ?>">
                                <span class="album-item-img <?php  if($album->publication != 1){ echo 'in-moderation'; } ?>">
                                    <img class="img-responsive" src="<?php echo home_url().Photo::GALLERY_FOLDER.Photo::THUMB_PREFIX.$album->path; ?>" alt="">
                                    <?php if($album->publication != 1): ?>
                                        <span class="album-item-review">
                                            En attente de validation
                                        </span>
                                    <?php endif; ?>
                                </span>
                                <h4><?= $album->name ?></h4>
                                <span class="album-item-photos-counter"><?php echo $album->nb_photos ?> photos</span>
                                <?php if($userRights->hasCreateRights(UserRights::TYPE_ALBUM)): ?>
                                  <div class="album-item-permissions"><i class="glyphicon glyphicon-lock"></i> Visibilité : <?php echo $album->visibility ?></div>
                                <?php endif; ?>
                            </a>
                            <?php if(is_user_logged_in() && $userRights->hasEditDeleteRights(UserRights::TYPE_ALBUM) ) { ?>
                            <div class="album-item-controls">
                                <ul>
                                    <li><a href="#" class="js-album-delete-album" data-album-id="<?php echo $album->id; ?>"><i class="icon icon-trash"></i></a></li>
                                </ul>
                            </div>

                            <?php } ?>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
<?php } ?>
