<?php

/**
 * Provide a public-facing view for the user profile
 *
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */
?>

<?php
function lpcr_htmlize_mon_profil($user) {
    ?>


    <div class="col-xs-12 cadre-post cadre-eq" id="main-column">
        <div class="profil-post">
            <div class="post-title pull-left"><h2>Mon profil</h2></div>

        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="infos-creche-container">
                    <div class="row">
                        <div class="col-xs-3 hidden-xs hidden-sm">
                            <a target="_blank" class="hidden-xs" href="https://www.google.fr/maps/search/22 Quai Duguay Trouin 35000 Rennes">
                                <img class="img-circle" style="width: 150px;" src="http://maps.googleapis.com/maps/api/staticmap?center=22 Quai Duguay Trouin 35000 Rennes&amp;zoom=15&amp;size=300x300&amp;key=AIzaSyDXPaQwLNGHbEBqO52YPhVoM8NbvX4jDbU" alt="">
                            </a>
                        </div>
                        <div class="col-xs-8">

                            <div class="infos-creche">
                                <p>
                                    <b>Crèche R2</b><br>
                                    22 Quai Duguay Trouin<br>
                                    35000 Rennes (Bretagne) <br>
                                    <b>Tel. </b> 0102030405 <b>Mail. </b> <a href="mailto:contact@r2.fr">contact@r2.fr</a>
                                </p>
                                <p>
                                    <b>Ouvert de</b> 07h00                <b>à</b> 19h00<br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
