<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/class-chaperons-et-vous-photo.php';

/**
 * Provide a public-facing view for a new album in the feed
 * 
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */
?>

<?php 
function lpcr_htmlize_feed_album($album) {
    $author = get_userdata($album->get_user_id());
    $nbPhotos = count($album->photos);
?>

    <div class="cadre-post col-xs-12 col-sm-12 col-md-12">
        <div class="pull-right">
            <div class="date-post hidden-xs">Il y a <?php echo human_time_diff(strtotime($album->get_date_add()), current_time('timestamp')); ?></div>
        </div>
        <div class="post-title">
            <h2>
                <a href="<?= Album::URL_DETAILS."?id=".$album->get_id()?>" rel="bookmark" title="Permanent Link to "><?php echo $album->get_name(); ?></a>
            </h2>
        </div>
        <div class="row fil-img-list">
            <?php 
            for($i=0;$i<9;$i++): 
              if(!empty($album->photos[$i])):
            ?>
            <div class="fil-img-list-item">
                <img class="img-responsive" src="<?php echo home_url().Photo::GALLERY_FOLDER."thumb-".$album->photos[$i]->path; ?>" alt="">
            </div>
            <?php 
              endif;
            endfor; 
            ?>
            <a href="<?= Album::URL_DETAILS."?id=".$album->get_id()?>" class="fil-img-list-all">
                <span class="icon icon-fil-photo-view-all"></span>
                <b>Voir toute la série</b>
                <div><?= $nbPhotos ?> Photo(s)</div>
            </a>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="date-post visible-xs">
            <br><br>
            Il y a <?php echo human_time_diff(strtotime($album->get_date_add()), current_time('timestamp')); ?>
        </div>
    </div>

<?php } ?>
