<?php

/**
 * Provide a public-facing view for the evenement package
 * 
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */
?>

<?php

function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

  // trim
  $text = trim($text, '-');

  // transliterate
  setlocale(LC_CTYPE, 'en_GB.utf8');
  $text = iconv(mb_internal_encoding(), 'us-ascii//TRANSLIT', $text);
  setlocale(LC_CTYPE, 'C');
  
  // lowercase
  $text = strtolower($text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  if (empty($text))
  {
    return 'n-a';
  }

  return $text;
}


function lpcr_htmlize_evenements($evenements, $user, $eventTypes) {
$userRights = new UserRights();
?>
    <div class="content-top"></div>
    <?php if($userRights->hasCreateRights(UserRights::TYPE_EVENT)):?>
    <div class="row">
      <div class="pull-right">
          <a href="#" class="button-md red js-create-event">Nouvel événement</a>
      </div>
    </div>
    <?php endif; ?>

    <?php foreach($evenements as $month => $days):
    ?>

    <div class="cadre-top-title">
        <div class="pull-left">
            <div class="top-title"><?= $month ?></div>
        </div>
    </div>
        <?php foreach($days as $events): ?>
          <?php foreach($events as $event): ?>

            <div class="evenements-post col-xs-12 col-sm-12 col-md-12 separation" id="event-<?= $event->get_id(); ?>">
        
                <div class="pull-left">
                    <div class="top-title">Le <?= strftime('%d %B %Y', strtotime($event->get_datetime_begin())); ?></div>
                </div>
                
                <div class="pull-right">
                    <div class="event-cetegory"><?= $event->get_type_event() ?></div>
                    <?php if(is_numeric($event->get_id()) && $userRights->hasCreateRights(UserRights::TYPE_EVENT)):?>
                    <a class="event-controls js-edit-event" data-id="<?= $event->get_id(); ?>" href="#"><span class="icon icon-edit"></span></a>
                    <a class="event-controls js-delete-event" data-id="<?= $event->get_id(); ?>" href="#"><span class="icon icon-trash"></span></a>
                    <?php endif; ?>
                </div>
                <div class="clearfix"></div>
                <div class="evenements-titre">
                  <?= $event->get_title(); ?>
                </div>
                <?php 
                ?>
                <div class="evenements-miniature">
                <?php if(!empty($event->get_image_url())): ?>
                    <img src="<?= $event->get_image_url(); ?>" alt="<?= $event->get_title() ?>" />
                <?php else: ?>
                    <img src="/wp-content/themes/chaperons-vous-theme/img/pictos-evenements/<?= slugify($event->get_type_event()) ?>.jpg" alt="">
               <?php endif; ?>
                </div>
                <div class="evenements-contenu"><?= $event->get_body() ?></div>
                <?php if(is_numeric($event->get_id())):?>
                <div class="evenements-lien"><a href="#">Participer à l’événement</a></div>
                <?php endif; ?>
            </div>
          <?php endforeach; ?>
        <?php endforeach; ?>

    <div class="clearfix"></div>
        
    <?php endforeach; ?>

    <!-- évènements du mois -->

    
    <!-- évènements du mois dernier -->  
    <div class="content-top"></div>  
    <div class="evenements-post col-xs-12 col-sm-12 col-md-12">
        <div class="evenements-vide-img"><img width="116" height="110" src="<?php echo theme_root();?>/img/evenements-vide.png" alt="evenements vide" /></div>
        <div class="evenements-vide-texte">Aucun autre évènement pour l'instant</div>
    </div>
    <div class="clearfix"></div>
    <!-- /évènements du mois dernier -->
        
        <!-- évènements du mois avant-dernier -->
        <!-- 
        <div class="content-top"></div>
        <div class="cadre-top-title">
            <div class="top-title">Septembre 2015</div>
        </div>
        
        <div class="evenements-post col-xs-12 col-sm-12 col-md-12">
            <div class="evenements-vide-img"><img src="<?php echo theme_root();?>/img/evenements-vide.png" alt="evenements vide" /></div>
            <div class="evenements-vide-texte">Aucun évènement pour l'instant</div>
            </div><div class="clearfix"></div>
        -->
            <!-- /évènements du mois avant-dernier -->
            <br /><br />



  <form action="/wp-admin/admin-ajax.php" class="js-create-event-form" method="POST" enctype="multipart/form-data">
    
  <div class="c-modal-binder">
      <div class="c-modal-overlay modal--create-event">
          <div class="c-modal-container">
              <div class="col-xs-12">
                  <div class="c-modal-header">
                      <div class="row">
                          <h1>Créer un nouvel album</h1>
                      </div>
                  </div>
                  <div class="row">
                      <div class="c-modal-form-content">
                         
                              <div class="col-xs-12 col-sm-8">
                                  <div class="row">
                                      <fieldset>
                                          <label for="">Nom de l'événement</label>
                                          <input type="text" name="title" class="form-control js-form-create-event-name" autofocus placeholder="Nom de l'événement">
                                      </fieldset>
                                      <div class="row">
                                          <div class="col-xs-12 col-sm-6">
                                              <fieldset>
                                                  <label for="">Date de début</label>
                                                  <input type="text" name="datetime_begin" class="form-control datepicker" autofocus>
                                              </fieldset>
                                          </div>
                                          <div class="col-xs-12 col-sm-6">
                                              <fieldset>
                                                  <label for="">Date de fin</label>
                                                  <input type="text" name="datetime_end" class="form-control datepicker" autofocus>
                                              </fieldset>
                                          </div>
                                          <div class="col-xs-12 col-sm-6">
                                              <fieldset>
                                                  <label for="">Heure de début</label>
                                                  <input type="text" name="" class="form-control timepicker" autofocus>
                                              </fieldset>
                                          </div>
                                          <div class="col-xs-12 col-sm-6">
                                              <fieldset>
                                                  <label for="">Heure de fin</label>
                                                  <input type="text" name="" class="form-control timepicker" autofocus>
                                              </fieldset>
                                          </div>
                                      </div>
                                      <fieldset>
                                          <label for="">Type d'événement</label>
                                          <select class="form-control" name="type_event">
                                                <?php foreach($eventTypes as $key => $event): ?>
                                              <option value="<?= $event ?>"><?= $event ?></option>
                                          <?php endforeach; ?>
                                          </select>
                                      </fieldset>
                                      <fieldset>
                                          <label for="">Descriptif (40 caractères max)</label>
                                          <textarea class="form-control" name="body" id="" cols="30" rows="10"></textarea>
                                      </fieldset>
                                  </div>
                              </div>
                              <div class="col-xs-12 col-sm-4">
                                  <a href="#" class="js-event-trigger-file modal-add-zone">
                                          <span class="icon icon-add-photo"></span>
                                          Ajouter une photos
                                  </a>
                                  <input type="file" name="image_url" id="photo-upload">
                              </div>
                      </div>
                  </div>
              </div>
              <div class="clearfix"></div>
              <hr>
              <div class="c-modal-footer">
                  <div class="pull-right">
                      <a class="button-md grey modal--close-event" href="">Annuler</a>
                      <button class="button-md red js-create-event-submit" onclick="submit();" href="">Créer l'événement</button>
                  </div>
                  <div class="clearfix"></div>
              </div>
          </div>
      </div>
  </div>

  <input type="hidden" value="" name="id" id="event-edit-id">
  <input type="hidden" value="/evenements/" name="redirect">
  <input type="hidden" value="lpcr_event_create" name="action">
  </form>


<?php 
} 
?>

<?php 
function lpcr_htmlize_month_calendar($calendar, $user) {
?>

    <div class="col-xs-12 col-sm-8 col-md-3 side">
    <a href="/calendrier"><div class="vue-cal-i"><img src="http://preprod.chaperonsetvous.fr/wp-content/themes/chaperons-vous-theme/img/vue-cal-i.png" alt="vue cal"></div><div class="vue-cal">Vue calendrier</div></a>
    <table id="wp-calendar" class="table"><thead class="caption"><tr><td colspan="1" id="prev"></td>
    <td colspan="5"><?php echo strftime("%B %Y"); ?></td><td colspan="1" id="next"></td>
    </tr></thead>
    <thead class="days">
    <tr>
        <th scope="col" title="lundi"><span class="cal-j">lu</span></th>
        <th scope="col" title="mardi"><span class="cal-j">ma</span></th>
        <th scope="col" title="mercredi"><span class="cal-j">me</span></th>
        <th scope="col" title="jeudi"><span class="cal-j">je</span></th>
        <th scope="col" title="vendredi"><span class="cal-j">ve</span></th>
        <th scope="col" title="samedi"><span class="cal-j">sa</span></th>
        <th scope="col" title="dimanche"><span class="cal-j">di</span></th>
    </tr>
    </thead>

    <tfoot>

    <?php

        $currentMonth=DATE("m");

        if ($currentMonth>="03" && $currentMonth<="05")
      $season = "Beau printemps";
    elseif ($currentMonth>="06" && $currentMonth<="08")
      $season = "Bel été";
    elseif ($currentMonth>="09" && $currentMonth<="11")
      $season = "Bel automne";
    else
      $season = "Bel Hiver";


    ?>
    <tr><td colspan="1"></td><td colspan="3" class="imgcal-img-td"><img src="http://preprod.chaperonsetvous.fr/wp-content/themes/chaperons-vous-theme/img/imgcal.png" alt="imgcal"></td><td colspan="3" class="imgcal-texte-td"><span class="imgcal-texte"><?= $season; ?></span></td>
    </tr>
    </tfoot>

    <tbody>

    <?php

    $currentYear = date('Y');
    $currentMonth = date('m');
    $day_dec =  date('N', strtotime($currentYear."-".$currentMonth."-01"));

    ?>


    <?php $i = 1;  $first = true;?>
    <?php foreach($calendar as $key => $day): ?>
        
        <?php if($i%7 == 1):  ?>
            <tr>
        <?php endif; ?>

            <?php if($first): ?>    
                    
               <?php for ($ii = 1; $ii <= $day_dec-1; $ii++): ?>
                     <td><span class="cal-j"></span></td>
                     <?php $i++;  ?>
               <?php endfor; ?>

            <?php $first = false; ?>
            <?php endif; ?>

            <?php if($day): ?>
           <td><a href="#event-<?= $day ?>"><?= $key ?></a></td>
            <?php else: ?>
           <td><span class="cal-j"><?= $key ?></span></td>
            <?php endif; ?>
    
        <?php if($i%7 == 0):  ?>
            </tr>
        <?php endif; ?>

    <?php $i++; ?>
    <?php endforeach; ?>
    </tbody>
    </table><!-- fonction calendrier wp avec quelques ajustements, voir functions.php -->
</div>

<?php 
} 
?> 
