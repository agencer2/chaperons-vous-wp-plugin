<?php

/**
 * Provide a menu in front
 * 
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */

function lpcr_menu() {
  
  $wp_session = WP_Session::get_instance();
  
  $arrayItems = array(
  	"fil-rouge"=> array(
      "title"=>"fil-rouge",
      "name"=>"Fil rouge",
      "url"=>get_permalink( get_page_by_path("fil-rouge")),
  	  "notification"=>0
    ),
    "albums"=> array(
      "title"=>"albums",
      "name"=>"Albums",
      "url"=>get_permalink( get_page_by_path("albums")),
      "notification"=>(!empty($wp_session["albums"])?$wp_session["albums"]:0)
    ),
    "articles"=> array(
      "title"=>"articles",
      "name"=>"Chaperons News",
      "url"=>get_permalink( get_page_by_path("articles")),
      "notification"=>(!empty($wp_session["articles"])?$wp_session["articles"]:0)
    ),
    "evenements"=> array(
      "title"=>"evenements",
      "name"=>"Événements",
      "url"=>get_permalink( get_page_by_path("evenements")),
      "notification"=>(!empty($wp_session["evenements"])?$wp_session["evenements"]:0)
    ),
    "documents"=> array(
      "title"=>"documents",
      "name"=>"Documents",
      "url"=>get_permalink( get_page_by_path("documents")),
      "notification"=>(!empty($wp_session["documents"])?$wp_session["documents"]:0)
    ),
    "equipe"=> array(
      "title"=>"equipe",
      "name"=>"Équipe",
      "url"=>get_permalink( get_page_by_path("equipe")),
      "notification"=>0
    ),
    "messages"=> array(
      "title"=>"messages",
      "name"=>"Messages",
      "url"=>get_permalink( get_page_by_path("messages")),
      "notification"=>(!empty($wp_session["messages"])?$wp_session["messages"]:0)
    )
  );

  // If no creche are selected, we keep only articles
  if(empty($_SESSION["active_creche"])) {
    $arrayMenu = array($arrayItems["articles"]);
  } else {
    $arrayMenu = $arrayItems;
  }
  ?>
  
  <div class="sidebar-nav">
  
  <?php
  $postName = get_post()->post_name;
  foreach($arrayMenu as $item=>$details) {
    
    if ($postName == $details['title'] && !empty($wp_session[$postName])) {
      unset($wp_session[$postName]);
      $wp_session[$postName] = 0;
    }
  
    $class = "";
    if ($item == "albums" && (get_permalink() == home_url()."/albums/" || get_permalink() == home_url()."/album-details/")) {
      $class = "active";
    } elseif ($item == "evenements" && (get_permalink() == home_url()."/evenements/" || get_permalink() == home_url()."/calendrier/")) {
      $class = "active";
    } elseif ($item == "articles" && (get_permalink() == home_url()."/articles/" || is_single())) {
      $class = "active";
    } elseif($details['url'] == get_permalink()) {
      $class = "active";
    }

    ?>
    <div class="menu-item">
    <?php
      echo '<a href="'.$details['url'].'" class="menu-'.$details['title'].' '.$class.'">'.$details['name'].'</a>';
      
    ?>
    
    <?php if($details['notification'] != 0): ?>
       <span class="menu-item__notification"><?= $details['notification']; ?></span>
    <?php endif; ?>
    </div>
    <?php
  }
  ?>
  </div>
  <?php
}