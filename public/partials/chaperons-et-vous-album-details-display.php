<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/class-chaperons-et-vous-photo.php';
/**
* Provide a public-facing view for the blog articles
*
* @link       http://www.r2.fr
* @since      1.0.0
*
* @package    Chaperons_Et_Vous
* @subpackage Chaperons_Et_Vous/public/partials
*/
function lpcr_htmlize_album($album)
{
$userRights = new UserRights();
$date = $album->get_date_add();
$date_fr = new DateTime($date);
$date_fr = $date_fr->format('d M Y');
?>
<div class="col-xs-12 col-sm-8 col-md-9" id="main-column">
    <main id="main" class="site-main">
    <div class="content-top"></div>
    <div class="cadre-top-title2">

        <div class="album-header">
            <div class="row">
                <div class="pull-left">
                    <a href="/albums" class="button-md grey">Revenir aux albums</a>
                </div>
                <div class="pull-right">
                    <?php if($userRights->getRights(UserRights::TYPE_ALBUM) >= UserRights::WRITE_RIGHTS): ?>
                        <a href="#" class="button-md green js-album-edit" data-album-id="<?= $album->get_id() ?>"><i class="fa fa-pencil"></i> Éditer / Modifier</a>
                    <?php endif; ?>
                    <?php 
                    if($album->get_publication() != 1 && $userRights->getRights(UserRights::TYPE_ALBUM) >= UserRights::WRITE_RIGHTS): ?>
                        <a href="#" class="button-md green js-album-aprove" data-album-id="<?= $album->get_id() ?>">Aprouver</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
   </div>

<div class="cadre-post cadre-cat">
    <div class="profil-post col-xs-12" >
            <?php if($album->get_publication() != 1): ?>
            <div class="row">
                <div class="notify">
                    Cet album est en attente de modération
                </div>
            </div> 
            <?php endif; ?>
            <div class="row album-image-list">
            <div class="pull-left">
                <h1 class="js-album-name"><?php echo $album->get_name()?></h1>
            </div>
            <div class="pull-right">
                <span class="date-info">Créé <abbr class="timeago" title="<?= $date ?>">le <?= $date_fr ?></abbr></span>
            </div>
            <div class="clearfix"></div>
            <?php if($album->photos): ?>
            <?php
            $index = 0;
            foreach($album->photos as $photo): ?>
            <?php
            list($width, $height, $type, $attr) = getimagesize(getcwd().Photo::GALLERY_FOLDER.$photo->path);
            ?>
            <div class="col-md-3 col-sm-6 col-xs-6 album-item-container" data-photo-id="<?php echo $photo->id ?>">
                <div class="album-item" data-photo-id="<?= $photo->id ?>">
                    <a class="album-item-link js-photoswipe" data-index="<?= $index ?>" data-width="<?= $width ?>" data-height="<?= $height ?>" href="<?php echo home_url().Photo::GALLERY_FOLDER.$photo->path; ?>">
                        <span class="album-item-img">
                            <img class="img-responsive" src="<?php echo home_url().Photo::GALLERY_FOLDER.Photo::THUMB_PREFIX.$photo->path; ?>" alt="">
                        </span>
                        <?php if($photo->name): ?>
                            <h4 class="js-photo-legende" data-photo-id="<?= $photo->id ?>">
                                <?= $photo->name ?>
                            </h4>
                        <?php endif; ?>
                    </a>
                    
                    <?php if (is_user_logged_in() && $userRights->hasEditDeleteRights(UserRights::TYPE_ALBUM)) { ?>
                    <div class="album-item-controls">
                        <ul>
                            <li><a href="#" class="js-album-delete-photo" data-photo-id="<?php echo $photo->id ?>"><i class="icon icon-trash"></i></a></li>
                        </ul>
                    </div>
                    <?php } ?>
                    
                </div>
            </div>
            <?php $index++; ?>
            <?php endforeach; ?>
            <?php else: ?>
                <hr>
               <div class="row">
                   <div class="col-xs-12">
                        <div class="text-center album-empty">
                            <img class="center-block" src="/wp-content/themes/chaperons-vous-theme/img/evenements-vide.png"/>
                            <h3>Cet album est vide</h3>
                            <p>Vous pouvez <a href="/wp-admin/admin-ajax.php?album_id=<?= $album->album_id ?>&action=lpcr_album_delete&redirect=/albums">archiver cet album</a></p>
                        </div>
                   </div>
               </div>
            <?php endif; ?>
        </div>
    </div>
</div>

    </main>
</div>
<?php } ?>