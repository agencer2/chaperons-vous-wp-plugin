<?php

/**
 * Provide a public-facing view for the blog articles
 * 
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */
?>

<?php 
function lpcr_htmlize_article($datetime, $post_id, $title, $body) {
?>

<div class='cadre-post col-xs-12 col-sm-12 col-md-12'>
	<div class='date-post hidden-xs'>
		Il y a <?php echo human_time_diff(strtotime($datetime), current_time('timestamp')); ?>
		<div class="cat-icon">
            <img src="<?php echo theme_root(); ?>/img/articles.svg">
        </div>
	</div>
	<br />
	<div class='post-title'>
		<h2>
			<a href="<?php echo get_permalink($post_id); ?>" rel='bookmark' title='Permanent Link to '><?php echo $title; ?></a>
		</h2>
	</div>
	<div class='excerpt'>
		<?php echo $body; ?>
	</div>
	<div class='clearfix'></div>
	<br />
	<div class='date-post visible-xs'>
		<br /><br />Il y a <?php echo human_time_diff(strtotime($datetime), current_time('timestamp')); ?>
	</div>
</div>

<?php } ?>
