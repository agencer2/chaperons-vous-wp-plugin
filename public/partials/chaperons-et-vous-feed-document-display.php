<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'models/class-chaperons-et-vous-photo.php';

/**
 * Provide a public-facing view for a new album in the feed
 * 
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/public/partials
 */
?>

<?php 
function lpcr_htmlize_feed_document($document) {
    $author = get_userdata($document->get_user_id());
    // var_dump($document);
    // var_dump($author);
?>
   <div class="cadre-post col-xs-12 col-sm-12 col-md-12">
        <div class="feed-document">
            <div class="row">
                <div class="pull-left">
                    <div class="feed-profile">
                        <div class="feed-profile__photo" style="background-image: url(/wp-content/themes/chaperons-vous-theme/img/profil-defaut.png);"></div>
                        <div class="feed-profile__name-container">
                            <span class="feed-profile__name"><?= ucwords($author->display_name) ?></span> 
                            <span class="feed-profile__creche"><?= ucwords('Crèche R2') ?></span> 
                        </div>
                    </div>
                </div>
                <div class="pull-right">
                     Il y a <?php echo human_time_diff(strtotime($document->get_date_add()), current_time('timestamp')); ?>
                </div>
            </div>
            <div class="row">
                <h3>Vous avez reçu un nouveau document</h3>
            </div>
            <div class="row">
                <div class="feed-item-document">
                    <a href="<?= $document->get_path() ?>" target="_blank" class="feed-item-document__title"><?= $document->get_name() ?></a>
                    <span class="feed-item-document__type hidden-sm hidden-xs">Fichier DOC</span>
                    <span class="feed-item-document__size hidden-sm hidden-xs"><?= $document->get_size() ?></span>
                    <a class="feed-item-document__download hidden-xs" href="<?= $document->get_path() ?>" target="_blank">
                        Télécharger
                    </a>
                </div>            
            </div>
        </div>
    </div>

<?php } ?>
