<?php
/**
* Provide a public-facing view for the blog articles
*
* @link       http://www.r2.fr
* @since      1.0.0
*
* @package    Chaperons_Et_Vous
* @subpackage Chaperons_Et_Vous/public/partials
*/
?>
<?php
function lpcr_htmlize_documents($documents, $array_categories, $rights) {
?>


<div class="col-xs-12 col-sm-8 col-md-9" id="main-column">
  <main id="main" class="site-main">
  <div class="content-top"></div>
  <?php
  $userRights = new UserRights();
  if(is_user_logged_in() && $userRights->hasCreateRights(UserRights::TYPE_DOCS) ) :
  ?>
  <div class="col-cs-12">
    <div class="row">
      <div class="pull-right"><a href="" class="button-md red js-create-document">Ajouter un fichier</a></div>
    </div>
  </div>
  <?php endif; ?>

  <?php foreach($documents as $category): ?>
  <div class="cadre-post-doc col-xs-12 col-sm-12 col-md-12">
    <div class="doc-icon">
      <img src="<?php echo theme_root()?>/img/doc-menu.svg" alt="Menus" data-no-retina />
    </div>
    <div class="doc-title"><?= $category->get_name() ?></div>
    <?php foreach($category->documents as $document): ?>
    <div class="doc-ligne">
      <div class="lien-fichier">
        <a href="<?= $document->get_path() ?>" target="_blank"><?= $document->get_name() ?></a>
      </div>
      <div class="type-fichier">Fichier <b><?= $document->get_format() ?></b></div>
      <div class="taille-fichier"><?= $document->get_human_size() ?></div>

      <?php if(!empty($userRights->hasCreateRights(UserRights::TYPE_DOCS))): ?>
        <div class="delete-fichier"><a href="#" class="js-delete-document" data-id="<?= $document->get_id(); ?>"><i class="glyphicon glyphicon-remove"></i> Supprimer le document</a></div>
      <?php endif; ?>
    </div>
  <?php endforeach; ?>
  </div><!-- cadre post -->
  <?php endforeach; ?>
    
<form action="/wp-admin/admin-ajax.php" method="post" enctype="multipart/form-data">
<div class="c-modal-binder">
  <div class="c-modal-overlay modal--create-document">
    <div class="c-modal-container">
      <div class="col-xs-12">
        <div class="c-modal-header">
          <div class="row">
            <h1>Publier un nouveau document</h1>
          </div>
        </div>
        <div class="row">
          <div class="c-modal-form-content">
            
            <div class="col-xs-12">
              <div class="row">
                <fieldset>
                  <label for="">Nom du document</label>
                  <input type="text" name="name" class="form-control" autofocus placeholder="Nom du document">
                </fieldset>
                <fieldset>
                  <label for="">Catégorie</label>
                  <select name="folder_id" id="" class="form-control">

                  <?php foreach($array_categories as $key => $categ): ?>
                    <option value="<?= $categ->id ?>"><?= $categ->name ?></option>
                  <?php endforeach; ?>
                  </select>
                </fieldset>
                <fieldset>
                  <label for="">Télécharger le document</label>
                  <input type="file" name="document" class="form-control">
                </fieldset>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <hr>
      <div class="c-modal-footer">
        <div class="pull-right">
          <a class="button-md grey modal--close-document" href="">Annuler</a>
          <button type="submit" onclick="submit();" class="button-md red js-create-document-submit">Publier</button>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<input type="hidden" name="action" value="lpcr_document_create">
<input type="hidden" name="description" value=" ">
<input type="hidden" name="redirect" value="/documents">
<input type="hidden" name="entity" value="1-<?= !empty($_SESSION['active_creche'])?$_SESSION['active_creche']->get_id():'' ?>">
<!-- <input type="hidden" name="redirect" value="/"> -->
</form>


    <?php } ?>

