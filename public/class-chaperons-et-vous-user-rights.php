<?php

class UserRights
{
    
    protected $user;
    protected $user_creche_list;
    protected $active_creche;
    
    const NO_RIGHTS = 0;
    const READ_RIGHTS = 1;
    const SUBMISSION_RIGHTS = 2;
    const WRITE_RIGHTS = 3;
    
    const TYPE_FEED = 1;
    const TYPE_ALBUM = 2;
    const TYPE_EVENT = 3;
    const TYPE_DOCS = 4;
    const TYPE_NEWS = 5;
    const TYPE_TEAM = 6;
    const TYPE_MESSENGER = 7;
    const TYPE_STATS = 8;
    
    const ROLE_PARENT = "parent";
    const ROLE_LIMITEDCONTRIB = "limitedContrib";
    const ROLE_CONTRIB = "contrib";
    const ROLE_EDITOR = "editor";
    const ROLE_MULTICONTRIB = "multicontrib";
    const ROLE_MULTIEDITOR = "multieditor";
    const ROLE_ADMIN = "administrator";
    
    public function __construct($user=null){
        if (!empty($_SESSION["current_user"])) {
          $this->user = $_SESSION["current_user"];
        }
        if (!empty($_SESSION["creches"])) {
          $this->user_creche_list = $_SESSION["creches"];
        }
        if (!empty($_SESSION["active_creche"])) {
          $this->active_creche = $_SESSION["active_creche"];
        }
    }

    // getters
    public function get_user() { return $this->user; }
    public function get_user_creche_list() { return $this->user_creche_list; }
    public function get_active_creche() { return $this->active_creche; }
 
    
    public function select_rights()
    {    
      if (is_user_logged_in()) {
        global $wpdb;
    
        $arrayRights = array();
        $rights = $this->hasCreateRights(self::TYPE_ALBUM);
        if ($rights >= self::WRITE_RIGHTS) {
          $arrayRights["creche"] = array(
          	"name" => "Crèche",
            "data" => array(
              array(
                "name"=>"Toute la crèche",
                "value" => Entity::VISIBILITY_CRECHE."-".$this->active_creche->get_id()
              )
            ) 
          );
        }
        
        // Load allowed sections
        $arraySections = array();
        if(!empty($this->active_creche)) {
          $sections = $this->active_creche->get_sections();
          
          if(!empty($sections)) {
            foreach ($sections as $id_section=>$name_section) {
              $arraySections[] = array(
                "name" => $name_section,
                "value" => Entity::VISIBILITY_SECTION."-".$id_section
              );
            }
          }
        }
        
        if(!empty($arraySections)) {
          $arrayRights["sections"] = array(
            "name" => "Sections",
            "data" => $arraySections
          );
        }
        
        
        // Load allowed children in creche OR section according to the rights
        $conditions = array();
        if ($this->user->roles[0] == UserRights::ROLE_LIMITEDCONTRIB) {
          if(!empty($this->active_creche)) {
            $conditions[] = "id_section IN (".implode(",",array_keys($this->active_creche->get_sections())).")";
          }
        } else {
          if(!empty($this->active_creche)) {
            $conditions[] = "id_creche = ".$this->active_creche->get_id();
          }
        }
        
        $query = "
          SELECT {$wpdb->prefix}app_children.id, {$wpdb->prefix}app_children.first_name, {$wpdb->prefix}app_children.last_name
          FROM {$wpdb->prefix}app_children ";
        if (!empty($conditions)) {
          $query .= "WHERE ".implode(" AND ", $conditions);
        }
        
        $results = $wpdb->get_results($query);
        
        $arrayChildren = array();
        foreach($results as $result) {
          $arrayChildren[] = array(
            "name" => $result->first_name." ".$result->last_name,
            "value" => Entity::VISIBILITY_CHILD."-".$result->id
          );
        }
        
        if (!empty($arrayChildren)) {
          $arrayRights["childrens"] = array(
            "name" => "Enfants",
            "data" => $arrayChildren
          );
        }
        
        return $arrayRights;
      }
    }
    
    
    // check if the creche is in the list of user's creches
    public function isMyCreche($id_creche) {
        $toReturn = false;
        if (in_array($id_creche, $this->user_creche_list));
           return true;
        return $toReturn;
    }

    public function hasCreateRights($entity_type) {
      $rights = $this->getRights($entity_type);
      if($rights > self::READ_RIGHTS) {
        return true;
      } else {
        return false;
      }
    }
    
    public function canModerate($entity_type) {
      $rights = $this->getRights($entity_type);
      if($rights > self::SUBMISSION_RIGHTS) {
        return true;
      } else {
        return false;
      }
    }
    
    public function getRights($entity_type) {
      // Does the user has the right to create an object?
      $role = $this->user->roles[0];
      
      switch ($entity_type) {
      	case self::TYPE_EVENT:
      	  if($role != self::ROLE_PARENT) {
      	    if(!empty($this->active_creche) && $this->isMyCreche($this->active_creche->get_id())) {
      	      return self::WRITE_RIGHTS;
      	    }
      	  } else {
      	    return self::READ_RIGHTS;
      	  }
      	break;
      	
      	case self::TYPE_ALBUM:
      	case self::TYPE_FEED:
      	  if($role == self::ROLE_PARENT) {
      	    return self::READ_RIGHTS;
      	  } elseif($role == self::ROLE_LIMITEDCONTRIB || $role == self::ROLE_CONTRIB || $role == self::ROLE_MULTICONTRIB) {
      	    if(!empty($this->active_creche) && $this->isMyCreche($this->active_creche->get_id())) {
      	      return self::SUBMISSION_RIGHTS;
      	    }
      	  } else {
      	    if(!empty($this->active_creche) && $this->isMyCreche($this->active_creche->get_id())){
      	      return self::WRITE_RIGHTS;
      	    }
      	  }
    	  break;
      	
    	  case self::TYPE_DOCS:
    	  case self::TYPE_TEAM:
    	    if($role == self::ROLE_PARENT || $role == self::ROLE_LIMITEDCONTRIB) {
    	      return self::READ_RIGHTS;
    	    } else {
    	      return self::WRITE_RIGHTS;
    	    }
  	    break;
  	    
    	  case self::TYPE_NEWS:
    	    if($role == self::ROLE_ADMIN) {
    	      return self::WRITE_RIGHTS;
    	    } else {
    	      return self::READ_RIGHTS;
    	    }
    	  break;
    	  
    	  case self::TYPE_MESSENGER:
    	    if($role != self::ROLE_LIMITEDCONTRIB) {
    	      return self::WRITE_RIGHTS;
    	    } else {
    	      return self::NO_RIGHTS;
    	    }
  	    break;
  	    
  	    case self::TYPE_STATS:
  	      if($role != self::ROLE_LIMITEDCONTRIB && $role != self::ROLE_PARENT) {
    	      return self::READ_RIGHTS;
    	    } else {
    	      return self::NO_RIGHTS;
    	    }
	      break;
    	  
      	default:
      		return false;
      	break;
      }
    }
    
    public function hasEditDeleteRights() {
      //Does the user has the right to edit or delete this object?
      if ($this->user->roles[0] == self::ROLE_ADMIN) {
        return true;
      }
      $user = $_SESSION["current_user"];
      $id_creche = $this->active_creche->get_id();
      $role = $user->roles[0];
      $permission = false;
      switch ($role) {
      	case UserRights::ROLE_PARENT:
      	  break;
      	case UserRights::ROLE_LIMITEDCONTRIB:
      	  if($user->ID == intval($this->user->get_id()))
      	    $permission = true;
      	  break;
      	case UserRights::ROLE_CONTRIB:
      	case UserRights::ROLE_EDITOR:
      	case UserRights::ROLE_MULTICONTRIB:
      	case UserRights::ROLE_MULTIEDITOR:
      	  $permission = $this->isMyCreche($id_creche);
      	  break;
      	case UserRights::ROLE_ADMIN:
      	  $permission = true;
      	  break;
      }
      return $permission;
    }

}
