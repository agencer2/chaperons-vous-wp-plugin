<?php
/**
 * Fired during plugin activation
 *
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/includes
 * @author     Agence R2 <contact@r2.fr>
 */
class Chaperons_Et_Vous_Activator {

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate() {
        self::add_lpcr_roles();
        self::add_lpcr_db_tables();
        self::addUsers();
    }

    private static function add_lpcr_roles() {
        add_role( UserRights::ROLE_PARENT, 'Parent',
            array(
                'read' => true,
                'edit_posts' => false,
                'create_posts' => false,
                'delete_posts' => false,
                'level_0' => true,
                'show_admin_bar' => false
            )
        );
        add_role( UserRights::ROLE_LIMITEDCONTRIB, 'Contributeur limité',
            array(
                'read' => true,
                'edit_posts' => false,
                'create_posts' => true,
                'delete_posts' => false,
                'level_0' => true
            )
        );
        add_role( UserRights::ROLE_CONTRIB, 'Directeur contributeur',
            array(
                'read' => true,
                'edit_posts' => false,
                'create_posts' => true,
                'delete_posts' => false,
                'level_0' => true
            )
        );
        add_role( UserRights::ROLE_EDITOR, 'Directeur éditeur',
            array(
                'read' => true,
                'edit_posts' => true,
                'create_posts' => true,
                'delete_posts' => true,
                'level_0' => true
            )
        );
        add_role( UserRights::ROLE_MULTICONTRIB, 'Contributeur multi-crèches',
            array(
                'read' => true,
                'edit_posts' => true,
                'create_posts' => true,
                'delete_posts' => true,
                'level_0' => true
            )
        );
        add_role( UserRights::ROLE_MULTIEDITOR, 'Editeur multi-crèches',
            array(
                'read' => true,
                'edit_posts' => false,
                'create_posts' => true,
                'delete_posts' => false,
                'level_0' => true
            )
        );
        add_role( UserRights::ROLE_MULTICONTRIB, 'Coordinateur',
          array(
            'read' => true,
            'edit_posts' => false,
            'create_posts' => true,
            'delete_posts' => false,
            'level_0' => true
          )
        );
        add_role( UserRights::ROLE_ADMIN, 'Siège',
          array(
            'read' => true,
            'edit_posts' => false,
            'create_posts' => true,
            'delete_posts' => false,
            'level_0' => true
          )
        );
    }

    public static function add_lpcr_db_tables() {
        global $wpdb;
        // Alter tables to sync LPCR users data
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}users` ADD `{$wpdb->prefix}ID` BIGINT(20) NOT NULL AFTER `ID`;");
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}users` ADD `{$wpdb->prefix}status` VARCHAR(20) NOT NULL AFTER `{$wpdb->prefix}ID`;");
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}users` ADD `{$wpdb->prefix}cgu_accept` TINYINT(1) DEFAULT 0 NOT NULL AFTER `{$wpdb->prefix}status`;");
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}users` ADD `{$wpdb->prefix}email_accept` TINYINT(1) DEFAULT 0 NOT NULL AFTER `{$wpdb->prefix}cgu_accept`;");
        $wpdb->query("ALTER TABLE `{$wpdb->prefix}users` ADD `{$wpdb->prefix}photo_accept` TINYINT(1) DEFAULT 0 NOT NULL AFTER `{$wpdb->prefix}email_accept`;");
        // Create LPCR core tables
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_creches (id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT, name VARCHAR(100) NOT NULL, country VARCHAR(50) NOT NULL, region VARCHAR(50) NOT NULL, department_number VARCHAR(5) NOT NULL, post_code VARCHAR(10) NOT NULL, city VARCHAR(50) NOT NULL, address VARCHAR(255) NOT NULL, phone VARCHAR(15), email VARCHAR(50), company VARCHAR(100) NOT NULL, siret VARCHAR(14), opening_date DATE NOT NULL, opening_time TIME NOT NULL, closing_time TIME NOT NULL, nature VARCHAR(11) NOT NULL, nb_sections INT NOT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_sections (id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT, name VARCHAR(10) NOT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_asso_creches_section (id_creche INT(11) NOT NULL, id_section INT(11) NOT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_children (id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(50) NOT NULL, birth_date DATE NOT NULL, birth_date_publication TINYINT(1) UNSIGNED NOT NULL, gender VARCHAR(1) NOT NULL, id_creche INT(11) NOT NULL, id_section INT(11) NOT NULL, contract_end_date DATE NOT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_asso_parents_children (id_child INT(11) NOT NULL, id_parent INT(11) NOT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_asso_employees_creches (id_user INT(11) NOT NULL, id_creche INT(11) NOT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        //$wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_asso_employees_sections (`id` int(10) unsigned NOT NULL AUTO_INCREMENT, `id_user` int(10) unsigned NOT NULL, `id_section` int(10) unsigned NOT NULL, PRIMARY KEY (`id`)) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        // Create Messenger tables
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_messages (id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT, conversation_id BIGINT(20) NOT NULL, sender_id BIGINT(20) NOT NULL, recipient_id BIGINT(20) NOT NULL, sent_date DATETIME NOT NULL, body LONGTEXT NOT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_conversations (id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT, subject varchar(140) NOT NULL, user1 bigint(20) DEFAULT NULL, user2 bigint(20) DEFAULT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        // Create Evenements tables
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_evenements (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `title` varchar(140) NOT NULL, `image_url` varchar(200) DEFAULT NULL, `body` text NOT NULL, `entity_type` tinyint(1) unsigned NOT NULL, `entity_id` int(10) unsigned NOT NULL, `datetime_begin` datetime NOT NULL, `datetime_end` datetime NOT NULL, `user_id` int(11) NOT NULL, `creche_id` int(11) NOT NULL, `type_event` varchar(140) NOT NULL, `active` int(1) NOT NULL DEFAULT '0', date_add DATETIME NOT NULL, PRIMARY KEY (`id`)) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_evenements_participation (id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT, id_event INT(11) NOT NULL, id_user INT(11) NOT NULL, answer TINYINT(1) NOT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        // Create Albums tables 
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_albums (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, `name` VARCHAR(100) NOT NULL, `publication` TINYINT(1) UNSIGNED NOT NULL, `user_id` INT UNSIGNED NOT NULL, `creche_id` INT UNSIGNED NOT NULL, `entity_type` TINYINT(1) UNSIGNED NOT NULL, `entity_id` INT UNSIGNED NOT NULL, `date_add` DATETIME NOT NULL, PRIMARY KEY (`id`)) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_photos (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, `album_id` INT UNSIGNED NOT NULL, `name` VARCHAR(250) NULL DEFAULT NULL, `path` TEXT NOT NULL, `video` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, `date_add` DATETIME NOT NULL, PRIMARY KEY (`id`)) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        // Create Documents tables
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_folders (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, `name` VARCHAR(100) NOT NULL, `publication` TINYINT(1) UNSIGNED NOT NULL, `date_add` DATETIME NOT NULL, PRIMARY KEY (`id`)) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_documents (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, `folder_id` INT UNSIGNED NOT NULL, `name` VARCHAR(100) NOT NULL, `description` TEXT NULL DEFAULT NULL, `path` TEXT NOT NULL, `type` TINYINT(1) UNSIGNED NOT NULL, `size` INT UNSIGNED NOT NULL, `user_id` INT UNSIGNED NOT NULL, `creche_id` INT UNSIGNED NOT NULL, `entity_type` TINYINT(1) UNSIGNED NOT NULL, `entity_id` INT UNSIGNED NOT NULL, `date_add` DATETIME NOT NULL, PRIMARY KEY (`id`)) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        // Create Feed tables
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_feed_status (`id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT, `author_id` INT UNSIGNED NOT NULL, `creation_date` DATETIME NOT NULL, `body` VARCHAR(255) NOT NULL, `ìd_creche` INT UNSIGNED NOT NULL) CHARACTER SET utf8 COLLATE utf8_general_ci;");
        // Create team table
        $wpdb->query("CREATE TABLE IF NOT EXISTS {$wpdb->prefix}app_team (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, `first_name` VARCHAR(150) NOT NULL, `last_name` VARCHAR(150) NOT NULL, `position` VARCHAR(100) NULL, `description` TEXT NULL, `photo` VARCHAR(250) NULL, `id_creche` INT UNSIGNED NOT NULL, `id_section` INT UNSIGNED NOT NULL, PRIMARY KEY (`id`)) CHARACTER SET utf8 COLLATE utf8_general_ci;");
    }
    
    public static function addUsers()
    {
      $directeur1 = create_user('directeur1', 'mdpmdp', 'directeur1@mail.com');
      $directeur1 = new WP_User($directeur1);
      $directeur1->set_role(UserRights::ROLE_EDITOR);
      
      $directeur2 = create_user('directeur2', 'mdpmdp', 'directeur2@mail.com');
      $directeur2 = new WP_User($directeur2);
      $directeur2->set_role(UserRights::ROLE_EDITOR);
      
      $eje1 = create_user('eje1', 'mdpmdp', 'eje1@mail.com');
      $eje1 = new WP_User($eje1);
      $eje1->set_role(UserRights::ROLE_LIMITEDCONTRIB);
      
      $hsangan = create_user('hsangan', 'mdpmdp', 'hsangan@mail.com');
      $hsangan = new WP_User($hsangan);
      $hsangan->set_role(UserRights::ROLE_ADMIN);
      
      $parent1 = create_user('parent1', 'mdpmdp', 'parent1@mail.com');
      $parent1 = new WP_User($parent1);
      $parent1->set_role(UserRights::ROLE_PARENT);
      
      $parent2 = create_user('parent2', 'mdpmdp', 'parent2@mail.com');
      $parent2 = new WP_User($parent2);
      $parent2->set_role(UserRights::ROLE_PARENT);
      
      $parent3 = create_user('parent3', 'mdpmdp', 'parent3@mail.com');
      $parent3 = new WP_User($parent3);
      $parent3->set_role(UserRights::ROLE_PARENT);
      
      $parent4 = create_user('parent4', 'mdpmdp', 'parent4@mail.com');
      $parent4 = new WP_User($parent4);
      $parent4->set_role(UserRights::ROLE_PARENT);
      
      $siege = create_user('siege', 'mdpmdp', 'siege@mail.com');
      $siege = new WP_User($siege);
      $siege->set_role(UserRights::ROLE_ADMIN);
    }
}
