<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/includes
 * @author     Agence R2 <contact@r2.fr>
 */
class Chaperons_Et_Vous_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
	  /*
	   * Old roles
	   */
	  remove_role('hq');
	  remove_role('coordinator');
	  remove_role('director');
	  remove_role('deputydirector');
	  remove_role('eje');
	  
	  remove_role(UserRights::ROLE_PARENT);
	  remove_role(UserRights::ROLE_LIMITEDCONTRIB);
	  remove_role(UserRights::ROLE_CONTRIB);
	  remove_role(UserRights::ROLE_EDITOR);
	  remove_role(UserRights::ROLE_MULTICONTRIB);
	  remove_role(UserRights::ROLE_MULTIEDITOR);
	  remove_role(UserRights::ROLE_MULTICONTRIB);
	  remove_role(UserRights::ROLE_ADMIN);
	}

}
