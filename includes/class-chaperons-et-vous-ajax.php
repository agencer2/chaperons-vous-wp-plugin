<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/includes
 * @author     Agence R2 <contact@r2.fr>
 */
class Chaperons_Et_Vous_Ajax {

    public function __construct() {

        $this->plugin_name = 'chaperons-et-vous';
        $this->version = '1.0.0';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $public_hooks = $this->define_public_hooks();

        $this->load_packages($public_hooks);
    }
    
    public static function generateResponse($status, $message)
    {
      if(!empty($_REQUEST['redirect'])){
        header('Location: '.$_REQUEST['redirect'], true, 302);
      }
      $arrayResponse = array(
      	"status"=>$status,
        "message"=>$message
      );
      
      echo json_encode($arrayResponse);
      die();
    }

}
