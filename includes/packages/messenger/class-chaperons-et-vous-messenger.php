<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-message.php';
/**
* 
*/
class Chaperons_Et_Vous_Messenger
{
    private $public_data;
    private $messages = array();
    
    public function __construct($public_hooks)
    {
        // Store the public data
        $this->public_data = $public_hooks;
        // Register the AJAX handler
        add_action('wp_ajax_lpcr_messenger_create_message', array($this, 'create_conversations_from_ajax_request'));
        // Get the messages for the current user
        $this->crawl_and_sort_messages();
        // Declare the shortcode
        add_shortcode('show_composer', array($this, 'shortcode_composer'));
        add_shortcode('show_messages', array($this, 'shortcode_messages'));
    }

    /**
     * Getters
     */
    public function get_messages() { return $this->messages; }

    /**
     * Handle the AJAX request to create a new conversation
     */
    public function create_conversations_from_ajax_request() {
        // Get data of each recipient
        $recipients_list = array();
        if (is_array($_POST['recipients'])) {
            foreach ($_POST['recipients'] as $email) {
                // Get user data
                $user = get_user_by('email',$email);
                // Store user ID and email
                array_push($recipients_list, array($user->data->ID, $email));
            }
        } else {
            $user = get_user_by('email',$_POST['recipients']);
            array_push($recipients_list, array($user->data->ID, $_POST['recipients']));
        }
        // Create a new convesation
        $this->create_conversation($recipients_list, $_POST['object'], $_POST['message']);
        wp_die();
    }

    /**
     * Create a new conversation, store the first message, and send a email notification
     * $recipients_list is an array -> [[user_id, user_email], [user_id, user_email]]
     */
    public function create_conversation($recipients_list, $object, $content) {
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/models/class-chaperons-et-vous-conversation.php';
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/models/class-chaperons-et-vous-message.php';
        // foreach recipient
        foreach ($recipients_list as $recipient) {
            // Create and save a new conversation
            $conversation = new Conversation($object, $this->public_data->get_user()->ID, $recipient[0]);
            $conversation_id = $conversation->save();
            // Create and save the content as the first message
            $message = new Message($conversation_id, $this->public_data->get_user()->ID, $recipient[0], date("Y-m-d H:i:s"), $content);
            $message->save();
            // Send an email notification
            $this->send_email_notification($recipient[1]);
        }
    }

    /**
     * 
     */
    private function send_email_notification($recipient) {
      $mail = new PHPMailer();
      $mail->isSMTP();
      $mail->Host = MAIL_HOST;
      $mail->SMTPAuth = true;
      $mail->Username = MAIL_USERNAME;
      $mail->Password = MAIL_PASSWORD;
      $mail->SMTPSecure = 'tls';
      $mail->Port = MAIL_PORT;
      
      $mail->CharSet = 'UTF-8';
      
      $mail->setFrom($this->public_data->get_user()->user_email, 'Les Petits Chaperons Rouges');
      $mail->addAddress($recipient);
      $mail->isHTML(true);
      
      $mail->Subject = 'Vous avez reçu un nouveau message';
      $mail->Body    = 'Connectez vous sur <a href="'.site_url().'/">Les Petits Chaperons Rouges</a> pour le consulter.';
      $mail->AltBody = 'Connectez vous sur Les Petits Chaperons Rouges pour le consulter.';
      
      if(!$mail->send()) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de l'envoi du message");
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
      }
    }

    public function crawl_and_sort_messages() {
        if (is_user_logged_in()) {
            global $wpdb;
            $messages = Message::fetchUserMessages($this->public_data->get_user()->ID);
            foreach ($messages as $message) {
                $conversation = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_conversations` WHERE `id` = ".$message->conversation_id.";");
                array_push($this->messages, array('datetime' => $message->sent_date, 'conversation' => $conversation[0]->subject, 'message' => $message));
            }

            // Sort the messages array
            usort($this->messages, function($a, $b) {
                return strtotime($b['datetime']) - strtotime($a['datetime']);
            });
        }
    }

    public function shortcode_composer($atts, $content) {
        // Init default values
        $atts = shortcode_atts(array(), $atts);
        // Display the form
        $this->display_send_message_form();
    }
    
    public function shortcode_messages($atts, $content) {
      // Init default values
      $atts = shortcode_atts(array(), $atts);
      // Display the messages
      $this->display_messages();
    }

    private function display_send_message_form() {
      global $wpdb;
      require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-messenger-display.php';
      // Define the recipients' list
      $recipients_list = array();
      if ($this->public_data->get_user()->roles[0] == UserRights::ROLE_PARENT) {
        $listAdmins = $_SESSION["active_creche"]->get_admins();
        foreach ($listAdmins as $id_admin){
          // Load information about user
          $metas = get_userdata($id_admin);
          if($metas->roles[0] == UserRights::ROLE_EDITOR) {
            // Fill the recipients list with the active creche email address
            array_push($recipients_list, array($metas->ID, $this->public_data->user_active_creche->get_name(), $metas->data->user_email));
          }
        }
        
        // Display the form
        lpcr_messenger_form($this->public_data->get_user(), $recipients_list);
        return;
      } elseif ($this->public_data->get_user()->roles[0] != UserRights::ROLE_LIMITEDCONTRIB) {
        $parents_list = array();
        // Fill the recipients list with the active creche email address
        // get all the children in the creche
        $childrens = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."app_children` WHERE `id_creche` = ".$this->public_data->user_active_creche->get_id().";");
        // For each child
        foreach ($childrens as $child) {
          // Get his parents id
          $parents_id = $wpdb->get_results("SELECT `id_parent` FROM `".$wpdb->prefix."app_asso_parents_children` WHERE `id_child` = ".$child->id.";");
          // For each parent id
          foreach ($parents_id as $parent_id) {
            // If it is not already listed in $parents_list
            if (!array_key_exists($parent_id->id_parent, $parents_list)) {
              // Get the parent data
              $the_parent = get_userdata($parent_id->id_parent);
              // List him
              $parents_list[$parent_id->id_parent] = $the_parent;
              // And fill the recipients list with this parent ID, name and email address
              array_push($recipients_list, array($parent_id->id_parent, $the_parent->display_name, $the_parent->user_email));
            }
          }
        }
        // Display the form
        lpcr_messenger_form($this->public_data->get_user(), $recipients_list);
      } else {
        return;
      }
    }

    private function display_messages() {
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-messenger-display.php';
        foreach ($this->messages as $message) {
            global $wpdb;
            $contact = $wpdb->get_row("SELECT * FROM `".$wpdb->prefix."users` WHERE id = ".$message['message']->sender_id.";");
            lpcr_htmlize_message($contact, $message['datetime'], $message['conversation'], $message['message']->body);
        }
    }

}
