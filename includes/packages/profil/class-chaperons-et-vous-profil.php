<?php
/**
 *
 */
class Chaperons_Et_Vous_Profil
{
    /**
     * Details about the connected user
     * @var stdClass
     */
    private $public_data;

    public function __construct($public_hooks)
    {
        // Store the public data
        $this->public_data = $public_hooks;


        add_shortcode('mon_profil', array($this, 'shortcode_mon_profil'));


        // Declare ajax action
        add_action('wp_ajax_lpcr_profile_deactivate', array($this, 'deactivate_profile_from_ajax_request'));
        add_action('wp_ajax_lpcr_profile_change_birthday', array($this, 'switch_birthday_auth_from_ajax_request'));
        add_action('wp_ajax_lpcr_profile_accept_cgu', array($this, 'accept_cgu_from_ajax_request'));
    }

    public function accept_cgu_from_ajax_request()
    {
        global $wpdb;
        $wpdb->update($wpdb->prefix."users", array($wpdb->prefix."cgu_accept"=>1), array("ID"=>$this->public_data->get_user()->ID));
        Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
    }

    public function deactivate_profile_from_ajax_request()
    {
        // Only parents can deactivate their account
        if($this->public_data->get_user()->roles[0] == UserRights::ROLE_PARENT) {
            global $wpdb;
            $wpdb->update($wpdb->prefix."users", array("user_pass"=>""), array("ID"=>$this->public_data->get_user()->ID));

            // Force disconnect user
            wp_logout();

        }
        wp_redirect(home_url(), 301);
    }

    public function switch_birthday_auth_from_ajax_request()
    {
        if(!isset($_POST['birth_date_publication']) || !is_numeric($_POST['birth_date_publication'])) {
            Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez préciser le paramètre.");
        }
        // Only parents can do this
        if($this->public_data->get_user()->roles[0] == UserRights::ROLE_PARENT) {
            global $wpdb;
            $arrayChildren = $this->public_data->get_children();
            foreach($arrayChildren as $child) {
                $child->set_birth_date_publication(intval($_POST['birth_date_publication']));
                $child->save();
            }
            Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
        } else {
            Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires afin de modifier cette information");
        }
    }

    public function shortcode_mon_profil($atts, $content)
    {
        // Init default values
        $atts = shortcode_atts(array(), $atts);

        $this->display_mon_profil();


    }

    /**
     * Load the view that display the team on the web interface
     */
    private function display_mon_profil() {
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-profil-display.php';
        lpcr_htmlize_mon_profil(0);
    }


}