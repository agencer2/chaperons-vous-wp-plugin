<?php

require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-status.php';

/**
* 
*/
class Chaperons_Et_Vous_Feed
{

    private $public_data;
    private $package;
    private $feed_items = array();

    public function __construct($packages, $public_hooks)
    {
        if (is_user_logged_in()) {
            $this->public_data = $public_hooks;
            $this->package = $packages;
            add_action('wp_ajax_lpcr_feed_create_status', array($this, 'create_status_from_ajax_request'));
            // Register the shortcode
            add_shortcode('show_feed', array($this, 'shortcode'));
        }
    }

    /**
     * Getters
     */
    public function get_feed_items() { return $this->feed_items; }

    /**
     * Feed and sort the feed
     */
    public function feed_and_sort_the_feed($packages) {
        // "Feed the feed" array with the active creche's admins status
        $this->crawl_status();
        // "Feed the feed" array with the albums
        $this->crawl_albums($packages['albums']);
        // "Feed the feed" array with the articles
        $this->crawl_articles();
        // "Feed the feed" array with the events
        $this->crawl_events($packages['evenement']);
        // "Feed the feed" array with the docs
        $this->crawl_documents($packages['documents']);
        // DISABLED - "Feed the feed" array with the messages
        // $this->crawl_messages($packages['messenger']);
        // Sort the feed
        usort($this->feed_items, function($a, $b) {
            return strtotime($b['datetime']) - strtotime($a['datetime']);
        });
    }

    /**
     * Crawl the director and deputy director status
     */
    private function crawl_status() {
        global $wpdb;
        
        // Prepare to query the status table
        $query = "SELECT * FROM ".$wpdb->prefix."app_feed_status";
        $cond = array();
        if (isset($_SESSION['active_creche'])) {
            $query .= " WHERE `id_creche` = ".$_SESSION['active_creche']->get_id();
        }
        else
        {
            return;
        }
        
        // Execute the query
        $query_results = $wpdb->get_results($query);
        // "Feed the feed" array with the active creche's admins status
        foreach ($query_results as $status) {
            array_push($this->feed_items, array('type' => 'status', 'datetime' => $status->creation_date, 'object' => $status));
        }
    }

    public function create_status_from_ajax_request() {
        $this->create_status($_POST['status']);
        Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
    }

    public function create_status($body) {
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/models/class-chaperons-et-vous-status.php';
        $status = new Status($this->public_data->get_user()->ID, date("Y-m-d H:i:s"), $body, $_SESSION['active_creche']->get_id());
        $status->save();
    }

    /**
     * Crawl the albums
     */
    public function crawl_albums($albums) {
        $albums->set_albums_list();
        foreach ($albums->get_albums() as $album) {
            $albums->get_album_details($album->id);
            $album_data = $albums->get_album();
            array_push($this->feed_items, array('type' => 'album', 'datetime' => $album->date_add, 'object' => $album_data));
        }
    }
    
    /**
     * Crawl the events
     */
    public function crawl_events($eventsPackage) {
      $eventsPackage->set_evenements_list();
      $eventsList = $eventsPackage->get_evenements();
      foreach($eventsList as $month => $days) {
        foreach($days as $events) {
          foreach($events as $event) {
            // We only push real events, who have a real ID, not anniversary
            if(is_numeric($event->get_id())) {
              array_push($this->feed_items, array('type' => 'event', 'datetime' => $event->get_date_add(), 'object' => $event));
            }
          }
        }
      }
    }
    
    /**
     * Crawl the events
     */
    public function crawl_documents($documentsPackage) {
      $documentsPackage->set_documents_list(null, null);
      $documentsList = $documentsPackage->get_documents();
      foreach($documentsList as $folder) {
        foreach($folder as $documents) {
          foreach($documents as $document) {
            $document->folder_name = $folder->get_name();
            array_push($this->feed_items, array('type' => 'document', 'datetime' => $document->get_date_add(), 'object' => $document));
          }
        }
      }
    }

    /**
     * Crawl the articles
     */
    private function crawl_articles()
    {
        $args = array(
            'category_name'    => 'articles',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'post_type'        => 'post',
            'post_status'      => 'publish',
            'suppress_filters' => true
        );

        foreach (get_posts($args) as $article) {
            array_push($this->feed_items, array('type' => 'article', 'datetime' => $article->post_date, 'object' => $article));
        }
    }

    /**
     * Crawl the messages
     */
    private function crawl_messages($messenger) {
        $messages = $messenger->get_messages();

        foreach ($messages as $message) {
            array_push($this->feed_items, array('type' => 'message', 'datetime' => $message['datetime'], 'object' => $message));
        }
    }

    public function shortcode($atts, $content) {
        // Init default values
        $atts = shortcode_atts(array(), $atts);
        // Feed and sort the feed
        $this->feed_and_sort_the_feed($this->package);
        // Display the new status form
        $this->display_create_status_form();
        // Display the feed
        $this->display_feed();
    }

    private function display_create_status_form() {
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-status-display.php';
        if ($_SESSION['current_user']->roles[0] != UserRights::ROLE_PARENT) {
          lpcr_status_form();
        }
    }

    private function display_feed() {
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-status-display.php';
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-feed-album-display.php';
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-feed-event-display.php';
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-feed-document-display.php';
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-blog-display.php';
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-messenger-display.php';
        foreach ($this->feed_items as $item) {
            switch ($item['type']) {
                case 'status':
                    lpcr_htmlize_status($item['object']);
                    break;
                case 'event':
                  lpcr_htmlize_feed_event($item['object']);
                  break;
                case 'document':
                  lpcr_htmlize_feed_document($item['object']);
                  break;
                case 'album':
                    lpcr_htmlize_feed_album($item['object']);
                    break;
                case 'article':
                    lpcr_htmlize_article($item['datetime'], $item['object']->ID, $item['object']->post_title, $item['object']->post_content);
                    break;
                case 'message':
                    global $wpdb;
                    $contact = $wpdb->get_row("SELECT * FROM `".$wpdb->prefix."users` WHERE id = ".$item['object']['message']->sender_id.";");
                    lpcr_htmlize_message($contact, $item['datetime'], $item['object']['conversation'], $item['object']['message']->body);
                    break;
            }
        }
    }

}