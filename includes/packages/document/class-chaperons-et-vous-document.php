<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-folder.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-document.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-section.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-entity.php';
/**
* 
*/
class Chaperons_Et_Vous_Document
{
    /**
     * Details about the connected user
     * @var stdClass
     */
    private $public_data;
    private $documents = array();
    private $folders = array();
    private $rightsList = array();
    
    public function __construct($public_hooks)
    {
        // Store the public data
        $this->public_data = $public_hooks;
        
        // Declare ajax action
        add_action('wp_ajax_lpcr_document_create', array($this, 'create_document_from_ajax_request'));
        add_action('wp_ajax_lpcr_document_delete', array($this, 'delete_document_from_ajax_request'));
        
        // Declare the shortcodes
        add_shortcode('documents_list', array($this, 'shortcode_documents_list'));
    }
    
    public function create_document_from_ajax_request()
    {
      $finalFolder = $_SERVER['DOCUMENT_ROOT'].Document::DOCUMENT_FOLDER;
          
      // Verify datas
      if (empty($_POST['name']) || !isset($_POST['description']) || empty($_POST['folder_id']) || !is_numeric($_POST['folder_id']) || empty($_FILES['document'])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Merci de renseigner l'intégralité des champs.");
      }
      
      // Entity datas
      $entity_type = intval($_POST['entity_type']);
      $entity_id = intval($_POST['entity_id']);
      if(empty($entity_type) || empty($entity_id)) {
        $entity = $_POST['entity'];
        if(!empty($entity) && preg_match("#^(\d+)\-(\d+)$#", $entity, $responses)) {
          $entity_type = $responses[1];
          $entity_id = $responses[2];
        } else {
          Chaperons_Et_Vous_Ajax::generateResponse(false, "Merci de préciser les règles de partage liées à ce document.");
        }
      }
      
      // Does the folder exist ?
      $folder = new Folder($_POST['folder_id']);
      if (empty($folder->get_id())) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de l'enregistrement du document.");
      }
    
      // Check rights to create the document
      $userRights = new UserRights();
      if(!$userRights->hasCreateRights(UserRights::TYPE_DOCS)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaire pour ajouter un document.");
      }
      
      // Uploading document
      $file = $_FILES['document'];
      if($file['error'] != 0) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de l'enregistrement du document.");
      }
      
      // Verifying the weigth
      if($file['size'] > Document::MAX_SIZE) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Fichier trop volumineux. Merci de respecter la taille maximale autorisée.");
      }
      
      // Verify the mime type
      if (array_key_exists($file['type'], Document::$mime_types)) {
        $typeOfFile = Document::$mime_types[$file['type']];
        $sizeOfFile = $file['size'];
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Ce type de document n'est pas autorisé.");
      }
      
      // Move the uploaded doc to folder path
      $docName = substr(md5(uniqid()), 0, 10);
      $extension = pathinfo($file['name'], PATHINFO_EXTENSION);
      move_uploaded_file($file['tmp_name'], $finalFolder.$docName.'.'.$extension);
    
      // Saving the document
      $document = new Document();
      $document->set_folder_id(intval($_POST['folder_id']));
      $document->set_name($_POST['name']);
      $document->set_description($_POST['description']);
      $document->set_path(Document::DOCUMENT_FOLDER.$docName.'.'.$extension);
      $document->set_type($typeOfFile);
      $document->set_size($sizeOfFile);
      $document->set_user_id($this->public_data->get_user()->ID);
      $document->set_entity_type($entity_type);
      $document->set_entity_id($entity_id);
      $document->set_date_add(date('Y-m-d H:i:s'));
      if ($document->create()) {
        global $wpdb;
        $id_document = $wpdb->insert_id;
        $documentResponse = array(
        "id"=>$id_document,
        "name"=>$document->get_name(),
        "path"=>$document->get_path()
      );
    
      Chaperons_Et_Vous_Ajax::generateResponse(true, $documentResponse);
      }
    }
    
    public function delete_document_from_ajax_request()
    {
      $document_id = intval($_POST['document_id']);
      $document = new Document($document_id);
      
      if(empty($document->get_id()) || !$this->hasRights($document, $this->public_data)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires pour supprimer ce document.");
      }
      
      if($document->delete()) {
        Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de la suppression du document");
      }
    }
    
    public function set_documents_list($atts, $content)
    {
      // List all the allowed album for the current user
      if (is_user_logged_in()) {
        $raw_documents = Document::fetchUserDocuments($this->public_data->get_user()->ID, $this->public_data->get_creche(), $this->public_data->get_children());

        $idFolderMenu = null;
        foreach ($raw_documents as $raw_document) {
          $folder_id = intval($raw_document->folder_id);
          $document_id = intval($raw_document->document_id);
          if (!isset($this->documents[$folder_id])) {
            $this->documents[$folder_id] = new Folder($folder_id);
            if($this->documents[$folder_id]->get_name() == "Menus") {
              $idFolderMenu = $folder_id;
            }
          }
          
          $document = new Document($document_id);
          if (!empty($document->get_id())) {
            // Append document
            // If it' a menu, we display only the 4 last menus
            if($folder_id == $idFolderMenu && count($this->documents[$folder_id]->documents)<=3) {
              $this->documents[$folder_id]->appendDocument($document);
            } elseif ($folder_id != $idFolderMenu) {
              $this->documents[$folder_id]->appendDocument($document);
            }
          }
        }
      }
    }
    
    public function set_folders_list() {
      global $wpdb;
      $query = "SELECT * FROM ".$wpdb->prefix."app_folders WHERE publication = 1";
      $this->folders = $wpdb->get_results($query);
    }

    /**
     * Getters
     */
    public function get_documents() { return $this->documents; }
    
    public function shortcode_documents_list($atts, $content)
    {
      // Init default values
      $this->set_folders_list(array(), $atts);
      $this->set_documents_list(array(), $atts);
      // Preparing rights box
      $userRights = new UserRights();
      $this->rightsList = $userRights->select_rights();
      // Display the messages
      $this->display_documents_list();
    }
    
    private function hasRights($document, $user) {
      // Does the user has the right to view this album ?
      $isAllowed = false;
      if ($document->get_user_id() == $user->get_user()->ID) {
        $isAllowed = true;
      } else {
        // Checking with creche, section and children
        $active_creche = $user->get_creche();
    
        $userRights = new UserRights();
        $rights = $userRights->getRights(UserRights::TYPE_DOCS);
        if($rights >= UserRights::WRITE_RIGHTS && $document->get_creche_id() == $active_creche->get_id()) {
          return true;
        }
    
        $entity_type = $document->get_entity_type();
        $entity_id = $document->get_entity_id();
        switch ($entity_type) {
        	case Entity::VISIBILITY_CRECHE:
        	  if ($entity_id == $active_creche->get_id()) {
        	    $isAllowed = true;
        	  }
        	  break;
    
        	case Entity::VISIBILITY_SECTION:
        	  if (!empty($active_creche)) {
        	    $sections = $active_creche->get_sections();
        	    foreach ($sections as $section) {
        	      $section = Section::loadByName($section);
        	      if ($entity_id == $section->get_id()) {
        	        $isAllowed = true;
        	        break;
        	      }
        	    }
        	  }
        	  break;
    
        	case Entity::VISIBILITY_CHILD:
        	  $children = $user->get_children();
        	  if (!empty($children)) {
        	    foreach ($children as $child) {
        	      if ($entity_id == $child->get_id()) {
        	        $isAllowed = true;
        	        break;
        	      }
        	    }
        	  }
        	  break;
        }
      }
      return $isAllowed;
    }
    
    private function display_documents_list() {
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-document-display.php';
        lpcr_htmlize_documents($this->documents, $this->folders, $this->rightsList);
    }
}