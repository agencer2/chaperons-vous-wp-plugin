<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../includes/class-chaperons-et-vous-ajax.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-photo.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-album.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-child.php';
// include composer autoload
require plugin_dir_path(dirname(dirname(dirname(__FILE__)))).'vendor/autoload.php';

// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;

/**
* 
*/
class Chaperons_Et_Vous_Album
{
    /**
     * Details about the connected user
     * @var stdClass
     */
    private $public_data;
    private $albums = array();
    private $album = null;
    private $rightsList = array();
    
    public function __construct($public_hooks)
    {
        // Store the public data
        $this->public_data = $public_hooks;
        
        // Declare ajax action
        add_action('wp_ajax_lpcr_album_upload', array($this, 'upload_photo_from_ajax_request'));
        add_action('wp_ajax_lpcr_album_create', array($this, 'create_album_from_ajax_request'));
        add_action('wp_ajax_lpcr_album_change_name', array($this, 'change_album_name_from_ajax_request'));
        add_action('wp_ajax_lpcr_album_delete', array($this, 'delete_album_from_ajax_request'));
        add_action('wp_ajax_lpcr_photo_change_name', array($this, 'change_photo_name_from_ajax_request'));
        add_action('wp_ajax_lpcr_photo_delete', array($this, 'delete_photo_from_ajax_request'));
        add_action('wp_ajax_lpcr_tmpphoto_delete', array($this, 'delete_tmpphoto_from_ajax_request'));
        add_action('wp_ajax_lpcr_album_approve', array($this, 'approve_album_from_ajax_request'));
        
        // Declare the shortcodes
        add_shortcode('album_modal', array($this, 'shortcode_album_modal'));
        add_shortcode('album_list', array($this, 'shortcode_albums_list'));
        add_shortcode('album_details', array($this, 'shortcode_album_details'));
    }
    
    public function approve_album_from_ajax_request() {
      if(empty($_POST['album_id'])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez préciser l'album à valider.");
      }
      $userRights = new UserRights();
      if($userRights->hasCreateRights(UserRights::TYPE_ALBUM)) {
        // Approve the album
        $album = new Album(intval($_POST['album_id']));
        
        if (empty($album->get_id()) || !$this->hasRights($album, $this->public_data)) {
          Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne possédez pas les droits nécessaires pour valider cet album.");
        } else {
          $album->set_publication(1);
          $album->update();
          Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
        }
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne possédez pas les droits nécessaires pour valider cet album.");
      }
    }
    
    public function create_album_from_ajax_request()
    {
      $finalFolder = $_SERVER['DOCUMENT_ROOT'].Photo::GALLERY_FOLDER;
      
      $uploadFolder = $_SERVER['DOCUMENT_ROOT'].Photo::TMP_FOLDER;
      
      // Entity datas
      if (!empty($_POST['entity_type']) && !empty($_POST['entity_id'])) {
        $entity_type = intval($_POST['entity_type']);
        $entity_id = intval($_POST['entity_id']);
      } else {
        $entity = $_POST['entity'];
        if(!empty($entity) && preg_match("#^(\d+)\-(\d+)$#", $entity, $responses)) {
          $entity_type = $responses[1];
          $entity_id = $responses[2];
        } else {
          Chaperons_Et_Vous_Ajax::generateResponse(false, "Merci de préciser les règles de partage liées à cet album.");
        }
      }
      
      $token = $_POST['albumToken']; 
      $uploadFolder .= $token.'/';
      
      // Check rights to create the album
      $userRights = new UserRights();
      $rights = $userRights->getRights(UserRights::TYPE_ALBUM);
      if(!$userRights->hasCreateRights(UserRights::TYPE_ALBUM)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaire pour créer un album.");
      }
      
      // Move temps files to final directory and create for each, a new photo instance
      if( !is_dir($uploadFolder) ) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de la sauvegarde des images. Veuillez recommencer.");
      }
      
      $fileList = scandir($uploadFolder);
      
      $arrayPhotosEntity = array();
      foreach($fileList as $file) {
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        if(empty($extension)) {
          continue;
        }
      
        if (!preg_match("#^thumb\-(.*)\.".$extension."$#", $file)) {
          $filename = pathinfo($file, PATHINFO_BASENAME);
          $arrayPhotosEntity[] = $filename;
        }
        // In all case we move the uploaded file to the final folder
        rename($uploadFolder.$file, $finalFolder.$file);
      }
      
      // If the user can only submit, we make publication to 0
      if($rights == UserRights::SUBMISSION_RIGHTS) {
        $publication = 0;
      } else {
        $publication = 1;
      }
      
      // Saving album     
      $album = new Album();
      $album->set_name($_POST['name']);
      $album->set_publication($publication);
      $album->set_user_id($this->public_data->get_user()->ID);
      $album->set_creche_id($this->public_data->user_active_creche->get_id());
      $album->set_entity_type($entity_type);
      $album->set_entity_id($entity_id);
      $album->set_date_add(date('Y-m-d H:i:s'));
      $id_album = $album->create();
      
      // Convert post data into associative array
      $arrayDescription = array();
      foreach ($_POST['descriptions'] as $description) {
        $arrayDescription[$description["id"]] = $description["text"];
      }
      
      // Saving all photos
      $first_path = null;
      $photoList = array();
      foreach($arrayPhotosEntity as $photoToSave) {
        $name = "";
        if (!empty($arrayDescription[$photoToSave])) {
          $name = $arrayDescription[$photoToSave];
        }
        $photo = new Photo();
        $photo->set_album_id($id_album);
        $photo->set_name($name);
        $photo->set_path($photoToSave);
        $photo->set_video(0);
        $photo->set_date_add(date('Y-m-d H:i:s'));
        if ($photo->create() && empty($first_path)) {
          $first_path = $photoToSave;
        }
        $idPhoto = $photo->get_id();
        $photoList[$idPhoto] = $photoToSave;
      }
      
      $albumResponse = array(
      	"id"=>$id_album,
        "name"=>$album->get_name(),
        "path"=>$first_path,
        "photoList"=>$photoList
      );
      
      Chaperons_Et_Vous_Ajax::generateResponse(true, $albumResponse);
    }
    
    public function change_album_name_from_ajax_request()
    {
      $album_id = intval($_POST['album_id']);
      
      // Trying to load the album before deleting
      $album = new Album($album_id);
      if ($album->get_id() == null || !$this->hasRights($album, $this->public_data)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires pour modifier cet album.");
      }
      $album->set_name($_POST['album_name']);
      $album->update();
      Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
    }
    
    public function change_photo_name_from_ajax_request()
    {
      // Load the photo
      $photo_id = intval($_POST['photo_id']);
      $photo = new Photo($photo_id);
      
      // Trying to load the album before deleting the photo
      $album = new Album($photo->get_album_id());
      if ($album->get_id() == null || !$this->hasRights($album, $this->public_data)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires pour modifier cette photo.");
      }
      
      $photo->set_name($_POST['photo_name']);
      $photo->update();
    }
    
    public function delete_album_from_ajax_request()
    {
      $album_id = intval($_POST['album_id']);
      
      // Trying to load the album before deleting
      $album = new Album($album_id);
      if ($album->get_id() == null || !$this->hasRights($album, $this->public_data)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires pour supprimer cet album.");
      }
      
      if($album->delete()) {
        Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de la suppression de l'album");
      }
    }
    
    public function delete_photo_from_ajax_request()
    {      
      // Load the photo
      $photo_id = intval($_POST['photo_id']);
      $photo = new Photo($photo_id);
      
      // Trying to load the album before deleting the photo
      $album = new Album($photo->get_album_id());
      if ($album->get_id() == null || !$this->hasRights($album, $this->public_data)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires pour supprimer cette photo.");
      }
    
      if($photo->delete()) {
        Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de la suppression de l'album");
      }
    }
    
    public function delete_tmpphoto_from_ajax_request()
    {
      // Verifying folders
      $uploadFolder = $_SERVER['DOCUMENT_ROOT'].Photo::TMP_FOLDER;
      
      // Create temp subfolder according to the token
      $token = $_POST['albumToken'];
      $uploadFolder .= $token.'/';
      
      if( !is_dir($uploadFolder) ) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de la suppression de l'image : le dossier n'existe pas.");
      }
      
      if (!file_exists($uploadFolder.$_POST['name'])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de la suppression de l'image : le fichier n'existe pas.");
      } else {
        unlink($uploadFolder.$_POST['name']);
        Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
      }
    }
    
    public function upload_photo_from_ajax_request()
    {
      // Force memory_limit
      ini_set("memory_limit","256M");
      
      // create an image manager instance with favored driver
      $manager = new ImageManager(array('driver' => 'gd'));
      
      $allowedMimeType = array(
        'image/jpeg'=>'jpg',
        'image/png'=>'png',
        'image/gif'=>'gif'
      );
      
      if (!empty($_POST) && !empty($_FILES)) {
        // On verifie si le champ est rempli
        if (!empty($_FILES[0]['name'])) {
          // Recuperation de l'extension du fichier
          $mime_type  = $_FILES[0]['type'];
      
          // On verifie l'extension du fichier
          if (array_key_exists(strtolower($mime_type), $allowedMimeType)) {
            // On recupere les dimensions du fichier
            $infosImg = getimagesize($_FILES[0]['tmp_name']);
      
            // On verifie le type de l'image
            if(!empty($infosImg) && $infosImg[2] >= 1 && $infosImg[2] <= 14) {
      
              //if(($infosImg[0] <= WIDTH_MAX) && ($infosImg[1] <= HEIGHT_MAX) && (filesize($_FILES[0]['tmp_name']) <= MAX_SIZE)) {
              if($_FILES[0]['size'] <= Photo::MAX_SIZE) {
                // Parcours du tableau d'erreurs
                if(isset($_FILES[0]['error']) && UPLOAD_ERR_OK === $_FILES[0]['error']) {
                  // On renomme le fichier
                  $imgName = substr(md5(uniqid()), 0, 10);
                  $extension = $allowedMimeType[$mime_type];
      
                  // Verifying folders
                  $uploadFolder = $_SERVER['DOCUMENT_ROOT'].Photo::TMP_FOLDER;
      
                  // Create temp subfolder according to the token
                  $token = $_POST['albumToken'];
                  $uploadFolder .= $token.'/';
      
                  if( !is_dir($uploadFolder) ) {
                    if( !mkdir($uploadFolder, 0755, true) ) {
                      Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
                    }
                  }
      
                  // Si c'est OK, on teste l'upload
                  if(move_uploaded_file($_FILES[0]['tmp_name'], $uploadFolder.$imgName.'.'.$extension)) {                    
                    // Create thumbnail
                    $img = $manager->make($uploadFolder.$imgName.'.'.$extension);
                    $img->fit(500, 500, function ($constraint) {
                      $constraint->upsize();
                    });
                    $img->save($uploadFolder.Photo::THUMB_PREFIX.$imgName.'.'.$extension);
      
                    // Applying watermark on original photo and resize to max size
                    $img = $manager->make($uploadFolder.$imgName.'.'.$extension);
                    $img->resize(2500, 2500, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                    $img->insert(get_template_directory().'/img/watermark.png', 'bottom-right', 10, 10);
                    $img->save();
                    
                    $response = array(
                    	"full_path"=>Photo::TMP_FOLDER.$token.'/'.Photo::THUMB_PREFIX.$imgName.'.'.$extension,
                      "img_name"=>$imgName.'.'.$extension
                    );
                    Chaperons_Et_Vous_Ajax::generateResponse(true, $response);
                  } else {
                    // Sinon on affiche une erreur systeme
                    Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
                  }
                } else {
                  Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
                }
              } else {
                // Sinon erreur sur les dimensions et taille de l'image
                Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur dans les dimensions de l'image !");
              }
            } else {
              // Sinon erreur sur le type de l'image
              Chaperons_Et_Vous_Ajax::generateResponse(false, "Type de fichier non autorisé");
            }
          } else {
            // Sinon on affiche une erreur pour l'extension
            Chaperons_Et_Vous_Ajax::generateResponse(false, "Type de fichier non autorisé");
          }
        } else {
          // Sinon on affiche une erreur pour le champ vide
          Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez sélectionner une image");
        }
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez sélectionner une image");
      }
    }
    
    public function get_album_details($id)
    {
      // Load the album
      $album = new Album($id);
      if ($album->get_id() == null || !$this->hasRights($album, $this->public_data)) {
        return false;
      }
      $this->album = $album;
            
      // Load related photos
      $photos = $album->get_photos();
      $this->album->photos = $photos;
    }
    
    private function hasRights($album, $user) {
      // Does the user has the right to view this album ?
      $isAllowed = false;
      if ($album->get_user_id() == $user->get_user()->ID) {
        $isAllowed = true;
      } else {
        // Checking with creche, section and children
        $active_creche = $user->get_creche();
      
        $userRights = new UserRights();
        $rights = $userRights->getRights(UserRights::TYPE_ALBUM);
        if($rights >= UserRights::WRITE_RIGHTS && $album->get_creche_id() == $active_creche->get_id()) {
          return true;
        }
        
        $entity_type = $album->get_entity_type();
        $entity_id = $album->get_entity_id();
        switch ($entity_type) {
        	case Entity::VISIBILITY_CRECHE:
        	  if ($entity_id == $active_creche->get_id()) {
        	    $isAllowed = true;
        	  }
        	break;
        	   
        	case Entity::VISIBILITY_SECTION:
        	  if (!empty($active_creche)) {
        	    $sections = $active_creche->get_sections();
        	    foreach ($sections as $section) {
        	      $section = Section::loadByName($section);
        	      if ($entity_id == $section->get_id()) {
        	        $isAllowed = true;
        	        break;
        	      }
        	    }
        	  }
        	break;
        	   
        	case Entity::VISIBILITY_CHILD:
        	  $children = $user->get_children();
        	  if (!empty($children)) {
        	    foreach ($children as $child) {
        	      if ($entity_id == $child->get_id()) {
        	        $isAllowed = true;
        	        break;
        	      }
        	    }
        	  }
        	break;
        }
      }
      return $isAllowed;
    }
    
    /**
     * List albums and set the first image as a thumbnail
     */
    public function set_albums_list()
    {
      // List all the allowed album for the current user
      if (is_user_logged_in()) {
        global $wpdb;
                
        $userRights = new UserRights();
        $right = $userRights->getRights(UserRights::TYPE_ALBUM);
        
        // Get user's active creche                
        $active_creche = $this->public_data->get_creche();
        // Get user's children
        $children = $this->public_data->get_children();
        
        /** List albums and set the first image as a thumbnail **/
        $results = Album::fetchUserAlbums($this->public_data->get_user()->ID, $right, $active_creche, $children);
        
        foreach($results as $album) {
          // Display visibility for director
          switch ($album->entity_type) {
          	case Entity::VISIBILITY_CRECHE:
          	  $visibilityString = "Toute la crèche";
        	  break;
          
          	case Entity::VISIBILITY_SECTION:
          	  $sectionList = $this->public_data->get_creche()->get_sections();
          	  if (!empty($sectionList[$album->entity_id])) {
          	    $visibilityString = $sectionList[$album->entity_id];
          	  }
        	  break;
        	  
        	  case Entity::VISIBILITY_CHILD:
        	    $child = new Child($album->entity_id);
        	    if(!empty($child->get_id())) {
        	      $visibilityString = $child->get_first_name()." ".$child->get_last_name();
        	    }
      	    break;
          
          	default:
          	  $visibilityString = "Inconnue";
          	break;
          }
          $album->visibility = $visibilityString;
        }

        $this->albums = $results;
      }
    }

    /**
     * Getters
     */
    public function get_albums() { return $this->albums; }
    public function get_album() { return $this->album; }
    
    public function shortcode_albums_list($atts, $content)
    {
      // Init default values
      $atts = shortcode_atts(array(), $atts);
      $this->set_albums_list();
      // Display the messages
      $this->display_album_list();
    }
    
    public function shortcode_album_modal($atts, $content)
    {
      // Init default values
      $atts = shortcode_atts(array(), $atts);
      // Preparing rights box
      $userRights = new UserRights();
      $this->rightsList = $userRights->select_rights();
      // Display the modal
      $this->display_album_modal();
    }
    
    public function shortcode_album_details($atts, $content)
    {
      if (empty($atts)) {
        exit( wp_redirect( home_url('/page-accueil') ) );
      }
      $id_album = $atts['id_album'];
      // Init default values
      $this->get_album_details($id_album);
      // Display the messages
      $this->display_album_details();
    }

    private function display_album_list() {
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-album-list-display.php';
        lpcr_htmlize_albums_header();
        lpcr_htmlize_albums($this->albums);
    }
    
    private function display_album_modal() {
      require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-album-list-display.php';
      lpcr_htmlize_albums_modal($this->rightsList);
    }
    
    private function display_album_details() {
      require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-album-details-display.php';
      lpcr_htmlize_album($this->album);
    }
}