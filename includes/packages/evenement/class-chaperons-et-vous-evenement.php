<?php

require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/models/class-chaperons-et-vous-evenement.php';
require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/class-chaperons-et-vous-user-rights.php';

// include composer autoload
require plugin_dir_path(dirname(dirname(dirname(__FILE__)))).'vendor/autoload.php';

// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;
/** 
* 
*/
class Chaperons_Et_Vous_Evenement
{

    private $public_data;
    private $evenements = array();
    private $monthCalendar = array();
    private $user_rights = null;

    public function __construct($public_hooks)
    {
        // Store the public data
        $this->public_data = $_SESSION;
        $this->user_rights = new UserRights();
                
        // Declare the shortcode
        add_shortcode('show_evenements', array($this, 'shortcode_evenement_list'));
        add_shortcode('show_month_calendar', array($this, 'shortcode_month_calendar'));
        
        // Declare AJAX action
        add_action('wp_ajax_lpcr_event_create', array($this, 'create_event_from_ajax_request'));
        add_action('wp_ajax_lpcr_event_delete', array($this, 'delete_event_from_ajax_request'));
        add_action('wp_ajax_lpcr_event_get', array($this, 'get_event_details_from_ajax_request'));
        add_action('wp_ajax_lpcr_event_attendee', array($this, 'response_event_from_ajax_request'));
        add_action('wp_ajax_lpcr_event_month_calendar', array($this, 'show_month_event_from_ajax_request'));
    }
    
    private function hasRights($evenement, $user) {
      // Does the user has the right to view this album ?
      $isAllowed = false;
      if ($evenement->get_user_id() == $user["current_user"]->data->ID) {
        $isAllowed = true;
      } else {
        // Checking with creche, section and children
        $active_creche = $user["active_creche"];
    
        $userRights = new UserRights();
        $rights = $userRights->getRights(UserRights::TYPE_EVENT);
        if($rights >= UserRights::WRITE_RIGHTS && $evenement->get_creche_id() == $active_creche->get_id()) {
          $isAllowed = true;
        } else {
          $isAllowed = false;
        }
      }
      return $isAllowed;
    }
    
    public function delete_event_from_ajax_request()
    {
      // Do we have the right to create an event
      $userRights = new UserRights();
      if(!$userRights->hasCreateRights(UserRights::TYPE_EVENT)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous n'avez pas les droits nécessaires afin de supprimer un événement.");
      }
      
      if(empty($_POST['event_id']) || !is_numeric($_POST['event_id'])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez préciser l'identifiant de l'événement à supprimer.");
      }
      
      $evenement = new Evenement(intval($_POST['event_id']));
      if ($evenement->get_id() == null || !$this->hasRights($evenement, $this->public_data)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires pour supprimer cette photo.");
      }
      
      if($evenement->delete()) {
        Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de la suppression. Veuillez reessayer.");
      }
    }
    
    public function get_event_details_from_ajax_request()
    {
      if(empty($_POST["event_id"]) || !is_numeric($_POST["event_id"])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez préciser l'identifiant de l'événement");
      }
      
      $event = new Evenement(intval($_POST["event_id"]));
      if(empty($event->get_id()) || !$this->hasRights($event, $this->public_data)){
        Chaperons_Et_Vous_Ajax::generateResponse(false, "L'événement n'existe pas");
      }
      
      $arrayData = array();
      $arrayData["id"] = $event->get_id();
      $arrayData["title"] = $event->get_title();
      $arrayData["image_url"] = $event->get_image_url();
      $arrayData["body"] = $event->get_body();
      $arrayData["datetime_begin"] = $event->get_datetime_begin();
      $arrayData["datetime_end"] = $event->get_datetime_end();
      $arrayData["type_event"] = $event->get_type_event();
      
      Chaperons_Et_Vous_Ajax::generateResponse(true, $arrayData);
    }
    
    public function create_event_from_ajax_request()
    {
      // Do we have the right to create an event
      $userRights = new UserRights();
      if(!$userRights->hasCreateRights(UserRights::TYPE_EVENT)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous n'avez pas les droits nécessaires afin de créer un événement.");
      }
      
      // Does all the field are setted ?
      if(empty($_POST['title']) || empty($_POST['body']) || empty($_POST['datetime_begin']) || empty($_POST['datetime_end']) || empty($_POST['type_event'])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez remplir l'intégralité des champs afin de créer un événement.");
      }
      
      // Entity datas
      if (!empty($_POST['entity_type']) && !empty($_POST['entity_id'])) {
        $entity_type = intval($_POST['entity_type']);
        $entity_id = intval($_POST['entity_id']);
      } else {
        $entity = $_POST['entity'];
        if(!empty($entity) && preg_match("#^(\d+)\-(\d+)$#", $entity, $responses)) {
          $entity_type = $responses[1];
          $entity_id = $responses[2];
        } else {
          Chaperons_Et_Vous_Ajax::generateResponse(false, "Merci de préciser les règles de partage liées à cet album.");
        }
      }
        
      // Does a image has been uploaded ?
      $image = null;
      if (!empty($_FILES['image_url']['type'])) {
        // Force memory_limit
        ini_set("memory_limit","256M");
        
        // create an image manager instance with favored driver
        $manager = new ImageManager(array('driver' => 'gd'));
        
        $allowedMimeType = array(
          'image/jpeg'=>'jpg',
          'image/png'=>'png',
          'image/gif'=>'gif'
        );
        
        $uploadedImage = $_FILES['image_url'];
        // Recuperation de l'extension du fichier
        $mime_type  = $uploadedImage['type'];
        
        // On verifie l'extension du fichier
        if (array_key_exists(strtolower($mime_type), $allowedMimeType)) {
          // On recupere les dimensions du fichier
          $infosImg = getimagesize($uploadedImage['tmp_name']);
        
          // On verifie le type de l'image
          if(!empty($infosImg) && $infosImg[2] >= 1 && $infosImg[2] <= 14) {
        
            if($uploadedImage['size'] <= Evenement::PHOTO_MAX_SIZE) {
              // Parcours du tableau d'erreurs
              if(isset($uploadedImage['error']) && UPLOAD_ERR_OK === $uploadedImage['error']) {
                // On renomme le fichier
                $imgName = substr(md5(uniqid()), 0, 10);
                $extension = $allowedMimeType[$mime_type];
        
                // Verifying folders
                $uploadFolder = $_SERVER['DOCUMENT_ROOT'].Evenement::PHOTO_FOLDER;
        
                if( !is_dir($uploadFolder) ) {
                  if( !mkdir($uploadFolder, 0755, true) ) {
                    Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
                  }
                }
        
                // Si c'est OK, on teste l'upload
                if(move_uploaded_file($uploadedImage['tmp_name'], $uploadFolder.$imgName.'.'.$extension)) {
                  // Create thumbnail
                  $img = $manager->make($uploadFolder.$imgName.'.'.$extension);
                  $img->fit(500, 500, function ($constraint) {
                    $constraint->upsize();
                  });
                  $img->save();
        
                  $image = Evenement::PHOTO_FOLDER.$imgName.'.'.$extension;
                } else {
                  // Sinon on affiche une erreur systeme
                  Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
                }
              } else {
                Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
              }
            } else {
              // Sinon erreur sur les dimensions et taille de l'image
              Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur dans les dimensions de l'image !");
            }
          } else {
            // Sinon erreur sur le type de l'image
            Chaperons_Et_Vous_Ajax::generateResponse(false, "Type de fichier non autorisé");
          }
        } else {
          // Sinon on affiche une erreur pour l'extension
          Chaperons_Et_Vous_Ajax::generateResponse(false, "Type de fichier non autorisé");
        }
      }
      
      // Is a submission or a creation ?
      $rights = $userRights->getRights(UserRights::TYPE_EVENT);
      if($rights == UserRights::SUBMISSION_RIGHTS) {
        $active = 0;
      } else {
        $active = 1;
      }
            
      // Is it a creation or a edit ?
      if(!empty($_POST['id'])) {
        $evenement = new Evenement(intval($_POST['id']));
        if (empty($evenement->get_id())) {
          $evenement = new Evenement();
        }
      } else {
        $evenement = new Evenement();
      }      
            
      if (!empty($image)) {
        $evenement->set_image_url($image);
      }
      
      $evenement->set_title($_POST['title']);
      $evenement->set_body($_POST['body']);
      $evenement->set_entity_type($entity_type);
      $evenement->set_entity_id($entity_id);
      $evenement->set_datetime_begin($_POST['datetime_begin']);
      $evenement->set_datetime_end($_POST['datetime_end']);
      $evenement->set_user_id($this->public_data["current_user"]->ID);
      $evenement->set_creche_id($this->public_data["active_creche"]->get_id());
      $evenement->set_type_event($_POST['type_event']);
      $evenement->set_active($active);
      $evenement->set_date_add(date('Y-m-d H:i:s'));
      
      $idOrFail = $evenement->save();
      
      if($idOrFail !== false) {
        $id = $evenement->get_id();
        Chaperons_Et_Vous_Ajax::generateResponse(true, $id);
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de l'enregistrement, veuillez réessayer.");
      }
    }
    
    public function response_event_from_ajax_request()
    {
      global $wpdb;
      
      if(empty($_POST['event_id']) || !is_numeric($_POST['event_id']) || !isset($_POST['attendee']) || !is_numeric($_POST['attendee'])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez renseigner tous les paramètres");
      }
      
      $event_id = intval($_POST['event_id']);
      $answer = intval($_POST['attendee']);
      
      $event = new Evenement($event_id);
      if(empty($event->get_id()) || !$this->hasRights($event, $this->public_data)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Cet événement n'existe pas.");
      }
      
      if($answer != 0) {
        $answer = 1;
      }
      
      // Does the user has already answered to the event ?
      $query = "SELECT * FROM ".$wpdb->prefix."app_evenements_participation WHERE id_event = ".$event->get_id()." AND id_user = ". $this->public_data["current_user"]->ID;
      $oldAnswer = $wpdb->get_row($query);
      if (!empty($oldAnswer)) {
        $wpdb->update($wpdb->prefix."app_evenements_participation", array("answer"=>$answer), array("id_event"=>$event->get_id(), "id_user"=>$this->public_data["current_user"]->ID));
      } else {
        $wpdb->insert($wpdb->prefix."app_evenements_participation", array("id_event"=>$event->get_id(), "id_user"=>$this->public_data["current_user"]->ID, "answer"=>$answer));
      }
      Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
    }

    public function shortcode_evenement_list($atts, $content) {
        // Init default values
        $atts = shortcode_atts(array(), $atts);
        $this->set_evenements_list();
        // Display the evenements
        $this->display_evenement_list();
    }
    
    public function shortcode_month_calendar($atts, $content) {
      // Init default values
      $atts = shortcode_atts(array(), $atts);
      $this->set_month_calendar();
      // Display the evenements
      $this->display_month_calendar();
    }

    /**
     * Getters
     */
    public function get_evenements() { return $this->evenements; }

    public function set_evenements_list()
    {
        global $wpdb;

        // List all the allowed evenements for the current user
        if (is_user_logged_in()) {
            $role = $this->public_data["current_user"]->roles[0];
            $arrayEvents = Evenement::fetchUserEvents($this->public_data["current_user"]->ID, $this->public_data["active_creche"]);

            $this->evenements = $this->group_evenements_by_month($arrayEvents['events'], $arrayEvents['anniversary']);
        }
    }
    
    private function set_month_calendar()
    {
      global $wpdb;
      $calEvents = array();
      
      $currentMonth = date("n");
      $currentYear = date("Y");
      
      if(!empty($this->public_data["active_creche"])) {
        // Preparing small calendar of current month
        // Getting events of this month
        $query = "SELECT id, datetime_begin, DAY(datetime_begin) AS day FROM `".$wpdb->prefix."app_evenements` WHERE creche_id = ".$this->public_data["active_creche"]->get_id()." AND DATE(datetime_begin) >= CURDATE() AND MONTH(`datetime_begin`) = ".$currentMonth." AND YEAR(`datetime_begin`) = ".$currentYear." ORDER BY datetime_begin";
        $results = $wpdb->get_results($query);
        
        foreach($results as $result) {
          if(empty($calEvents[$result->day])) {
            $calEvents[$result->day] = $result->id;
          }
        }
        
        // Getting anniversary of this month
        $query = "SELECT id, DAY(birth_date) AS day FROM ".$wpdb->prefix."app_children WHERE id_creche = ".$this->public_data["active_creche"]->get_id()." AND birth_date_publication = 1 AND DATE(birth_date) >= CURDATE() AND MONTH(`birth_date`) = ".$currentMonth;
        $results = $wpdb->get_results($query);
        foreach($results as $result) {
          if(empty($calEvents[$result->day])) {
            $calEvents[$result->day] = "bd-".$result->id;
          }
        }
      }
              
      // Number of days in month
      $nbJours = cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear);
      $monthCalendar = array();
      for($i=1;$i<=$nbJours;$i++) {
        if(!empty($calEvents[$i])) {
          $monthCalendar[$i] = $calEvents[$i];
        } else {
          $monthCalendar[$i] = false;
        }
      }
      
      $this->monthCalendar = $monthCalendar;
    }
    
    private function set_anniversary()
    {
      
    }

    private function group_evenements_by_month($evenements, $birthdays) {
        global $wpdb;
        $toReturn = array();
        if (is_array($evenements)) {
            foreach ($evenements as $id_evenement) {
                $evenement = new Evenement($id_evenement->id);
                // Group the evenements by month and days
                $month = $evenement->getFormatMonth();
                $day = date("d", strtotime($evenement->get_datetime_begin()));
                
                if (!array_key_exists($month, $toReturn))
                    $toReturn[$month] = array();
                $toReturn[$month][$day][] = $evenement;
            }
        }
        
        if (is_array($birthdays)) {
          foreach ($birthdays as $birthday) {
            $month = date("m", strtotime($birthday->birth_date));
            $day = date("d", strtotime($birthday->birth_date));
            
            $evenement = new Evenement();
            $evenement->set_id("bd-".$birthday->id);
            $evenement->set_title("Anniversaire ".$birthday->first_name." ".$birthday->last_name);
            $evenement->set_body("Bon anniversaire à ".$birthday->first_name." ".$birthday->last_name." !");
            $evenement->set_datetime_begin(date("Y")."-".$month."-".$day." 00:00:00");
            $evenement->set_datetime_end(date("Y")."-".$month."-".$day." 23:59:59");
            $evenement->set_type_event("Anniversaire");
            $evenement->set_active(1);
            
            // Group the evenements by month and days
            $formatMonth = $evenement->getFormatMonth();
            if (!array_key_exists($formatMonth, $toReturn))
              $toReturn[$formatMonth] = array();
            $toReturn[$formatMonth][$day][] = $evenement;
          }
        }
                
        // Ordering array per days
        foreach($toReturn as $formatMonth=>$month) {
          ksort($toReturn[$formatMonth]);
        }
        return $toReturn;
    }

    private function display_evenement_list() {
        $user = $this->public_data["current_user"];
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-evenement-display.php';
        lpcr_htmlize_evenements($this->evenements, $user, Evenement::$types);
    }
    
    private function display_month_calendar() {
      $user = $this->public_data["current_user"];
      require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-evenement-display.php';
      lpcr_htmlize_month_calendar($this->monthCalendar, $user);
    }
 
    private function save_evenement($event_id) {
        $event = new Evenement($event_id);     
        $event->set_title(stripslashes($_POST["title"]));
        $event->set_body(stripslashes($_POST["body"]));
        $event->set_entity_id($_POST["entity_id"]);
        $event->set_entity_type($_POST["entity_type"]);
        $event->set_type_event(stripslashes($_POST["type"]));
        $event->set_datetime_begin(stripslashes($_POST["datetime_begin"]));
        $event->set_datetime_end(stripslashes($_POST["datetime_end"]));
        $active = isset($_POST["active"]) ? 1 : 0;
        $event->set_active($active);
        $user_id = $this->public_data->get_user()->ID;
        $event->set_user_id($user_id);
        $event->save();
    }
    
    public function show_month_event_from_ajax_request()
    {
      global $wpdb;
      $arrayResponse = array(
        "success"=>1,
        "result"=>array()
      );
      
      if (is_user_logged_in() && !empty($this->public_data["active_creche"]) && !empty($_GET['from']) && !empty($_GET['to'])) {
        // Getting events
        $datetimeFrom = date('Y-m-d H:i:s', intval($_GET['from']/1000));
        $datetimeTo = date('Y-m-d H:i:s', intval($_GET['to']/1000));
        
        $query = "SELECT * FROM `".$wpdb->prefix."app_evenements` WHERE datetime_begin BETWEEN '".$datetimeFrom."' AND '".$datetimeTo."' AND creche_id = ".$this->public_data["active_creche"]->get_id()." AND active = 1";
        $results = $wpdb->get_results($query);
               
        foreach($results as $result) {
          $obj = new stdClass();
          $obj->id = $result->id;
          $obj->title = $result->title;
          $obj->url = "/evenements/#event-".$result->id;
          $obj->class = "event-info";
          $obj->start = strtotime($result->datetime_begin)*1000;
          $obj->end = strtotime($result->datetime_end)*1000;
          $arrayResponse["result"][] = $obj;
        }
      }
      echo json_encode($arrayResponse);
      die();
    }

}