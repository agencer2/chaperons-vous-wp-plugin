<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-folder.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-document.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-section.php';
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../../../../wp-includes/class-IXR.php';
/**
* 
*/
class Chaperons_Et_Vous_Article
{
    /**
     * Details about the connected user
     * @var stdClass
     */
    private $public_data;
    private $documents = array();
    private $folders = array();
    
    public function __construct($public_hooks)
    {
        // Store the public data
        $this->public_data = $public_hooks;
        
        // Declare ajax action
        add_action('wp_ajax_lpcr_article_create', array($this, 'create_article_from_ajax_request'));
    }
    
    public function create_article_from_ajax_request()
    {
      $userRights = new UserRights();
      if(!$userRights->hasCreateRights(UserRights::TYPE_NEWS)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires pour publier un article.");
      }
      
      if(empty($_POST['title']) || empty($_POST['body'])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez remplir tous les champs.");
      }
      
      $category_id = get_cat_ID("articles");
      if(empty($category_id)) {
        $category_id = wp_create_category("articles");
      }
      
      $my_post = array(
        'post_title'    => $_POST['title'],
        'post_content'  => $_POST['body'],
        'post_status'   => 'publish',
        'post_author'   => $this->public_data->get_user()->ID,
        'post_category' => array($category_id)
      );
      
      // Insert the post into the database
      if(wp_insert_post( $my_post )) {
        Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
      } else {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de l'enregistrement de l'article");
      }
    }
}