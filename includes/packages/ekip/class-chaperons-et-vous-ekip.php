<?php
require_once plugin_dir_path( dirname( __FILE__ ) ) . '../../public/models/class-chaperons-et-vous-team.php';

// include composer autoload
require plugin_dir_path(dirname(dirname(dirname(__FILE__)))).'vendor/autoload.php';

// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;
/**
* Define a Chaperons_Et_Vous_Ekip object
*/
class Chaperons_Et_Vous_Ekip
{

    private $public_data;
    private $employees = array();
    private $creche = null;

    public function __construct($public_hooks)
    {
        // Store the public data
        $this->public_data = $public_hooks;
        // Declare the shortcodes
        add_shortcode('ekip_list', array($this, 'shortcode_ekip_list'));
        add_shortcode('creche_details', array($this, 'shortcode_creche_details'));
        
        add_action('wp_ajax_lpcr_ekip_add', array($this, 'add_people_info_from_ajax_request'));
        add_action('wp_ajax_lpcr_ekip_edit_get', array($this, 'get_people_info_from_ajax_request'));
        add_action('wp_ajax_lpcr_ekip_edit_validate', array($this, 'edit_people_info_from_ajax_request'));
        add_action('wp_ajax_lpcr_ekip_delete', array($this, 'delete_people_info_from_ajax_request'));
    }

    /**
     * Getters
     */
    public function get_employees() { return $this->employees; }
    
    public function get_creche() { return $this->creche; }

    public function get_people_info_from_ajax_request()
    {
      global $wpdb;
      
      // Check rights
      $userRights = new UserRights();
      if (!$userRights->hasCreateRights(UserRights::TYPE_TEAM) || empty($this->public_data->user_active_creche)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires afin d'éditer un membre de l'équipe.");
      }
      
      $user_id = intval($_POST["user_id"]);
      if (empty($user_id)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez préciser l'utilisateur à éditer");
      }
      
      $user_role = null;
      if(!empty($_POST['user_role'])) {
        $user_role = $_POST['user_role'];
      }
      
      if(!empty($user_role)) {
        // Does the user edit a creche related people ?
        $user_asso = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."app_asso_employees_creches WHERE id_user = ".$user_id." AND id_creche = ".$this->public_data->user_active_creche->get_id());
        if(empty($user_asso)) {
          Chaperons_Et_Vous_Ajax::generateResponse(false, "L'utilisateur n'existe pas.");
        }
        
        // User infos
        $userMeta = get_user_meta($user_id);
        if(empty($userMeta)) {
          Chaperons_Et_Vous_Ajax::generateResponse(false, "L'utilisateur n'existe pas.");
        }
        $userMeta = (object)$userMeta;
        
        $arrayData = array(
        	"first_name" => !empty($userMeta->first_name[0])?$userMeta->first_name[0]:"",
          "last_name" => !empty($userMeta->last_name[0])?$userMeta->last_name[0]:"",
          "position" => !empty($userMeta->position[0])?$userMeta->position[0]:"",
          "description" => !empty($userMeta->description[0])?$userMeta->description[0]:"",
          "photo" => !empty($userMeta->photo[0])?$userMeta->photo[0]:"",
          "id_section" => null
        );
        Chaperons_Et_Vous_Ajax::generateResponse(true, $arrayData);
      } else {
        $team = new Team($user_id);
        if(empty($team) || $team->get_id_creche() != $this->public_data->user_active_creche->get_id()) {
          Chaperons_Et_Vous_Ajax::generateResponse(false, "L'utilisateur n'existe pas.");
        }
        $arrayData = array(
          "first_name" => $team->get_first_name(),
          "last_name" => $team->get_last_name(),
          "position" => $team->get_position(),
          "description" => $team->get_description(),
          "photo" => $team->get_photo(),
          "id_section" => $team->get_id_section()
        );
        Chaperons_Et_Vous_Ajax::generateResponse(true, $arrayData);
      }
    }
    
    public function add_people_info_from_ajax_request()
    {
      // Check rights
      $userRights = new UserRights();
      if (!$userRights->hasCreateRights(UserRights::TYPE_TEAM) || empty($this->public_data->user_active_creche)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires afin d'ajouter un membre de l'équipe.");
      }
    
      // All fields are mandatories
      if(empty($_POST['first_name']) || empty($_POST['last_name']) || empty($_POST['position']) || empty($_POST['description']) || !isset($_POST['id_section'])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez compléter l'intégralité des champs requis.");
      }
      
      // Does an image has been set ?
      $image = null;
      if(!empty($_FILES['photo']['type'])) {
        // Force memory_limit
        ini_set("memory_limit","256M");
        
        // create an image manager instance with favored driver
        $manager = new ImageManager(array('driver' => 'gd'));
        
        $allowedMimeType = array(
          'image/jpeg'=>'jpg',
          'image/png'=>'png',
          'image/gif'=>'gif'
        );
        
        $uploadedImage = $_FILES['photo'];
        // Recuperation de l'extension du fichier
        $mime_type  = $uploadedImage['type'];
        
        // On verifie l'extension du fichier
        if (array_key_exists(strtolower($mime_type), $allowedMimeType)) {
          // On recupere les dimensions du fichier
          $infosImg = getimagesize($uploadedImage['tmp_name']);
        
          // On verifie le type de l'image
          if(!empty($infosImg) && $infosImg[2] >= 1 && $infosImg[2] <= 14) {
        
            if($uploadedImage['size'] <= Team::PHOTO_MAX_SIZE) {
              // Parcours du tableau d'erreurs
              if(isset($uploadedImage['error']) && UPLOAD_ERR_OK === $uploadedImage['error']) {
                // On renomme le fichier
                $imgName = substr(md5(uniqid()), 0, 10);
                $extension = $allowedMimeType[$mime_type];
        
                // Verifying folders
                $uploadFolder = $_SERVER['DOCUMENT_ROOT'].Team::PHOTO_FOLDER;
        
                if( !is_dir($uploadFolder) ) {
                  if( !mkdir($uploadFolder, 0755, true) ) {
                    Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
                  }
                }
        
                // Si c'est OK, on teste l'upload
                if(move_uploaded_file($uploadedImage['tmp_name'], $uploadFolder.$imgName.'.'.$extension)) {
                  // Create thumbnail
                  $img = $manager->make($uploadFolder.$imgName.'.'.$extension);
                  $img->fit(500, 500, function ($constraint) {
                    $constraint->upsize();
                  });
                  $img->save();
        
                  $image = Team::PHOTO_FOLDER.$imgName.'.'.$extension;
                } else {
                  // Sinon on affiche une erreur systeme
                  Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
                }
              } else {
                Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
              }
            } else {
              // Sinon erreur sur les dimensions et taille de l'image
              Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur dans les dimensions de l'image !");
            }
          } else {
            // Sinon erreur sur le type de l'image
            Chaperons_Et_Vous_Ajax::generateResponse(false, "Type de fichier non autorisé");
          }
        } else {
          // Sinon on affiche une erreur pour l'extension
          Chaperons_Et_Vous_Ajax::generateResponse(false, "Type de fichier non autorisé");
        }        
      }
      
      $team = new Team();
      $team->set_first_name($_POST['first_name']);
      $team->set_last_name($_POST['last_name']);
      $team->set_position($_POST['position']);
      $team->set_description($_POST['description']);
      $team->set_photo($image);
      $team->set_id_section($_POST['id_section']);
      $team->set_id_creche($this->public_data->get_creche()->get_id());
      $team->create();
      
      Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
    }
    
    public function edit_people_info_from_ajax_request()
    {
      // Check rights
      $userRights = new UserRights();
      if (!$userRights->hasCreateRights(UserRights::TYPE_TEAM) || empty($this->public_data->user_active_creche)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Vous ne disposez pas des droits nécessaires afin d'éditer un membre de l'équipe.");
      }
      
      // All fields are mandatories
      if(empty($_POST['first_name']) || empty($_POST['last_name']) || empty($_POST['position']) || empty($_POST['description']) || !isset($_POST['id_section'])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez compléter l'intégralité des champs requis.");
      }
      
      // Checking user_id
      $user_id = intval($_POST["user_id"]);
      if (empty($user_id)) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez préciser l'utilisateur à éditer");
      }
      
      // Does the user is a wordpress user or a team member ?
      $isWordpressUser = false;
      if(!empty($_POST["user_role"])) {
        $isWordpressUser = true;
      }
      
      // Does an image has been set ?
      $image = null;
      if(!empty($_FILES['photo']['type'])) {
        // Force memory_limit
        ini_set("memory_limit","256M");
        
        // create an image manager instance with favored driver
        $manager = new ImageManager(array('driver' => 'gd'));
        
        $allowedMimeType = array(
          'image/jpeg'=>'jpg',
          'image/png'=>'png',
          'image/gif'=>'gif'
        );
        
        $uploadedImage = $_FILES['photo'];
        // Recuperation de l'extension du fichier
        $mime_type  = $uploadedImage['type'];
        
        // On verifie l'extension du fichier
        if (array_key_exists(strtolower($mime_type), $allowedMimeType)) {
          // On recupere les dimensions du fichier
          $infosImg = getimagesize($uploadedImage['tmp_name']);
        
          // On verifie le type de l'image
          if(!empty($infosImg) && $infosImg[2] >= 1 && $infosImg[2] <= 14) {
        
            if($uploadedImage['size'] <= Team::PHOTO_MAX_SIZE) {
              // Parcours du tableau d'erreurs
              if(isset($uploadedImage['error']) && UPLOAD_ERR_OK === $uploadedImage['error']) {
                // On renomme le fichier
                $imgName = substr(md5(uniqid()), 0, 10);
                $extension = $allowedMimeType[$mime_type];
        
                // Verifying folders
                $uploadFolder = $_SERVER['DOCUMENT_ROOT'].Team::PHOTO_FOLDER;
        
                if( !is_dir($uploadFolder) ) {
                  if( !mkdir($uploadFolder, 0755, true) ) {
                    Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
                  }
                }
        
                // Si c'est OK, on teste l'upload
                if(move_uploaded_file($uploadedImage['tmp_name'], $uploadFolder.$imgName.'.'.$extension)) {
                  // Create thumbnail
                  $img = $manager->make($uploadFolder.$imgName.'.'.$extension);
                  $img->fit(500, 500, function ($constraint) {
                    $constraint->upsize();
                  });
                  $img->save();
        
                  $image = Team::PHOTO_FOLDER.$imgName.'.'.$extension;
                } else {
                  // Sinon on affiche une erreur systeme
                  Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
                }
              } else {
                Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors du transfert de fichier");
              }
            } else {
              // Sinon erreur sur les dimensions et taille de l'image
              Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur dans les dimensions de l'image !");
            }
          } else {
            // Sinon erreur sur le type de l'image
            Chaperons_Et_Vous_Ajax::generateResponse(false, "Type de fichier non autorisé");
          }
        } else {
          // Sinon on affiche une erreur pour l'extension
          Chaperons_Et_Vous_Ajax::generateResponse(false, "Type de fichier non autorisé");
        }        
      }
      
      if ($isWordpressUser) {
        // Does the user edit a creche related people ?
        global $wpdb;
        $user_asso = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."app_asso_employees_creches WHERE id_user = ".$user_id." AND id_creche = ".$this->public_data->user_active_creche->get_id());
        if(empty($user_asso)) {
          Chaperons_Et_Vous_Ajax::generateResponse(false, "L'utilisateur n'existe pas.");
        }
        
        update_user_meta($user_id, "description", $_POST["description"]);
        if (!empty($image)) {
          update_user_meta($user_id, "photo", $image);
        }
      }
      // Its an team member
      else {
        $team = new Team($user_id);
        if (empty($team->get_id()) || $team->get_id_creche() != $this->public_data->get_creche()->get_id()) {
          Chaperons_Et_Vous_Ajax::generateResponse(false, "L'utilisateur n'existe pas.");
        }
        
        $team->set_first_name($_POST['first_name']);
        $team->set_last_name($_POST['last_name']);
        $team->set_position($_POST['position']);
        $team->set_description($_POST['description']);
        if (!empty($image)) {
          $team->set_photo($image);
        }
        $team->set_id_section($_POST['id_section']);
        $team->update();
      }
      Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
    }
    
    public function delete_people_info_from_ajax_request()
    {
      if(empty($_POST["user_id"])) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Veuillez préciser l'identifiant de l'utilisateur");
      }
      
      $team = new Team(intval($_POST['user_id']));
      if(empty($team->get_id()) || $team->get_id_creche() != $this->public_data->get_creche()->get_id()) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Utilisateur inconnu");
      }
      if(!$team->delete()) {
        Chaperons_Et_Vous_Ajax::generateResponse(false, "Erreur lors de la suppression de l'utilisateur");
      }
      Chaperons_Et_Vous_Ajax::generateResponse(true, "OK");
    }
    
    /**
     * List all employees of the active creche
     */
    public function list_employees() {
        if (is_user_logged_in() && $this->public_data->user_active_creche != NULL) {
            global $wpdb;

            // Get all employees users from the active creche
            /*$query = "SELECT ".$wpdb->prefix."app_asso_employees_creches.`id_user`, GROUP_CONCAT( `id_section` ) as sections
                    FROM lpcr_app_asso_employees_creches 
                    LEFT JOIN lpcr_app_asso_employees_sections ON lpcr_app_asso_employees_creches.id_user = lpcr_app_asso_employees_sections.id_user 
                    WHERE lpcr_app_asso_employees_creches.`id_creche` = 1
                    GROUP BY lpcr_app_asso_employees_creches.id_user;";*/
            $employee_users = $wpdb->get_results('SELECT `id_user` FROM '.$wpdb->prefix.'app_asso_employees_creches WHERE `id_creche` = '.$this->public_data->user_active_creche->get_id().';');
            foreach ($employee_users as $user) {
                $employee = get_userdata($user->id_user);
                $metas = get_user_meta($user->id_user);
                
                // Handle empty values
                $position = "";
                if(!empty($metas['position'])) {
                  $position = $metas['position'][0];
                }
                $description = "";
                if(!empty($metas['description'])) {
                  $description = $metas['description'][0];
                }
                $section = "";
                if(!empty($metas['section'])) {
                  $section = $metas['section'][0];
                }
                
                array_push($this->employees, array('role' => $employee->roles[0], 'id' => $employee->data->ID, 'name' => $employee->data->display_name, 'description' => $description, 'position' => $position, 'section'=>$section));
            }
            
            // Organize wordpress members
            // Prepare array for groups
            $directors = array();
            $subdirectors = array();
            $others = array();
            $sections = array();
            
            foreach ($this->employees as $user) {
              // Getting avatar
              $avatar_path = theme_root().'/img/profil-defaut.png';
              $image = get_user_option( 'photo', $user['id'] );
              if(!empty($image)) {
                $avatar_path = $image;
              }
              $user['avatar_path'] = $avatar_path;
            
              if ($user['role'] == UserRights::ROLE_EDITOR) {
                $directors[] = $user;
              } elseif ($user['role'] == UserRights::ROLE_CONTRIB) {
                $subdirectors[] = $user;
              } elseif(!empty($user['section'])) {
                $sections[$user['section']][] = $user;
              } else {
                $others[] = $user;
              }
            }
            
            // Get other employes from team table
            $list_others = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'app_team WHERE `id_creche` = '.$this->public_data->user_active_creche->get_id().';');
            foreach($list_others as $other) {
              $avatar_path = theme_root().'/img/profil-defaut.png';
              $image = $other->photo;
              if(!empty($image)) {
                $avatar_path = $image;
              }
              $arrayOther = array(
                'role' => NULL, 
                'id' => $other->id, 
                'name' => $other->first_name." ".$other->last_name, 
                'description' => $other->description, 
                'position' => $other->position,
                'avatar_path' => $other->photo,
                'section' => $other->id_section
              );
              
              if (empty($other->id_section)) {
                $others[] = $arrayOther;
              } else {
                $sections[$other->id_section][] = $arrayOther;
              }
            }
            
            $this->employees = array(
            	'directors' => $directors,
              'subdirectors' => $subdirectors,
              'sections' => $sections,
              'others' => $others
            );
        }
    }
    
    public function list_creche_details() {
      if (is_user_logged_in() && $this->public_data->user_active_creche != NULL) {
        global $wpdb;
        // Get all employees users from the active creche
        $this->creche = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'app_creches WHERE `id` = '.$this->public_data->user_active_creche->get_id().';');
      }
    }

    /**
     * Define the creche_details shortcode
     * Load the web display of the active creche
     */
    public function shortcode_creche_details($atts, $content)
    {
        // Init default values
        $atts = shortcode_atts(array(), $atts);
        // List all employees
        $this->list_creche_details();
        // Display the active creche team
        $this->display_creche_details();
    }
    
    /**
     * Define the ekip_list shortcode
     * Load the web display of the active creche team
     */    
    public function shortcode_ekip_list($atts, $content)
    {
      // Init default values
      $atts = shortcode_atts(array(), $atts);
      // List all employees
      $this->list_employees();
      // Display the active creche team
      $this->display_ekip();
    }

    private function display_creche_details() {
      require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-ekip-list-display.php';
      lpcr_htmlize_creche($this->public_data->user_active_creche, $this->get_employees());
    }
    
    /**
     * Load the view that display the team on the web interface
     */
    private function display_ekip() {
        require_once plugin_dir_path( dirname(dirname(dirname(__FILE__))) ).'public/partials/chaperons-et-vous-ekip-list-display.php';
        lpcr_htmlize_ekip($this->public_data->user_active_creche, $this->get_employees());
    }

}
