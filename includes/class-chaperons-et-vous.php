<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.r2.fr
 * @since      1.0.0
 *
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Chaperons_Et_Vous
 * @subpackage Chaperons_Et_Vous/includes
 * @author     Agence R2 <contact@r2.fr>
 */
class Chaperons_Et_Vous {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Chaperons_Et_Vous_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct() {

        $this->plugin_name = 'chaperons-et-vous';
        $this->version = '1.0.0';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $public_hooks = $this->define_public_hooks();

        $this->load_packages($public_hooks);
        
        define('MAIL_HOST', 'smtp.mandrillapp.com');
        define('MAIL_PORT', '587');
        define('MAIL_USERNAME', 'mickael44@gmail.com');
        define('MAIL_PASSWORD', '8JMkYkn8NH6YD7_ABmYhgg');
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Chaperons_Et_Vous_Loader. Orchestrates the hooks of the plugin.
     * - Chaperons_Et_Vous_i18n. Defines internationalization functionality.
     * - Chaperons_Et_Vous_Admin. Defines all hooks for the admin area.
     * - Chaperons_Et_Vous_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies() {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-chaperons-et-vous-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-chaperons-et-vous-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-chaperons-et-vous-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-chaperons-et-vous-public.php';
        
        /**
         * The class responsible for handling rights
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-chaperons-et-vous-user-rights.php';

        $this->loader = new Chaperons_Et_Vous_Loader();
    }

    /**
     * Load LPCR packages
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_packages($public_hooks) {

        $packages = array();

        /**
         * Load the album functionality
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/packages/album/class-chaperons-et-vous-album.php';
        $packages['albums'] = new Chaperons_Et_Vous_Album($public_hooks);

        /**
         * Load the evenements functionality
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/packages/evenement/class-chaperons-et-vous-evenement.php';
        $packages['evenement'] = new Chaperons_Et_Vous_Evenement($public_hooks);
        
        /**
         * Load the document functionality
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/packages/document/class-chaperons-et-vous-document.php';
        $packages['documents'] = new Chaperons_Et_Vous_Document($public_hooks);

        /**
         * Load the ekip functionality
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/packages/ekip/class-chaperons-et-vous-ekip.php';
        $packages['ekip'] = new Chaperons_Et_Vous_Ekip($public_hooks);

        /**
         * Load the messenger functionality
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/packages/messenger/class-chaperons-et-vous-messenger.php';
        $packages['messenger'] = new Chaperons_Et_Vous_Messenger($public_hooks);

        /**
         * Load the feed functionality
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/packages/feed/class-chaperons-et-vous-feed.php';
        new Chaperons_Et_Vous_Feed($packages, $public_hooks);
        
        /**
         * Load the article functionality
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/packages/article/class-chaperons-et-vous-article.php';
        new Chaperons_Et_Vous_Article($public_hooks);
        
        /**
         * Load the profile functionality
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/packages/profil/class-chaperons-et-vous-profil.php';
        new Chaperons_Et_Vous_Profil($public_hooks);
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Chaperons_Et_Vous_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale() {

        $plugin_i18n = new Chaperons_Et_Vous_i18n();
        $plugin_i18n->set_domain( $this->get_plugin_name() );

        $this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks() {

        $plugin_admin = new Chaperons_Et_Vous_Admin( $this->get_plugin_name(), $this->get_version() );

        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        
        // Custom field with people position
        $this->loader->add_action( 'show_user_profile', $plugin_admin, 'yoursite_extra_user_profile_fields' );
        $this->loader->add_action( 'edit_user_profile', $plugin_admin, 'yoursite_extra_user_profile_fields' );
        $this->loader->add_action( 'personal_options_update', $plugin_admin, 'yoursite_save_extra_user_profile_fields' );
        $this->loader->add_action( 'edit_user_profile_update', $plugin_admin, 'yoursite_save_extra_user_profile_fields' );
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks() {

        $plugin_public = new Chaperons_Et_Vous_Public( $this->get_plugin_name(), $this->get_version() );

        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
        return $plugin_public;
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name() {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Chaperons_Et_Vous_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }

}
